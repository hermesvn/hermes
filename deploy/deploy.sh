#!/bin/bash

DEPLOY_SERVER="volta.northstudio.dev"

echo "Deploying to ${DEPLOY_SERVER}"
ssh monokaijs@${DEPLOY_SERVER} 'bash' < ./deploy/build.sh
