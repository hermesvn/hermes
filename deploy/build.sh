#!/bin/bash

# Pull code
cd /home/monokaijs/hermes/
git checkout main
git pull origin main

# Build and deploy
yarn install

# sudo kill -9 $(sudo lsof -t -i:4124)

nx build frontend --prod
nx build backend --prod

pm2 stop backend
pm2 start frontend
pm2 start pm2.config.json
# pm2 log backend
