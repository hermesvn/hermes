import 'iron-session';

declare module "iron-session" {
  interface IronSessionData {
    account?: Account,
    isLoggedIn: boolean,
    credentials: Credentials
  }
}
