import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(handleProject);

async function handleProject(req: NextApiRequest, res: NextApiResponse) {
  let data;
  if (req.method === "GET") {
    data = await ApiService.getGitCredentials(req.query.accountId as string);
    console.log(data)
  } else if (req.method === "POST") {
    data = await ApiService.updateGitCredentials(req.query.accountId as string, req.body);
  } else if (req.method === "DELETE") {
    // data = await ApiService.deleteProject(req.query.projectId as string);
  }
  res.json({data});
}
