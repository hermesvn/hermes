import {requestInterceptor} from "@server/utils/request-interceptor";
import {NextApiRequest, NextApiResponse} from "next";
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(processAccounts);

async function processAccounts(req: NextApiRequest, res: NextApiResponse) {
  let data;
  if (req.method === "GET") {
    // data = await ApiService.getAccounts();
  } else if (req.method === "POST") {
    // data = await ApiService.createAccount(req.body);
  }
  res.json({data});
}
