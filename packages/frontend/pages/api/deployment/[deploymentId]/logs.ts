import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(getDeploymentLogs);

async function getDeploymentLogs(req: NextApiRequest, res: NextApiResponse) {
  const data = await ApiService.getDeploymentLogs(req.query.deploymentId as string);
  // console.log(data);
  res.json({data});
}
