import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiResponse} from 'next';
import {NextApiRequest} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(login, false);

async function login(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const data = await ApiService.logout(req.body.refreshToken);
    res.json(data);
  }
}
