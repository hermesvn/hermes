import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(checkConnectionRoute);

async function checkConnectionRoute(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const data = await ApiService.getGitLabTokens(req.body.code, req.body.redirectUri);
    res.json({data});
  }
}

