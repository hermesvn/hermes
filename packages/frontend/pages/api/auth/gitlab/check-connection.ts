import {requestInterceptor} from "@server/utils/request-interceptor";
import {NextApiResponse} from "next";
import {NextApiRequest} from "next";
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(checkConnectionRoute);

async function checkConnectionRoute(req: NextApiRequest, res: NextApiResponse) {
  try {
    const data = await ApiService.getGitLabConnectionStatus();
    return res.json({data});
  } catch (e) {
    return res.json({
      data: false
    });
  }
}
