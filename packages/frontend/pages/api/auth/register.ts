import { requestInterceptor } from '@server/utils/request-interceptor';
import { NextApiResponse } from 'next';
import { NextApiRequest } from 'next';


export default requestInterceptor(checkConnectionRoute);

async function checkConnectionRoute(req: NextApiRequest, _res: NextApiResponse) {
  console.log(req.query);
}
