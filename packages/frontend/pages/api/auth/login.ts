import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiResponse} from 'next';
import {NextApiRequest} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(login, false);

async function login(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const data = await ApiService.login(req.body.loginData, req.body.password);
    // console.log('data', data);
    req.session.isLoggedIn = true;
    req.session.account = data.account;
    req.session.credentials = data.credentials;
    await req.session.save();
    res.json(data);
  }
}
