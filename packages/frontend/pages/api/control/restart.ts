import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(checkConnectionRoute);

async function checkConnectionRoute(req: NextApiRequest, res: NextApiResponse) {
  const data = await ApiService.restart(req.body.projectId);
  res.json({data});
}
