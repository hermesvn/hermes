import {requestInterceptor} from "@server/utils/request-interceptor";
import {NextApiRequest, NextApiResponse} from "next";
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(getProjects);

async function getProjects(req: NextApiRequest, res: NextApiResponse) {
  let data;
  if (req.method === "GET") {
    data = await ApiService.getProjects();
  } else if (req.method === "POST") {
    data = await ApiService.createProject(req.body);
  }
  res.json({data});
}
