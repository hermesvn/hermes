import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(handleProject);

async function handleProject(req: NextApiRequest, res: NextApiResponse) {
  let data = {};
  if (req.method === "GET") {
    // data = await ApiService.getProject(req.query.projectId as string);
  } else if (req.method === "POST") {
    data = await ApiService.deployProject(req.query.projectId as string, req.body);
  } else if (req.method === "DELETE") {
    // data = await ApiService.deleteProject(req.query.projectId as string);
  }
  res.json({data});
}
