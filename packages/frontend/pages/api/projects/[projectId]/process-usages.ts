import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiRequest, NextApiResponse} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(handleProject);

async function handleProject(req: NextApiRequest, res: NextApiResponse) {
  let data = {};
    data = await ApiService.getProcessUsages(req.query.projectId as string);

  res.json({data});
}
