import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiResponse} from 'next';
import {NextApiRequest} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(disconnect);

async function disconnect(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const data = await ApiService.disconnectFromGitlab();
    res.json(data);
  }
}
