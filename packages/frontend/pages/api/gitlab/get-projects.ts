import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiResponse} from 'next';
import {NextApiRequest} from 'next';
import {ApiService} from "@lib/services/ApiService";

export default requestInterceptor(getProjects);

async function getProjects(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const data = await ApiService.getGitLabProjects(req.body.namespace);
    res.json({data});
  }
}
