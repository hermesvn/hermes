import { requestInterceptor } from '@server/utils/request-interceptor';
import { NextApiResponse } from 'next';
import { NextApiRequest } from 'next';
import {ApiService} from "@lib/services/ApiService";


export default requestInterceptor(getProjectBranches);

async function getProjectBranches(req: NextApiRequest, res: NextApiResponse) {
  const data = await ApiService.getGitLabProjectBranches(req.query.projectId as string);
  res.json({data});
}
