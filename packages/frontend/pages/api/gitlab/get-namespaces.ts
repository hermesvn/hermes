import {requestInterceptor} from '@server/utils/request-interceptor';
import {NextApiResponse} from 'next';
import {NextApiRequest} from 'next';
import {ApiService} from "@lib/services/ApiService";


export default requestInterceptor(getProjects);

async function getProjects(req: NextApiRequest, res: NextApiResponse) {
  const data = await ApiService.getGitLabNamespaces();
  res.json({data});
}
