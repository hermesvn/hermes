import '@lib/styles/globals.scss';
import 'antd-css-utilities/utility.min.css';
import {Provider, useSelector} from 'react-redux';
import {RootState, store} from '@lib/redux/store';
import {THEME_CONFIG} from "@lib/configs/AppConfig";
import {ThemeSwitcherProvider} from "react-css-theme-switcher";
import {IntlProvider} from "react-intl";
import {ConfigProvider} from 'antd';
import AppLocale from "@lib/lang";
import Head from "next/head";
import {AppLayout} from '@lib/layouts/app-layout';
import AuthLayout from '@lib/layouts/auth-layout';

const themes = {
  dark: `/css/dark-theme.css`,
  light: `/css/light-theme.css`,
};

function AppContent({children}: any) {
  const {locale} = useSelector((state: RootState) => state.theme);
  // @ts-ignore
  const currentAppLocale = AppLocale[locale];

  return (
    <IntlProvider
      locale={currentAppLocale.locale}
      messages={currentAppLocale.messages}>
      <ConfigProvider locale={currentAppLocale.antd} direction={"ltr"}>
        {children}
      </ConfigProvider>
    </IntlProvider>
  );
}

function MyApp({Component, pageProps, router}: any) {
  let Layout = AppLayout;
  if (router.pathname.startsWith('/auth/')) Layout = AuthLayout;
  return (
    <div className="App">
      <Head>
        <title>Hermes</title>
      </Head>
      <Provider store={store}>
        <ThemeSwitcherProvider themeMap={themes} defaultTheme={THEME_CONFIG.currentTheme}
                               insertionPoint="styles-insertion-point">
          <AppContent>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </AppContent>
        </ThemeSwitcherProvider>
      </Provider>
    </div>
  );
}

export default MyApp;
