import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import {GetServerSidePropsContext, GetServerSidePropsResult} from "next";
import React from "react";
import {ApiService} from "@lib/services/ApiService";

const AuthCallback = () => {
  return <></>;
};

export const getServerSideProps = withSessionAuthorizedSsr(async ({req}: GetServerSidePropsContext): Promise<GetServerSidePropsResult<any>> => {
  await ApiService.logout(req.session.credentials.refreshToken);
  return {
    redirect: {
      permanent: false,
      destination: '/auth/login'
    }
  }
});

export default AuthCallback;
