import React from "react";

const SuccessAuth = () => {
  return (
    <>
      Authentication success, now you can close this page.
    </>
  )
};

export default SuccessAuth;
