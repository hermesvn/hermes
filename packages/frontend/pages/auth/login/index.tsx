import LoginForm from '@lib/components/auth-components/LoginForm'
import {Card, Col, Row} from "antd";

import React from "react";

interface LoginProps {
  loading: boolean,
  showMessage: boolean,
  message: string,
}

const LoginOne = (props: LoginProps) => {

  // const theme = useSelector(state => state.theme.currentTheme);
  const theme = 'light';
  return (
    <Row justify="center">
      <Col xs={20} sm={20} md={20} lg={7}>
        <Card>
          <div className="my-4">
            <div className="text-center">
              <img className="img-fluid" src={`/img/${theme === 'light' ? 'logos/fulllogo_hermes.png' : 'logo-white.png'}`} alt=""/>
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              <p>Don't have an account yet? <a href="/auth/register-1">Sign Up</a></p>
            </div>
            <Row justify="center">
              <Col xs={24} sm={24} md={20} lg={20}>
                <LoginForm {...props} />
              </Col>
            </Row>
          </div>
        </Card>
      </Col>
    </Row>
  )
}


export default LoginOne;

