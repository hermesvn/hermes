import {JwtAuthService} from "@lib/services/JwtAuthService";
import {withSessionSsr} from "@server/utils/with-session";
import {GetServerSidePropsContext, GetServerSidePropsResult} from "next";
import React, {useEffect} from "react";
import {setData} from "@lib/services/StorageService";

interface AuthCallbackProps {
  authorized: boolean,
  account: Account
}

const AuthCallback = ({authorized, account}: AuthCallbackProps) => {
  useEffect(() => {
    if (authorized) {
      setData("account", account);
      location.href = "/auth/success";
    }
  }, [authorized, account]);
  return <></>;
};

export const getServerSideProps = withSessionSsr(async (context: GetServerSidePropsContext): Promise<GetServerSidePropsResult<any>> => {
  if (!context.query || !context.query.code || typeof context.query.code !== "string" || context.query.code.trim() === "") throw new Error("Missing code param.");
  try {
    const {data} = await JwtAuthService.requestToken(context.query.code);
    let authorized = false;
    if (data['token_type']) {
      const {access_token, refresh_token, expires_in} = data;
      const selfInfo = await JwtAuthService.getSelfInfo(access_token);

      context.req.session.isLoggedIn = true;
      context.req.session.account = selfInfo.data as Account;
      // context.req.session.credentials = {
      //   accessToken: access_token,
      //   refreshToken: refresh_token,
      //   expirationTime: new Date().getTime() + (expires_in * 1000)
      // }

      //!!!\\ this is super important.
      await context.req.session.save();
      authorized = true;
    }
    return {
      props: {
        authorized: authorized,
        account: context.req.session.account
      },
    };
  } catch (e) {
    console.log(e);
    return {
      redirect: {
        permanent: false,
        destination: "/auth/error",
      },
    };
  }
});

export default AuthCallback;
