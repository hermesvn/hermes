import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import {GetServerSidePropsContext, GetServerSidePropsResult} from "next";
import React, {useEffect} from "react";
import { getData, setData } from "@lib/services/StorageService";
import {ApiService} from "@lib/services/ApiService";

interface AuthCallbackProps {
  authorized: boolean,
  // account: Account,
  code: string
}

const AuthCallback = ({authorized, /*account,*/ code}: AuthCallbackProps) => {
  useEffect(() => {
    const redirectUri = (location.hostname === "localhost") ? `${location.protocol}//${location.hostname}:4200/auth/callback/gitlab` : `${location.protocol}//${location.hostname}/auth/callback/gitlab`;
    ApiService.getGitLabTokens(code, redirectUri).then(_ => {
      location.href = "/auth/success";
    });
  }, [authorized]);
  return <>

  </>;
};

export const getServerSideProps = withSessionAuthorizedSsr(async (context: GetServerSidePropsContext): Promise<GetServerSidePropsResult<any>> => {
  try {
    if (!context.query || !context.query.code || typeof context.query.code !== "string" || context.query.code.trim() === "") throw new Error("Missing code param.");

    return {
      props: {
        code: context.query.code
      }
    }
  } catch (e) {
    console.log(e);
    return {
      redirect: {
        permanent: false,
        destination: "/auth/error",
      },
    };
  }
});

export default AuthCallback;
