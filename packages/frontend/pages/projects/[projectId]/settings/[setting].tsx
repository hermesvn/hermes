import PageHeaderAlt from "@lib/components/layout-components/PageHeaderAlt";
import Flex from "@lib/components/shared-components/Flex";
import React from "react";
import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import SettingSections from "@lib/components/projects-components/SettingSections";
import {ApiService} from "@lib/services/ApiService";
import {Col, Row} from "antd";
import {useRouter} from "next/router";
import SettingList from "@lib/components/projects-components/SettingList";

const data = [
  {
    title: 'General',
    path: 'general'
  },
  {
    title: 'Git',
    path: 'git'
  },
  {
    title: 'Environment Variables',
    path: 'environment-variables'
  },
  {
    title: 'Nginx & Domains',
    path: "nginx-config"
  },
];

const ProjectSettingPage = ({project, setting}) => {
  const router = useRouter();
  return (
    <>
      <PageHeaderAlt className="border-bottom">
        <div className="container-fluid">
          <Flex justifyContent="between" alignItems="center" className="py-4">
            <h2>Project Settings</h2>
          </Flex>
        </div>
      </PageHeaderAlt>

      <div className="mt-5">
        <Row gutter={16}>
          <Col xs={24} md={6}>
            <SettingList mode={"project"} pathname={router.pathname} targetId={project.id} settingSections={data}/>
          </Col>
          <Col xs={24} md={18}>
            <SettingSections mode={"project"} project={project} setting={setting}/>
          </Col>
        </Row>
      </div>
    </>
  );
};

export const getServerSideProps = withSessionAuthorizedSsr(async ({req, query}) => {
  const project = await ApiService.getProject(query.projectId as string);
  switch (query.setting) {
    case "general":
      break;
    case "git":
      break;
    case "environment-variables":
      project.environmentVariablesAsString = project.autoDeploy.environmentVariables.reduce((previousValue, currentValue) => {
        return previousValue += `${currentValue.key}=${currentValue.value}\n`, previousValue;
      }, "");
      break;
    case "nginx-config":
      project.environmentVariablesAsString = project.autoDeploy.environmentVariables.reduce((previousValue, currentValue) => {
        return previousValue += `${currentValue.key}=${currentValue.value}\n`, previousValue;
      }, "");
      break;
  }

  return {
    props: {
      project: project,
      setting: query.setting
    }
  };
});

export default ProjectSettingPage;

