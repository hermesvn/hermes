import React, {useEffect, useState} from "react";
import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import {ApiService} from "@lib/services/ApiService";
import {Button, Card, Table, Tabs} from "antd";
import Flex from "@lib/components/shared-components/Flex";
import {PageHeaderAlt} from "@lib/components/layout-components/PageHeaderAlt";
import ProjectUsageChart from "@lib/components/projects-components/ProjectUsageChart";
import {ProjectSetting} from "@lib/components/projects-components/ProjectSetting";
import {ProjectOverview} from "@lib/components/projects-components/ProjectOverview";
import BuildLog from "@lib/components/deployment-components/BuildLog";
import io from 'socket.io-client';
import {v4 as uuidv4} from "uuid";

const socket = io(process.env.NEXT_PUBLIC_SOCKET_HOST || `wss://hermes-api.northstudio.dev`);
// const socket = io("wss://api.hermess.site");
// const socket = io(`ws://localhost:${4123}`);

interface DataType {
  key?: React.Key;
  label?: string;
  content?: string;
}

interface ProjectPageProps {
  projectId: string,
}

const ProjectPage = ({projectId}: ProjectPageProps) => {
  const [project, setProject] = useState<Project>();
  const [processLogs, setProcessLogs] = useState<DataType[]>([]);
  const [deploymentLogs, setDeploymentLogs] = useState<DataType[]>([]);
  const [columnLength, setColumnLength] = useState<number>(100);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    (async () => {
      try {
        const loadedProject = await ApiService.getProject(projectId);
        loadedProject.environmentVariablesAsString = loadedProject.autoDeploy.environmentVariables.reduce((previousValue, currentValue) => {
          return previousValue += `${currentValue.key}=${currentValue.value}\n`, previousValue;
        }, "");

        setProject(loadedProject);
        setLoading(false);

        if (loadedProject.deployments.length > 0) {
          let deploymentLogs = await ApiService.getDeploymentLogs(loadedProject.deployments[0]._id);
          if (deploymentLogs.logData) {
            deploymentLogs = deploymentLogs.logData.split("-break-").map(data => {
              data = data.split("-logContent-");
              return {
                key: uuidv4(),
                date: data[0],
                content: data[1],
              }
            });
            setDeploymentLogs(deploymentLogs);
          }
          let processLogs = await ApiService.getProcessLogs(loadedProject.id)
          if (processLogs) {
            processLogs = processLogs.split("\n").slice(-15).map(data => {
              return {
                key: uuidv4(),
                label: `${loadedProject?.autoDeploy?.domain} |`,
                content: data,
              }
            });

            setColumnLength(`${loadedProject?.autoDeploy?.domain} |`.length * 8.5);

            processLogs.splice(-1);

            setProcessLogs(processLogs);
          }
        }

        socket.emit("registerDeploymentLogThread", loadedProject._id);

        socket.on('updateDeploymentLogs', (log) => {
          const data = log.split("-logContent-");
          setDeploymentLogs((prev) => ([
            ...prev, {
              key: uuidv4(),
              date: data[0],
              content: data[1],
            }
          ]));
        });

        return () => {
          socket.off('updateDeploymentLogs');
        };
      } catch (e) {
        location.href = "/404";
      }
    })();
  }, []);

  return (
    <>
      <PageHeaderAlt className="border-bottom">
        <div>
          <Flex justifyContent="between" alignItems="center" className="py-4">
            <h2>{project?.name}</h2>
            <div className={"d-flex flex-row"}>
              <Button className="mr-4" href={project?.repo.http_url_to_repo} target="_blank">
                View Git Repository
              </Button>
              <Button type="primary" href={`https://${project?.autoDeploy?.domain}`} target="_blank">
                Visit
              </Button>
            </div>
          </Flex>
        </div>
      </PageHeaderAlt>

      <Tabs defaultActiveKey="1" style={{marginTop: -45}}>
        <Tabs.TabPane tab="Deployment" key="1">
          <Card>
            <ProjectOverview project={project} resetItemList={() => {
              setDeploymentLogs([]);
            }}/>
          </Card>
          <h2 className="mt-5">
            Deployment Status
          </h2>
          <BuildLog itemList={deploymentLogs}/>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Application Logs" key="2">
          <Card>
            <Table showHeader={false} columns={[
              {
                dataIndex: 'label',
                width: columnLength,
              },
              {
                dataIndex: 'content',
              },
            ]} dataSource={processLogs} pagination={false}/>
          </Card>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Statistics" key="3">
          <Card>
            <ProjectUsageChart project={project} socket={socket}/>
          </Card>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Settings" key="4">
          <ProjectSetting project={project} setting="general"/>
        </Tabs.TabPane>
      </Tabs>
    </>
  )
};

export const getServerSideProps = withSessionAuthorizedSsr((context) => {
  return {
    props: {
      projectId: context?.params?.projectId
    }
  }
});
export default ProjectPage;
