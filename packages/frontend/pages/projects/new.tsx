import { GitLabConnect } from "@lib/components/gitlab-connect";
import { DeploymentBrief } from "@lib/components/gitlab-connect/deployment-brief";
import { GitLabProjectDetail } from "@lib/components/gitlab-connect/project-detail";
import PageHeaderAlt from "@lib/components/layout-components/PageHeaderAlt";
import Flex from "@lib/components/shared-components/Flex";
import { RootState } from "@lib/redux/store";
import { Col, Row } from "antd";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {withSessionAuthorizedSsr} from "@server/utils/with-session";

const NewProjectPage = () => {
  const {selectedProject} = useSelector((state: RootState) => state.newProject);
  return (
    <>
      <PageHeaderAlt className="border-bottom">
        <div className="container-fluid">
          <Flex justifyContent="between" alignItems="center" className="py-4">
            <h2>Add new Project</h2>
          </Flex>
        </div>
      </PageHeaderAlt>
      <div className="mt-4">
        <Row gutter={16}>
          <Col xs={24} md={8}>
            {!selectedProject ? (
              <GitLabConnect/>
            ): (
              <GitLabProjectDetail/>
            )}
          </Col>
          <Col xs={24} md={16}>
            <DeploymentBrief/>
          </Col>
        </Row>
      </div>
    </>
  )
};

export const getServerSideProps = withSessionAuthorizedSsr(({req}) => {
  return {
    props: {
      authorized: true // just nothing
    }
  }
});

export default NewProjectPage;
