import PageHeaderAlt from "@lib/components/layout-components/PageHeaderAlt";
import {ProjectsList} from "@lib/components/projects-components/ProjectsList";
import Flex from "@lib/components/shared-components/Flex";
import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import {Button, Radio} from "antd";
import {
  AppstoreOutlined,
  UnorderedListOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {useState} from "react";
import Link from "next/link";

function Home() {

  const [currentView, setCurrentView] = useState<'grid' | 'list'>('grid');

  return (
    <>
      <PageHeaderAlt className="border-bottom">
        <div className="container-fluid">
          <Flex justifyContent="between" alignItems="center" className="py-4">
            <h2>Projects</h2>
            <div>
              <Radio.Group defaultValue={"grid"} onChange={(e) => {
                setCurrentView(e.target.value);
              }}>
                <Radio.Button value={"grid"}><AppstoreOutlined/></Radio.Button>
                <Radio.Button value={"list"}><UnorderedListOutlined/></Radio.Button>
              </Radio.Group>
              <Link href="/projects/new" passHref={true}>
                <Button href="/projects/new" type="primary" className="ml-2">
                  <PlusOutlined/>
                  <span>New</span>
                </Button>
              </Link>
            </div>
          </Flex>
        </div>
      </PageHeaderAlt>
      <ProjectsList view={currentView}/>
    </>
  )
}

export const getServerSideProps = withSessionAuthorizedSsr(async ({req}) => {
  return {
    props: {
      test: "ok"
    }
  }
});

export default Home;
