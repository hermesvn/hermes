import {withSessionRoute} from '@server/utils/with-session';
import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";
import {ApiService} from "@lib/services/ApiService";

export const requestInterceptor = (handler: NextApiHandler, authRequired: boolean | string[] = true) => async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const withSessionHandler = withSessionRoute(async (attachedRequest, attachedResponse) => {
      if (!!authRequired && typeof authRequired === "boolean") {
        if (!attachedRequest.session.isLoggedIn) {
          return attachedResponse.status(400).send({
            message: "Unauthorized."
          });
        }
      }
      if (attachedRequest.session.credentials) await ApiService.setCredentialsWithSession(attachedRequest);
      return handler(attachedRequest, attachedResponse);
    });
    try {
      await withSessionHandler(req, res);
    } catch (e) {
      // console.log(e)
      res.status(400).send({
        message: e.message || "Something went wrong."
      })
    }
  } catch (e) {
    console.log(e);
    res.status(400).send({
      message: e.message || "Unauthorized."
    });
  }
};
