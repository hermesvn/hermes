import errorHandler from "@server/middleware/error.middleware"
import {NextApiRequest, NextApiResponse} from "next";

export function runMiddleware(req: NextApiRequest, res: NextApiResponse, fn: any) {
  return new Promise((resolve) => {
    if (typeof fn !== "function") {
      fn.then((data: any) => {
        return resolve(data);
      }).catch((err: any) => {
        errorHandler(err, req, res);
      });
    } else {
      fn(req, res, (result: any) => {
        return resolve(result);
      }).catch((err: any) => {
        errorHandler(err, req, res);
      });
    }
  });
}
