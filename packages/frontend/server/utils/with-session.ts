import {withIronSessionApiRoute, withIronSessionSsr} from "iron-session/next";
import {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  NextApiHandler,
  NextApiRequest,
  NextApiResponse,
} from "next";
import {ApiService} from "@lib/services/ApiService";

const sessionOptions = {
  password: "complex_password_at_least_32_characters_long",
  cookieName: "myapp_cookiename",
  // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
  cookieOptions: {
    secure: process.env.NODE_ENV === "production",
  },
};

export function withSessionRoute(handler: NextApiHandler) {
  return withIronSessionApiRoute(handler, sessionOptions);
}

export function withSessionAuthorizedRoute(handler: NextApiHandler) {
  return withIronSessionApiRoute((req: NextApiRequest, res: NextApiResponse) => {
    if (!req.session.isLoggedIn) throw "Unauthorized.";
    return handler(req, res);
  }, sessionOptions);
}

export function withSessionSsr<P extends { [key: string]: unknown } = { [key: string]: unknown }>(
  handler: (
    context: GetServerSidePropsContext
  ) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>
) {
  return withIronSessionSsr(handler, sessionOptions);
}

export function withSessionAuthorizedSsr<P extends { [key: string]: unknown } = { [key: string]: unknown }>(
  handler: (
    context: GetServerSidePropsContext
  ) => GetServerSidePropsResult<P> | Promise<GetServerSidePropsResult<P>>
) {
  return withIronSessionSsr(async (context: GetServerSidePropsContext): Promise<any> => {
    const {req} = context;
    if (req.session && req.session.isLoggedIn) {
      if (req.session.credentials) await ApiService.setCredentialsWithSession(req);
      return handler(context);
    } else return {
      redirect: {
        permanent: false,
        destination: '/auth/login'
      }
    };
  }, sessionOptions);
}
