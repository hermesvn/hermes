import dotenv from 'dotenv';
import path from 'path';

dotenv.config({path: path.join(__dirname, '../../../.env')});

export default {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  system: {
    roles: ["ADMIN", "USER"]
  },
  mongoose: {
    url: process.env.MONGODB_URL + (process.env.NODE_ENV === 'test' ? '-test' : ''),
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    accessExpirationMinutes: process.env.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: process.env.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: process.env.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: process.env.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
  gCloud: {
    projectId: process.env.GCLOUD_PROJECT_ID,
    clientEmail: process.env.GCLOUD_CLIENT_EMAIL,
    privateKey: process.env.GCLOUD_PRIVATE_KEY,
    baseUrl: "https://storage.googleapis.com/"
  },
  discord: {
    botToken: process.env.DISCORD_BOT_TOKEN || `OTQ1Mjk1NDk0OTQ1NTk5NTMw.YhOFCA.KbVT0kQbco-HEWnaKtq5b-Uf6BU`,
    pushLogChannel: process.env.DISCORD_PUSH_CHANNEL || `945292558479790160`,
    deploymentLogChannel: process.env.DISCORD_DEPLOYMENT_CHANNEL || `945372477478039602`,
  }
};
