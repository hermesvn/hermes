import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type NullableDeploymentType = null | DeploymentType;

interface NewProjectState {
  namespace: string,
  selectedProject: boolean,
  project: Project<NullableDeploymentType>,
  searchQuery: string,
  deploymentType: NullableDeploymentType,
}


const initialState: NewProjectState = {
  namespace: 'default',
  searchQuery: '',
  selectedProject: false,
  deploymentType: null,
  project: {
    name: '',
    repoSource: 'gitlab',
    repo: {},
    cloneOption: 'HTTPS',
    description: '',
    code: '',
    branch: '',
    avatarUrl: '',
    owner: undefined,
    creationTime: 0,
    deploymentType: null,
    scripts: {
      startup: '',
      install: '',
      build: '',
    },
    environmentVariables: {},
    valid: false
  }
};

export const newProjectSlice = createSlice({
  name: 'new-project',
  initialState,
  reducers: {
    setNamespace(state, action: PayloadAction<string>) {
      state.namespace = action.payload;
    },
    setSearchQuery(state, action: PayloadAction<string>) {
      state.searchQuery = action.payload;
    },
    setDeploymentRepository(state, action: PayloadAction<GitLabProject | any>) {
      state.selectedProject = true;
      const project = action.payload;
      if (action.payload.name && action.payload.name !== "") {
        console.log("change projects.");
        state.project = {
          ...state.project,
          repo: action.payload,
          name: project.name,
          description: project.description,
          avatarUrl: project.avatar_url,
        };
      }
    },
    setDeploymentProject(state, action: PayloadAction<Project>) {
      state.project = {
        ...state.project,
        ...action.payload
      };
    },
    setDeploymentFramework(state, action: PayloadAction<NullableDeploymentType>) {
      state.deploymentType = action.payload;
      if (action.payload !== null) {
        state.project = {
          ...state.project,
          deploymentType: action.payload
        }
      }
    }
  },
});

export const {
  setNamespace,
  setSearchQuery,
  setDeploymentProject,
  setDeploymentRepository,
  setDeploymentFramework
} = newProjectSlice.actions

export default newProjectSlice.reducer
