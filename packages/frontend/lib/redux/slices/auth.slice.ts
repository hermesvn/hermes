import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {getData, setData} from "@lib/services/StorageService";

interface InitialStateType {
  account: Account
}

const initialState: InitialStateType = {
  account: getData('account', {
    email: '',
    fullName: ''
  })
}

export const authSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    setAccount(state, action: PayloadAction<Account>) {
      setData('account', action.payload);
      state.account = action.payload;
    },
  },
});

export const {
  setAccount,
} = authSlice.actions

export default authSlice.reducer
