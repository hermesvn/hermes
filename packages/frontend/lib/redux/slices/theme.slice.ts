import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {THEME_CONFIG} from '@lib/configs/AppConfig'

export interface ThemeState {
  navCollapsed: boolean,
  sideNavTheme: string,
  locale: string,
  navType: string,
  topNavColor: string,
  headerNavColor: string,
  mobileNav: boolean,
  currentTheme: string,
  direction: string
}

const initialState: ThemeState = {
  ...THEME_CONFIG
};

export const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    switchTheme(state, action: PayloadAction<string>) {
      state.currentTheme = action.payload;
    },
    toggleMobileNav(state, action: PayloadAction<boolean>) {
      state.mobileNav = action.payload;
    },
    changeDirection(state, action: PayloadAction<string>) {
      state.direction = action.payload;
    },
    changeTopNavColor(state, action: PayloadAction<string>) {
      state.topNavColor = action.payload;
    },
    changeNavType(state, action: PayloadAction<string>) {
      state.navType = action.payload;
    },
    changeLocale(state, action: PayloadAction<string>) {
      state.locale = action.payload;
    },
    changeNavStyle(state, action: PayloadAction<string>) {
      state.sideNavTheme = action.payload;
    },
    toggleCollapsedNav(state, action: PayloadAction<boolean>) {
      state.navCollapsed = action.payload;
    },
    changeHeaderNavColor(state, action: PayloadAction<string>) {
      state.headerNavColor = action.payload;
    }
  },
});

export const {
  switchTheme,
  toggleCollapsedNav,
  toggleMobileNav,
  changeDirection,
  changeLocale,
  changeNavStyle,
  changeNavType,
  changeTopNavColor,
  changeHeaderNavColor
} = themeSlice.actions

export default themeSlice.reducer
