import {configureStore} from '@reduxjs/toolkit'
import themeReducer from "../slices/theme.slice";
import newProjectReducer from "../slices/new-project.slice";
import authReducer from "../slices/auth.slice";

export const store = configureStore({
  reducer: {
    theme: themeReducer,
    newProject: newProjectReducer,
    auth: authReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
