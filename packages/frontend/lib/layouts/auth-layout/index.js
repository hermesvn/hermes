import React from 'react'
import Loading from '@lib/components/shared-components/Loading';
import { useThemeSwitcher } from "react-css-theme-switcher";
import { useSelector } from 'react-redux';

const backgroundStyle = {
	backgroundImage: 'url(/img/others/img-17.jpg)',
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover'
}

export const AuthLayout = ({children}) => {
	const { status } = useThemeSwitcher();

	return (
		
		<div className="h-100" style={backgroundStyle}>
			<div className="container d-flex flex-column justify-content-center h-100">
				{children}
			</div>
		</div>
	)
}


export default AuthLayout
