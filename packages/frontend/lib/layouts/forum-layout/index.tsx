import React from "react";

interface ForumLayoutProps {
  children: any
}

const ForumLayout = ({children}: ForumLayoutProps) => {
  return (<>
    {children}
  </>)
};

export default ForumLayout;
