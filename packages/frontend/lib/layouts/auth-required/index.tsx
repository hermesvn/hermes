import React from "react";
import {useSelector} from "react-redux";
import {RootState} from "@lib/redux/store";

interface AuthRequiredProps {
  requiredPermissions?: [],
  children?: any
}

export const AuthRequired = (props: AuthRequiredProps) => {
  const requiredPermissions = props.requiredPermissions || [];
  const children = props.children;
  const {account} = useSelector((state: RootState) => state.auth);
  return (
    <>
      {children}
    </>
  )
};