import React from 'react';
import {useSelector} from 'react-redux';
import SideNav from '@lib/components/layout-components/SideNav';
import TopNav from '@lib/components/layout-components/TopNav';
import MobileNav from '@lib/components/layout-components/MobileNav'
import HeaderNav from '@lib/components/layout-components/HeaderNav';
import PageHeader from '@lib/components/layout-components/PageHeader';
import Footer from '@lib/components/layout-components/Footer';

import {
  Layout,
  Grid, BackTop,
} from "antd";

import navigationConfig from "@lib/configs/NavigationConfig";
import {
  SIDE_NAV_WIDTH,
  SIDE_NAV_COLLAPSED_WIDTH,
  NAV_TYPE_SIDE,
  NAV_TYPE_TOP,
  DIR_RTL,
  DIR_LTR
} from '@lib/constants/ThemeConstant';
import utils from '@lib/utils';
import {useThemeSwitcher} from "react-css-theme-switcher";
import {useRouter} from "next/router";

const {Content} = Layout;
const {useBreakpoint} = Grid;

const TestComponent = () => {
  return <></>
}

export const AppLayout = ({direction = DIR_LTR, children}) => {
  const {navCollapsed, navType} = useSelector((state) => state['theme']);

  const router = useRouter();
  const currentRouteInfo = utils.getRouteInfo(navigationConfig, router.pathname);

  const screens = utils.getBreakPoint(useBreakpoint());
  const isMobile = screens.length === 0 ? false : !screens.includes('lg')
  const isNavSide = navType === NAV_TYPE_SIDE;
  const isNavTop = navType === NAV_TYPE_TOP;

  const getLayoutGutter = () => {
    if (isNavTop || isMobile) {
      return 0
    }
    return navCollapsed ? SIDE_NAV_COLLAPSED_WIDTH : SIDE_NAV_WIDTH
  }

  const {status} = useThemeSwitcher();

  if (status === 'loading') {
    // TODO: show loading
  }

  const getLayoutDirectionGutter = () => {
    if (direction === DIR_LTR) {
      return {paddingLeft: getLayoutGutter()}
    }
    if (direction === DIR_RTL) {
      return {paddingRight: getLayoutGutter()}
    }
    return {paddingLeft: getLayoutGutter()}
  }

  return (
    <Layout>
      <TestComponent/>
      <HeaderNav isMobile={isMobile}/>
      {(isNavTop && !isMobile) ? <TopNav routeInfo={currentRouteInfo}/> : null}
      <Layout className="app-container">
        {(isNavSide && !isMobile) ? <SideNav routeInfo={currentRouteInfo}/> : null}
        <Layout className="app-layout" style={getLayoutDirectionGutter()}>
          <div className={`app-content ${isNavTop ? 'layout-top-nav' : ''}`} >
            <PageHeader display={currentRouteInfo?.breadcrumb} title={currentRouteInfo?.title}/>
            <Content>
              {children}
            </Content>
          </div>
          <Footer/>
        </Layout>
      </Layout>
      {isMobile && <MobileNav/>}
      <BackTop />
    </Layout>
  )
}


export default AppLayout;
