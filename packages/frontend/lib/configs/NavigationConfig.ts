import {
  DashboardOutlined,
  AppstoreOutlined,
} from '@ant-design/icons';
import { APP_PREFIX_PATH, AUTH_PREFIX_PATH } from '@lib/configs/AppConfig'

const dashBoardNavTree = [{
  key: 'home',
  path: APP_PREFIX_PATH,
  title: 'sidenav.home',
  icon: DashboardOutlined,
  breadcrumb: false,
  submenu: []
}]

const appsNavTree = [{
  key: 'domains',
  path: `/domains`,
  title: 'sidenav.domains',
  icon: AppstoreOutlined,
  breadcrumb: false,
  submenu: []
}]

const navigationConfig = [
  ...dashBoardNavTree,
  ...appsNavTree,
]

export default navigationConfig;
