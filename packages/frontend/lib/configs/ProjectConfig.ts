export const Frameworks: DeploymentType[] = ['nextjs', 'nodejs', 'reactjs', 'svelte', 'svelte-kit', 'gatsby', 'preact', "angular"];

export const FrameworkNames: IFrameworkDetails = {
  "nextjs": "Next.Js",
  "reactjs": "React.Js",
  "svelte": "Svelte",
  "svelte-kit": "SvelteKit",
  "nodejs": "NodeJs",
  "gatsby": "Gatsby",
  "preact": "Preact",
  "angular": "Angular"
};
export const FrameworkIcons: IFrameworkDetails = {
  "nodejs": "https://cdn.iconscout.com/icon/free/png-128/nodejs-1-226034.png",
  "nextjs": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/next.svg",
  "reactjs": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/react.svg",
  "svelte": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/svelte.svg",
  "svelte-kit": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/svelte.svg",
  "gatsby": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/gatsby.svg",
  "preact": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/preact.svg",
  "angular": "https://res.cloudinary.com/zeit-inc/image/fetch/https:/raw.githubusercontent.com/vercel/vercel/main/packages/frameworks/logos/angular.svg",
}