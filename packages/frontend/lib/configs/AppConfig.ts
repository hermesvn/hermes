import {SIDE_NAV_LIGHT, DIR_LTR, NAV_TYPE_TOP} from '@lib/constants/ThemeConstant';
import { env } from './EnvironmentConfig'
import {getData} from "@lib/services/StorageService";

export const APP_NAME = 'Hermes';
export const API_BASE_URL = env["API_ENDPOINT_URL"]
export const APP_PREFIX_PATH = '/';
export const AUTH_PREFIX_PATH = '/auth';

export const THEME_CONFIG = {
	navCollapsed: false,
	sideNavTheme: SIDE_NAV_LIGHT,
	locale: 'en',
	navType: NAV_TYPE_TOP,
	topNavColor: '#3e82f7',
	headerNavColor: '',
	mobileNav: false,
	currentTheme: typeof window !== 'undefined' ? getData('theme', 'light') : 'light',
	direction: DIR_LTR
};
