import React from "react";
import {Button, Form, Input, message as messageAlert} from "antd";
// import {motion} from "framer-motion";
import {ApiService} from "@lib/services/ApiService";
import {setAccount} from "@lib/redux/slices/auth.slice";
import {useDispatch} from "react-redux";
// import {setData} from "@lib/services/StorageService";

const LoginForm = (props: any) => {
  const dispatch = useDispatch();

  const {
    loading,
    // showMessage,
    // message
  } = props;

  return (
    <>
      {/*<motion.div*/}
      {/*  initial={{ opacity: 0, marginBottom: 0 }}*/}
      {/*  animate={{*/}
      {/*    opacity: showMessage ? 1 : 0,*/}
      {/*    marginBottom: showMessage ? 20 : 0*/}
      {/*  }}>*/}
      {/*  <Alert type="error" showIcon message={message}></Alert>*/}
      {/*</motion.div>*/}
      <Form
        layout="vertical"
        name="login-form"
        onFinish={async ({loginData, password}) => {
          try {
            const data = await ApiService.login(loginData, password);
            dispatch(setAccount(data.account));
            // setData("credentials", data.credentials);
            location.href = "/";
          } catch (e) {
            messageAlert.error(`Failed to login.`);
          }
        }}
      >
        <Form.Item
          label="Username or Email"
          labelCol={{span: 24}}
          name="loginData"
          rules={[{required: true, message: "Please input your username."}]}
        >
          <Input placeholder="Username or Email "/>
        </Form.Item>

        <Form.Item
          label="Password"
          labelCol={{span: 24}}
          name="password"
          rules={[{required: true, message: "Please input your password."}]}
        >
          <Input.Password placeholder="Password"/>
        </Form.Item>


        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default LoginForm;
