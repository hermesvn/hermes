import React, {useState} from 'react';
import {SettingOutlined} from '@ant-design/icons';
import {Drawer, Menu} from 'antd';
import ThemeConfigurator from '@lib/components/layout-components/ThemeConfigurator';
import {DIR_RTL} from '@lib/constants/ThemeConstant';

interface INavPanelProps {
  direction: string
}

const NavPanel = ({direction} : INavPanelProps) => {
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <Menu mode="horizontal">
        <Menu.Item key="panel" onClick={showDrawer}>
          <span><SettingOutlined className="nav-icon mr-0"/></span>
        </Menu.Item>
      </Menu>
      <Drawer
        title="Theme Config"
        placement={direction === DIR_RTL ? 'left' : 'right'}
        width={350}
        onClose={onClose}
        visible={visible}
      >
        <ThemeConfigurator/>
      </Drawer>
    </>
  );
}

export default NavPanel;
