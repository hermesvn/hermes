import React from 'react'
import {useSelector} from 'react-redux';
import {NAV_TYPE_TOP} from '@lib/constants/ThemeConstant';
import utils from '@lib/utils'
import MenuContent from '@lib/components/layout-components/MenuContent'
import {RootState} from "../../redux/store";

export const TopNav = ({routeInfo: any, localization = true}) => {
  const {topNavColor} = useSelector((state: RootState) => state.theme);
  return (
    <div className={`top-nav ${utils.getColorContrast(topNavColor)}`} style={{backgroundColor: topNavColor}}>
      <div className="top-nav-wrapper">
        <MenuContent
          type={NAV_TYPE_TOP}
          localization={localization}
        />
      </div>
    </div>
  )
}

export default TopNav;
