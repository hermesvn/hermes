import React from "react";
import {Menu, Dropdown, Avatar, Button, Space} from "antd";
import {
  EditOutlined,
  LogoutOutlined
} from '@ant-design/icons';
import Icon from '@lib/components/util-components/Icon';
import {useSelector} from "react-redux";
import {RootState} from "@lib/redux/store";
import { ApiService } from "@lib/services/ApiService";

const menuItem = [
	// {
	// 	title: "Edit Profile",
	// 	icon: EditOutlined ,
	// 	path: "/"
  // },
];

const menu = (
  <Menu
    onClick={async () => {
      await ApiService.logout();
      location.href = "/auth/login";
    }}
    items={[
      {
        label: '1st menu item',
        key: '1',
      },
      {
        label: '2nd menu item',
        key: '2',
      },
      {
        label: '3rd menu item',
        key: '3',
      },
    ]}
  />
);

export const NavProfile = () => {
  const {account} = useSelector((state: RootState) => state.auth);
  const profileImg = "https://vercel.com/api/www/avatar/t2Dtv9VhNJDo7hlTnPQmSjVl?&s=160";
  const profileMenu = (
    <div className="nav-profile nav-dropdown">
      <div className="nav-profile-header">
        <div className="d-flex">
          <Avatar size={45} src={profileImg} />
          <div className="pl-3">
            <h4 className="mb-0">{account?.username}</h4>
            <span className="text-muted">{account?.email}</span>
          </div>
        </div>
      </div>
      {/*<div className="nav-profile-body">*/}
        <Menu>
          <Menu.Item key={"edit_account"}>
            <a href={`/accounts/${account?._id}/settings/general`}>
              <Icon type={EditOutlined} /> <span className="font-weight-normal">Edit Profile</span>
            </a>
          </Menu.Item>
          {menuItem.map((el, i) => {
            return (
              <Menu.Item key={i}>
                <a href={el.path}>
                  <Icon type={el.icon} /> <span className="font-weight-normal">{el.title}</span>
                </a>
              </Menu.Item>
            );
          })}
          <Menu.Item onClick={async () => {
            await ApiService.logout();
            location.href = "/auth/login";
          }} key={menuItem.length + 1}>
            <span>
              <LogoutOutlined /> <span className="font-weight-normal">Sign Out</span>
            </span>
          </Menu.Item>
        </Menu>
      {/*</div>*/}
    </div>
  );
  return (
    <Dropdown placement="bottomRight" overlay={profileMenu} trigger={["click"]}>
      <Menu className="d-flex align-item-center" mode="horizontal">
        <Menu.Item key="profile">
          <Avatar src={profileImg} />
        </Menu.Item>
      </Menu>
    </Dropdown>
  );
}

export default NavProfile;
