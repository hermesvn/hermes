import React from "react";
import {Drawer} from "antd";
import {connect, useDispatch, useSelector} from "react-redux";
import {NAV_TYPE_SIDE} from "@lib/constants/ThemeConstant";
import {Scrollbars} from "react-custom-scrollbars";
import MenuContent from "@lib/components/layout-components/MenuContent";
import Logo from "./Logo";
import Flex from "@lib/components/shared-components/Flex";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {toggleMobileNav} from "@lib/redux/slices/theme.slice";
import {RootState} from "@lib/redux/store";

interface Props {
  routeInfo?: any,
  hideGroupTitle?: any,
  localization?: any
}

export const MobileNav = ({routeInfo, hideGroupTitle, localization}: Props) => {
  const {
    sideNavTheme,
    mobileNav,
  } = useSelector((state: RootState) => state.theme);
  const props = {sideNavTheme, routeInfo, hideGroupTitle, localization};
  const dispatch = useDispatch();

  const onClose = () => {
    dispatch(toggleMobileNav(false));
  };

  return (
    <Drawer
      placement="left"
      closable={false}
      onClose={onClose}
      visible={mobileNav}
      bodyStyle={{padding: 5}}
    >
      <Flex flexDirection="column" className="h-100">
        <Flex justifyContent="between" alignItems="center">
          <Logo mobileLogo={true}/>
          <div className="nav-close" onClick={() => onClose()}>
            <ArrowLeftOutlined/>
          </div>
        </Flex>
        <div className="mobile-nav-menu">
          <Scrollbars autoHide>
            <MenuContent type={NAV_TYPE_SIDE} {...props} />
          </Scrollbars>
        </div>
      </Flex>
    </Drawer>
  );
};

export default MobileNav;
