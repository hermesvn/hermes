import React, {Component} from 'react';
import {Breadcrumb} from 'antd';
import navigationConfig from "@lib/configs/NavigationConfig";
import IntlMessage from '@lib/components/util-components/IntlMessage';
import {useRouter} from "next/router";

const breadcrumbData: any = {
  '/app': <IntlMessage id="home"/>
};

navigationConfig.forEach((elm, _) => {
  const assignBreadcrumb = (obj: any) => breadcrumbData[obj.path] = <IntlMessage id={obj.title}/>;
  assignBreadcrumb(elm);
  if (elm.submenu) {
    elm.submenu.forEach((elm: any) => {
      assignBreadcrumb(elm)
      if (elm.submenu) {
        elm.submenu.forEach(elm => {
          assignBreadcrumb(elm)
        })
      }
    })
  }
})

const BreadcrumbRoute = () => {
  const {location}: any = useRouter();
  const pathSnippets = location.pathname.split('/').filter((i: any) => i);
  const buildBreadcrumb = pathSnippets.map((_: never, index: any) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
    return (
      <Breadcrumb.Item key={url}>
        <a href={url}>{breadcrumbData[url]}</a>
      </Breadcrumb.Item>
    );
  });

  return (
    <Breadcrumb>
      {buildBreadcrumb}
    </Breadcrumb>
  );
};

export class AppBreadcrumb extends Component {
  render() {
    return (
      <BreadcrumbRoute/>
    )
  }
}

export default AppBreadcrumb
