import React from 'react'
import {APP_NAME} from '@lib/configs/AppConfig';
import Link from "next/link";

export default function Footer() {
  return (
    <footer className="footer">
      <span>Copyright  &copy;  {`${new Date().getFullYear()}`} <span
        className="font-weight-semibold">{`${APP_NAME}`}</span> All rights reserved.</span>
      <div>
        <Link href="/#">
          <a className="text-gray" onClick={e => e.preventDefault()}>Term & Conditions</a>
        </Link>
        <span className="mx-2 text-muted"> | </span>
        <Link href="/#">
          <a className="text-gray" onClick={e => e.preventDefault()}>
            Privacy & Policy
          </a></Link>
      </div>
    </footer>
  )
}

