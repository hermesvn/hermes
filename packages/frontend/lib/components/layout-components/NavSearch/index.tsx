import React, {useEffect, useState} from 'react';
import {connect, useSelector} from "react-redux";
import {
  CloseOutlined,
} from '@ant-design/icons';
import utils from '@lib/utils'
import SearchInput from './SearchInput';
import {RootState} from "../../../redux/store";

export const NavSearch = (props) => {
  const {active, close} = props;
  const {headerNavColor} = useSelector((s: RootState) => s.theme);
  const [mode, setMode] = useState(utils.getColorContrast(headerNavColor));
  useEffect(() => {
    setMode(utils.getColorContrast(headerNavColor));
  }, [headerNavColor]);

  return (
    <div
      className={`nav-search ${active ? 'nav-search-active' : ''} ${mode}`}
      style={{backgroundColor: headerNavColor}}
    >
      <div className="d-flex align-items-center w-100">
        <SearchInput close={close} active={active}/>
      </div>
      <div className="nav-close" onClick={close}>
        <CloseOutlined/>
      </div>
    </div>
  )
}

export default NavSearch;
