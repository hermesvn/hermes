import {Col, Input, Row, Select} from "antd";
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {RootState} from "@lib/redux/store";
import {EyeInvisibleOutlined, EyeTwoTone} from "@ant-design/icons";

interface Props {
  mode?: "setting" | "system";
  defaultProject?: Project;
  defaultGitCredentials?: GitCredentials;
  onChange: (targetBranch: { [key: string]: any }) => void;
}

const CloneOptions =
  ({mode = "setting", defaultProject, defaultGitCredentials, onChange}: Props) => {
    const {project: newProject} = useSelector((state: RootState) => state.newProject);
    const [project, setProject] = useState<Project>(defaultProject || newProject);
    const [cloneOptions, setCloneOptions] = useState<CloneOption[]>([]);
    const [selectedCloneOption, setSelectedCloneOption] = useState<CloneOption>();
    const [gitCredentials, setGitCredentials] = useState<GitCredentials>(defaultGitCredentials);

    // useEffect(() => {
    //   if (defaultProject) dispatch(setDeploymentProject(project));
    // })

    useEffect(() => {
      setCloneOptions([{
        name: "HTTPS", url: project?.repo.http_url_to_repo, default: true
      }, {
        name: "SSH", url: project?.repo.ssh_url_to_repo, default: false
      }]);

      setSelectedCloneOption({
        name: project?.autoDeploy?.cloneOption || "HTTPS",
        url: (project?.autoDeploy?.cloneOption === "SSH") ? project?.repo.ssh_url_to_repo : project?.repo.http_url_to_repo,
        default: true
      });

    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
      if (selectedCloneOption) onChange(selectedCloneOption);
    }, [selectedCloneOption]);

    useEffect(() => {
      if (gitCredentials) onChange(gitCredentials);
    }, [gitCredentials]);

    return (
      <>
        <Row gutter={{xs: 8, sm: 16, md: 16, lg: 16}}>
          <Col span={4}>
            <Select
              // style={{width: 'fit-content'}}
              value={selectedCloneOption?.name}
              onChange={(value) => {
                setSelectedCloneOption(cloneOptions.find((x: CloneOption) => x?.name === value));
              }}
            >
              {cloneOptions.length > 0 && cloneOptions.map((cloneOption: CloneOption) => (
                <Select.Option key={cloneOption?.name} selected={cloneOption?.default}>
                  {cloneOption?.name}
                </Select.Option>)
              )}
            </Select>
          </Col>
          <Col span={20}>
            {selectedCloneOption?.url && <Input
              disabled
              value={selectedCloneOption?.url}
            />}
          </Col>
        </Row>

        {mode === "system" && <>
          {selectedCloneOption?.name === "HTTPS" ?
            <Row className={"mt-3"} gutter={{xs: 8, sm: 16, md: 16, lg: 16}}>
              <>
                <Col span={12}>
                  <Input
                    required

                    addonBefore={"Username"}
                    placeholder="Please enter your GitLab username"
                    onChange={(e) => {
                      setGitCredentials((prevState) => ({...prevState, username: e.target.value}));
                    }}
                    value={gitCredentials?.username}
                  />
                </Col>
                <Col style={{width: "100%"}} span={12}>
                  <Input.Password
                    iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                    required
                    addonBefore={"Password"}
                    placeholder="Please enter your GitLab password"
                    onChange={(e) => {
                      setGitCredentials((prevState) => ({...prevState, password: e.target.value}));
                    }}
                  />
                </Col>
              </>
            </Row>
            :
            <>
              <Row className={"mt-3"} gutter={{xs: 8, sm: 16, md: 16, lg: 16}}>
                <>
                  <Col span={12}>
                    <Input
                      key={"sshUsername"}
                      required
                      addonBefore={"Username"}
                      placeholder="Please enter your Git SSH username"
                      onChange={(e) => {
                        setGitCredentials((prevState) => ({...prevState, sshUsername: e.target.value}));
                      }}
                      value={gitCredentials?.sshUsername}
                    />
                  </Col>
                  <Col span={12}>
                    <Input
                      key={"passPhrase"}
                      required
                      addonBefore={"Pass Phrase"}
                      placeholder="Please enter your pass phrase"
                      onChange={(e) => {
                        setGitCredentials((prevState) => ({...prevState, passPhrase: e.target.value}));
                      }}
                      value={gitCredentials?.passPhrase}
                    />
                  </Col>
                </>
              </Row>
              <Row className={"mt-3"} gutter={{xs: 8, sm: 16, md: 16, lg: 16}}>
                <>
                  <Col span={12}>
                    <Input
                      key={"publicKey"}
                      required
                      addonBefore={"Public key"}
                      placeholder="Please enter your public key path"
                      onChange={(e) => {
                        setGitCredentials((prevState) => ({...prevState, publicKeyPath: e.target.value}));
                      }}
                      value={gitCredentials?.publicKeyPath}
                    />
                  </Col>
                  <Col span={12}>
                    <Input
                      key={"privateKey"}
                      required
                      addonBefore={"Private key"}
                      placeholder="Please enter your private key path"
                      onChange={(e) => {
                        setGitCredentials((prevState) => ({...prevState, privateKeyPath: e.target.value}));
                      }}
                      value={gitCredentials?.privateKeyPath}
                    />
                  </Col>
                </>
              </Row>
            </>}
        </>
        }
      </>
    )
  }

export default CloneOptions;
