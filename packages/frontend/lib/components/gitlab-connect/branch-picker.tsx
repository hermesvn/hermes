import React, {useEffect, useState} from "react";
import {ApiService} from "@lib/services/ApiService";
import {Select, Spin} from "antd";
import {useSelector} from "react-redux";
import {RootState} from "@lib/redux/store";

interface BranchPickerProps {
  project?: Project
  defaultBranch?: string,
  onChange: (targetBranch: string) => void
}

export const BranchPicker = ({project = undefined, defaultBranch, onChange}: BranchPickerProps) => {
  const {project: storedProject} = useSelector((state: RootState) => state.newProject);
  const [branches, setBranches] = useState<GitLabBranch[]>([]);
  const [loadedBranches, setLoadedBranches] = useState<boolean>(false);
  const [selectedBranch, setSelectedBranch] = useState<GitLabBranch>();

  useEffect(() => {
    if (storedProject.repoSource === "gitlab") {
      ApiService.getGitLabProjectBranches((project) ? project.repo.id : storedProject.repo.id).then(response => {
        setLoadedBranches(true);
        setBranches(response);
        if (typeof defaultBranch !== "undefined") {
          const gitlabBranch = response.find((x: GitLabBranch) => x.name === defaultBranch);
          if (gitlabBranch) setSelectedBranch(gitlabBranch);
        } else {
          setSelectedBranch(response.find((x: GitLabBranch) => x.default));
        }
      });
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (selectedBranch) onChange(selectedBranch.name);
  }, [selectedBranch]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Spin spinning={!loadedBranches}>
      {loadedBranches && (
        <Select
          style={{width: '100%'}}
          value={selectedBranch?.name}
          onChange={(value) => {
            setSelectedBranch(branches.find((x: GitLabBranch) => x.name === value));
          }}
        >
          {branches.map((branch: any) => (
            <Select.Option key={branch['name']} selected={branch['default']}>
              {branch['name']}
            </Select.Option>
          ))}
        </Select>
      )}
    </Spin>
  );
}
