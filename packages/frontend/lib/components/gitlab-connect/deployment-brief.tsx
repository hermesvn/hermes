import React, {useEffect, useState} from "react";
import {Button, Card, Divider} from "antd";
import {RootState} from "@lib/redux/store";
import {useDispatch, useSelector} from "react-redux";
import {EnvEditor} from "@lib/components/app-components/EnvEditor";
import {DeploymentTypeSelector} from "@lib/components/gitlab-connect/deployment-type-selector";
import {BranchPicker} from "@lib/components/gitlab-connect/branch-picker";
import {ConfigScripts} from "@lib/components/app-components/ConfigScripts";
import {setDeploymentProject} from "@lib/redux/slices/new-project.slice";
import {ApiService} from "@lib/services/ApiService";
import {NginxConfig} from "@lib/components/app-components/NginxConfig";
import {slugify} from "@server/utils/process-string";
import CloneOptions from "@lib/components/account-components/CloneOptions";


function randomString(length: number) {
  const result = [];
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return result.join('');
}

export const DeploymentBrief = () => {
  const dispatch = useDispatch();
  const {selectedProject, project} = useSelector((state: RootState) => state.newProject);
  const [scripts, setScripts] = useState<DeploymentScripts>({});

  useEffect(() => {
    console.log("project", project)
    dispatch(setDeploymentProject({
      scripts
    }));
  }, [scripts]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <Card>
        <h3>{selectedProject ? "Deployment Information" : "Choose a projects..."}</h3>
        <p>
          {selectedProject ? "Here is some short information about the deployment of the projects." : "Select a projects on the left panel to continue..."}
        </p>
        {selectedProject && (
          <>
            <h4>Clone Options</h4>
            <CloneOptions onChange={(cloneOption) => {
              dispatch(setDeploymentProject({
                cloneOption
              }));
            }}/>
            <Divider dashed={true}/>

            <h4>Deployment Branch</h4>
            <BranchPicker onChange={(branch) => {
              dispatch(setDeploymentProject({
                branch
              }));
            }}/>
            <Divider dashed={true}/>
            <DeploymentTypeSelector onChange={(deploymentType => {
              dispatch(setDeploymentProject({
                deploymentType
              }));
            })}/>
            <Divider dashed={true}/>
            <ConfigScripts
              onStartupScriptChange={script => setScripts({...scripts, startup: script})}
              onBuildScriptChange={script => setScripts({...scripts, build: script})}
              onInstallScriptChange={script => setScripts({...scripts, install: script})}
            />
            <Divider dashed/>
            <NginxConfig
              defaultValue={(project?.name) && {domain: `${slugify(project?.name)}-${randomString(5).toLowerCase()}`}}
              onChange={nginxSupport => {
                dispatch(setDeploymentProject(nginxSupport));
              }}/>
            <Divider dashed/>
            <EnvEditor onChange={envPairs => {
              dispatch(setDeploymentProject({
                environmentVariables: envPairs
              }));
            }}/>
            <div className={"mt-4"}>
            </div>
          </>
        )}
      </Card>
      {selectedProject && (
        <Button
          block
          disabled={!project.branch || !project.deploymentType}
          type={"primary"}
          size={"large"}
          onClick={() => {
            // console.log("project", project);
            ApiService.createProject(project).then((data) => {
              location.href = `/projects/${data.id}`;
            });
          }}
        >
          Submit
        </Button>
      )}
    </>
  );
};
