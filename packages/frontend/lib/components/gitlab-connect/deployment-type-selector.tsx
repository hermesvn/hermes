import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@lib/redux/store";
import {Radio, Space} from "antd";
import {setDeploymentFramework} from "@lib/redux/slices/new-project.slice";
import {FrameworkIcons, FrameworkNames, Frameworks} from "@lib/configs/ProjectConfig";
import React, {useEffect} from "react";

interface DeploymentTypeSelectorProps {
  onChange: (deploymentType: DeploymentType) => void,
  defaultValue?: null | DeploymentType
}

export const DeploymentTypeSelector = ({defaultValue = null, onChange}: DeploymentTypeSelectorProps) => {
  const dispatch = useDispatch();
  const {deploymentType} = useSelector((state: RootState) => state.newProject);

  useEffect(() => {
    dispatch(setDeploymentFramework(defaultValue));
  }, [defaultValue]);

  if (!deploymentType) return (
    <>
      <h4>Framework Preset</h4>
      <Radio.Group value={deploymentType} onChange={(event) => {
        dispatch(setDeploymentFramework(event.target.value));
        onChange(event.target.value)
      }}>
        <Space direction="vertical">
          {Frameworks.map(f => f && (
            <Radio key={f} value={f}>
              <img alt="frameworks" style={{width: 20, marginRight: 8}} src={FrameworkIcons[f]}/> {FrameworkNames[f]}
            </Radio>
          ))}
        </Space>
      </Radio.Group>
    </>
  );
  return (
    <div className={"d-flex justify-content-between"}>
      <h4 className={"mb-0 pb-0"}>
        Current Framework: {FrameworkNames[deploymentType]} <img alt="frameworks" style={{width: 20, marginRight: 8}} src={FrameworkIcons[deploymentType]}/>
      </h4>
      <a onClick={() => {
        dispatch((setDeploymentFramework(null)));
      }}>Change</a>
    </div>
  )
}
