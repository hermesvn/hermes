import {RootState} from "@lib/redux/store";
import {Avatar, Button, Card, Image} from "antd";
import React, {useState} from "react";
import {useSelector} from "react-redux";
import {
  GitlabOutlined,
  EyeOutlined,
  FormOutlined,
  GitlabFilled,
  CopyOutlined,
  CheckOutlined
} from "@ant-design/icons";
import ProjectClipboard from "@lib/components/projects-components/ProjectClipboard";

interface Props {
  project?: Project
}

export const GitLabProjectDetail = ({project = undefined}: Props) => {
  const {project: storedProject} = useSelector((state: RootState) => state.newProject);
  const [isCopied, setIsCopied] = useState(false);

  function onCopy(e) {
    e.preventDefault();
    setIsCopied(true);
    console.log({id: project});
    if (project.gitlabId) navigator.clipboard.writeText(project?.gitlabId || "Unknown id").then(_ => {
      setTimeout(_ => {
        setIsCopied(false);
      }, 1000);
    });
  }

  function onChangeProjectClick() {
    if (typeof window !== 'undefined') location.reload();
  }

  return (!project) ? (
    <Card>
      <div className={"d-flex flex-column align-items-center mt-4"}>
        <Avatar
          size={96}
          shape={"square"}
          src={
            <Image alt="gitlab-logo"
                   src="https://seeklogo.com/images/G/gitlab-logo-FAA48EFD02-seeklogo.com.png"
                   style={{width: 64}}
                   preview={false}
            />
          }/>
        <h4 className={"mt-2"}>
          {storedProject.name}
          <a target={"_blank"} className={"ml-2"} href={storedProject.repo.web_url} rel="noreferrer">
            <EyeOutlined/>
          </a>
        </h4>
      </div>
      <div className={"mt-4"}>
        <ProjectClipboard icon={<FormOutlined/>} link={storedProject.repo.id}/>
        <ProjectClipboard icon={<GitlabOutlined/>} link={storedProject.repo.http_url_to_repo}/>
        <ProjectClipboard icon={<GitlabFilled/>} link={storedProject.repo.ssh_url_to_repo}/>
      </div>
      <Button onClick={onChangeProjectClick} block className={"mt-4"}>
        Change Project
      </Button>
    </Card>
  ) : (
    <>
      <Card>
        <div className="d-flex align-items-center">
          <img className="mr-5" alt="frameworks" style={{width: 64, alignItems: "center"}}
               src={"https://seeklogo.com/images/G/gitlab-logo-FAA48EFD02-seeklogo.com.png"}/>
          <div className="mt-3">
            <a
              target={"_blank"}
              href={project.repo.web_url}
              rel="noreferrer">
              <h4>{project.name}</h4>
            </a>
            <p>
              ProjectId: {project.gitlabId} <a onClick={onCopy}>{(!isCopied) ?
              <CopyOutlined style={{color: "#404040"}}/> : <CheckOutlined style={{color: "#02c205"}}/>}</a>
            </p>
          </div>
        </div>
      </Card>
    </>
  )
};
