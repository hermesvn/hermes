import {setDeploymentRepository, setNamespace, setSearchQuery} from "@lib/redux/slices/new-project.slice";
import {RootState} from "@lib/redux/store";
import {ApiService} from "@lib/services/ApiService";
import {GitLabService} from "@server/services/gitlab.service";
import {Button, Card, Col, Input, List, Row, Select, Spin} from "antd";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {SearchOutlined} from "@ant-design/icons";

const GitLabSignIn = () => {
  return (
    <>
      <Button block onClick={() => {
        GitLabService.openLogin().then(() => {
          window.location.reload();
        });
      }}>
        Connect GitLab Account
      </Button>
    </>
  );
};

export const GitLabProjectSelector = () => {
  const dispatch = useDispatch();
  const displaying = 4;
  const {namespace, searchQuery} = useSelector((state: RootState) => state.newProject);
  const [projects, setProjects] = useState<GitLabProject[]>([]);
  const [loadingProjects, setLoadingProjectsStatus] = useState(false);
  const [loadedProjects, setLoadedProjects] = useState([]);
  useEffect(() => {
    if (namespace === "default") return;
    setLoadingProjectsStatus(true);
    ApiService.getGitLabProjects(namespace).then(data => {
      setLoadedProjects(data);
      setLoadingProjectsStatus(false);
    });
  }, [namespace]);

  useEffect(() => {
    const list = loadedProjects.filter((x: any) => {
      return x.name.toLowerCase().includes(searchQuery.toLowerCase());
    });
    list.length = list.length >= displaying ? displaying : list.length;
    setProjects(list);
  }, [loadedProjects, searchQuery]);

  return (
    <Spin spinning={loadingProjects}>
      <List
        className="mt-4"
        size="large"
        bordered
        dataSource={projects}
        renderItem={item => (
          <List.Item
            actions={[
              <Button key={"import-" + item.id} onClick={() => {
                dispatch(setDeploymentRepository(item));
              }} size="small" type="primary">
                Import
              </Button>
            ]}
          >
            {item["name"]}
          </List.Item>
        )}
      />
    </Spin>
  );
};

const GitLabNamespaceSelector = () => {
  const dispatch = useDispatch();
  const {namespace} = useSelector((state: RootState) => state.newProject);
  const [loadingNamespaces, setLoadingNamespaces] = useState(true);
  const [namespaces, setNamespaces] = useState([]);
  useEffect(() => {
    ApiService.getGitLabNamespaces().then(data => {
      const namespacesData = data;
      const defaultNamespace = namespacesData.find(data => data.kind === "user");
      dispatch(setNamespace(defaultNamespace["kind"] + "s" + "/" + defaultNamespace["full_path"]));
      setNamespaces(namespacesData);
      setLoadingNamespaces(false);
    });
  }, [dispatch]);

  return (
    <Spin spinning={loadingNamespaces}>
      <Select value={namespace} style={{width: "100%"}} onChange={(value) => {
        dispatch(setNamespace(value));
      }}>
        {namespaces.map(namespace => {
          // console.log("Gitlab namespace avatar", namespace["avatar_url"] ? `https://gitlab.com` + namespace["avatar_url"] : "");
          return (
            <Select.Option value={namespace["kind"] + "s" + "/" + namespace["full_path"]} key={namespace["full_path"]}>
              {namespace["avatar_url"] ? (
                <img style={{width: 16, height: 16, borderRadius: 20, marginRight: 8}}
                     src={`https://gitlab.com` + namespace["avatar_url"]}/>
              ) : (
                <img style={{width: 12, height: 12, borderRadius: 20, marginRight: 8}}
                     src={"https://seeklogo.com/images/G/gitlab-logo-FAA48EFD02-seeklogo.com.png"} alt={"gitlab-icon"}/>
              )}
              {namespace["name"]}
            </Select.Option>
          )
        })}
      </Select>
    </Spin>
  );
};

export const GitLabConnect = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [isConnected, setConnected] = useState(false);

  useEffect(() => {
    ApiService.getGitLabConnectionStatus().then(data => {
      setConnected(data);
      setIsLoading(false);
      dispatch(setNamespace("default"));
    });
  }, [dispatch]);

  let timeout: NodeJS.Timeout;

  return (
    <>
      <Spin spinning={isLoading}>
        <Card>
          <h3>Import Git Repository</h3>
          <p>
            Import repositories for deployments...
          </p>
          {!isConnected ? (
            <GitLabSignIn/>
          ) : (
            <>
              <Row gutter={16}>
                <Col span={10}>
                  <GitLabNamespaceSelector/>
                </Col>
                <Col span={14}>
                  <Input
                    prefix={<SearchOutlined className="site-form-item-icon"/>}
                    placeholder={"Search..."}
                    onKeyUp={function (event: any) {
                      clearInterval(timeout);
                      timeout = setTimeout(() => {
                        dispatch(setSearchQuery(event.target["value"]));
                      }, 300);
                    }}
                  />
                </Col>
              </Row>
              <GitLabProjectSelector/>
            </>
          )}
        </Card>
      </Spin>
    </>
  );
};
