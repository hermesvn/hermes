import React, {useState} from "react";
import {Badge, Button, Checkbox, Col, Image, message, Modal, Row} from "antd";
import {ExportOutlined, RetweetOutlined} from "@ant-design/icons"
import {ApiService} from "@lib/services/ApiService";

interface ProjectOverviewProps {
  project: Project,
  resetItemList: () => void
}

export const ProjectOverview = ({project, resetItemList}: ProjectOverviewProps) => {
  const [skipGitChecking, setSkipGitChecking] = useState<boolean>(false);
  const [cachedDeployment, setCachedDeployment] = useState<boolean>(false);
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const redeployProject = async () => {
    message.success(`Restart request has been received.`);
    ApiService.deployProject(project.id, {skipGitChecking, cachedDeployment})
      .then(() => message.success(`Project has been deployed.`))
      .catch(e => {
        // console.log("error", e.message);
        message.error(e.message);
      });
  }

  return (
    <div style={{
      display: "flex",
      flexWrap: "wrap"
    }}>
      <Modal
        className="mt-lg-5"
        title={
          <div style={{textAlign: "center"}}>
            <h2 className="mt-3">Redeploy to Production</h2>
          </div>

        }
        visible={modalVisible}
        onOk={() => {
          resetItemList && resetItemList()
          redeployProject().then(_ => {
            setModalVisible(!modalVisible);
          })
        }}
        onCancel={() => setModalVisible(!modalVisible)}
        width={500}
      >
        <div>
          <p>
            You are about to create a new Deployment with the same source code as your current Deployment, but with the
            newest configuration from your Project Settings.
          </p>
          <Checkbox onChange={() => {
            setSkipGitChecking(!skipGitChecking);
          }}>Enable Git skipping
          </Checkbox>
          <br/>
          <Checkbox onChange={() => {
            setCachedDeployment(!cachedDeployment);
          }}>Redeploy with existing Build Cache.</Checkbox>
        </div>
      </Modal>
      <Row justify="space-between" gutter={[8, 16]}>
        <Col xs={24} lg={7}>
          <a href={`https://${project?.autoDeploy?.domain}`} target="_blank" rel="noreferrer"
             style={{
               position: "relative", display: "inline-block", color: "white"
             }}>
            <div style={{position: "absolute", height: "100%", width: "100%", zIndex: 1}}></div>
            <img
              style={{width: "90%"}}
              src="https://cdn.dribbble.com/users/508588/screenshots/11263679/thg_m78_05_4x.jpg"
            />
            {/*<iframe*/}
            {/*  src={(project?.autoDeploy?.domain) ? project?.autoDeploy?.domain : "https://stable.northstudio.dev/"}*/}
            {/*  width="320px"*/}
            {/*  height="200px"*/}
            {/*  id="myId"*/}
            {/*  className="myClassname"*/}
            {/*  // display="initial"*/}
            {/*/>*/}
          </a>
        </Col>
        <Col xs={20} lg={14}>
          <div className="container-fluid mt-3">
            <div className="d-flex">
              <div style={{marginRight: 32}}>
                <h5>STATUS</h5>
                <p><Badge status="success"/>Ready</p>
              </div>
              <div style={{marginRight: 32}}>
                <h5>ENVIRONMENT</h5>
                <p>Production</p>
              </div>
              <div>
                <h5>DURATION</h5>
                <p>5s (6h ago)</p>
              </div>
            </div>
            <div className="mb-3">
              <h5>DOMAINS</h5>
              <a
                style={{color: "black"}}
                href={`https://${project?.autoDeploy?.domain}`}
                target={"_blank"}
                rel={"noreferrer"}
              >
                {project?.autoDeploy?.domain || "Not registered"} <ExportOutlined/>
              </a>
              <br/>
            </div>
            <div>
              <h5>BRANCH</h5>
              <a className="d-flex align-items-center" style={{color: "black"}}
                 href={`${project?.repo?.web_url}/-/tree/${project?.branch}`}
                 target="_blank"
                 rel="noreferrer"
              >
                <Image preview={false} width={12} alt="gitlab-icon"
                       src="https://seeklogo.com/images/G/gitlab-logo-FAA48EFD02-seeklogo.com.png"/><span
                className="ml-1">{project?.branch}</span>
              </a>
            </div>
          </div>
        </Col>
        <Col xs={24} lg={3}>
          <div style={{display: "flex"}}>
            <Button block style={{marginLeft: "auto"}} onClick={() => setModalVisible(!modalVisible)}>
              Redeploy <RetweetOutlined/>
            </Button>
          </div>
        </Col>
      </Row>
    </div>
  );
};
