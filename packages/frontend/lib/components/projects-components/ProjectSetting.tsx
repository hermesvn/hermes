import {Col, List, Row} from "antd";
import SettingSections from "@lib/components/projects-components/SettingSections";
import React, {useState} from "react";

export const ProjectSetting = ({project, setting}) => {
  const [settingType, setSettingType] = useState<ProjectSettingTypes>(setting);
  const settings = [
    {
      title: 'General',
      path: 'general'
    },
    {
      title: 'Git',
      path: 'git'
    },
    {
      title: 'Environment Variables',
      path: 'environment-variables'
    },
    {
      title: 'Nginx & Domains',
      path: "nginx-config"
    },
  ];

  const changeSetting = (e) => {
    e.preventDefault();
    const value: ProjectSettingTypes = (settings.filter(data => e.target.innerHTML.replace("&amp;", "&") === data.title))[0].path as ProjectSettingTypes;
    setSettingType(value);
  }

  return (
    <>
      <div className="mt-5">
        <Row gutter={16}>
          <Col xs={24} md={6}>
            <List
              style={{
                position: "sticky",
                // top: 150
              }}
              itemLayout="horizontal"
              dataSource={settings}
              split={false}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    title={<a
                      onClick={changeSetting}
                    >
                      <p style={(item.path !== settingType) ? {fontSize: 16} : {fontSize: 16, color: "black"}}>{item.title}</p>
                    </a>}
                  />
                </List.Item>
              )}
            />
          </Col>
          <Col xs={24} md={18}>
            <SettingSections mode="project" project={project} setting={settingType}/>
          </Col>
        </Row>
      </div>
    </>
  );
};
