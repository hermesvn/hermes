import React from "react";
import { Progress } from "antd";
import Utils from "@lib/utils";

export const ProjectItemProgress = ({progression}: any) => (
	<Progress percent={progression} strokeColor={Utils.getProgressStatusColor(100- progression)} size="small"/>
)