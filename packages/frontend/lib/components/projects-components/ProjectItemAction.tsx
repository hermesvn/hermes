import React from "react";
import { Menu } from "antd";
import EllipsisDropdown from "../shared-components/EllipsisDropdown";
import {
	EyeOutlined,
	EditOutlined,
} from '@ant-design/icons';

interface Props {
  id: string,
  deploy?: (id: string) => void
}

export const ProjectItemAction = ({id, deploy}: Props) => (
	<EllipsisDropdown
		menu={
			<Menu>
				<Menu.Item key="0" onClick={() => {
          location.href = `/projects/${id}`;
        }}>
					<EyeOutlined />
					<span className="ml-2">View</span>
				</Menu.Item>
				<Menu.Item key="1" onClick={() => {
          location.href = `/projects/${id}/settings/general`;
        }}>
					<EditOutlined />
					<span className="ml-2">Edit</span>
				</Menu.Item>
			</Menu>
		}
	/>
)
