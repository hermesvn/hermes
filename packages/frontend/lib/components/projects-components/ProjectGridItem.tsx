import { Card } from "antd";
import React from "react";
import Flex from "../shared-components/Flex";
import { ProjectItemAction } from "./ProjectItemAction";
import { ProjectItemHeader } from "./ProjectItemHeader";
import { ProjectItemInfo } from "./ProjectItemInfo";
import { ProjectItemProgress } from "./ProjectItemProgress";
import Link from "next/link";

export const ProjectGridItem = ({ data, removeId }: any) => (
	<Card>
		<Flex alignItems="center" justifyContent="between">
			<Link href={`/projects/${data.id}/`}>
				<a href={`/projects/${data.id}/`}>
					<ProjectItemHeader name={data.name} category={data.category} />
				</a>
			</Link>
			<ProjectItemAction id={data.id} deploy={removeId}/>
		</Flex>
		<div className="mt-2">
			<ProjectItemInfo
				project={data}
				attachmentCount={data.attachmentCount}
				completedTask={data.completedTask}
				usageText={"13MB"}
				statusColor={data.statusColor}
				uptime={32.2}
			/>
		</div>
		<div className="mt-3">
			<ProjectItemProgress progression={20} />
		</div>
		{/*<div className="mt-2">*/}
		{/*	<ProjectItemMember member={data.member}/>*/}
		{/*</div>*/}
	</Card>
);
