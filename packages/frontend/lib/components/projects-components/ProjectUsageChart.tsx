import React, {useEffect, useState} from 'react';
import {Line} from '@ant-design/plots';
import {Card} from "antd";
import {ApiService} from "@lib/services/ApiService";
import {Socket} from "socket.io-client";
import moment from "moment";

export interface ProcessUsage {
  date: string,
  cpu: number,
  ram: number
}

interface ProjectUsageChartProps {
  project: Project,
  socket: Socket
}

const ramConfig = {
  xField: 'date',
  yField: 'ram',
  xAxis: {
    tickCount: 5,
  },
};

const cpuConfig = {
  xField: 'date',
  yField: 'cpu',
  xAxis: {
    tickCount: 5,
  },
};

const ProjectUsageChart = ({project, socket}: ProjectUsageChartProps) => {
  const [processChart, setProcessChart] = useState<ProcessUsage[]>([]);

  useEffect(() => {
    (async () => {
      const initialChart = (await ApiService.getProcessUsages(project.id)).map(data => {
        return {
          date: moment(data.createdAt).utcOffset('+0700').format("YYYY-MM-DD HH:mm:ss"),
          cpu: data.cpu,
          ram: data.mem
        }
      });

      setProcessChart(initialChart);

      socket.emit("startProcessUsage", project._id);

      socket.on('updateProcessChart', (processUsages) => {
        // console.log("Socket process usages--", processUsages);
        setProcessChart(processUsages);
      });

      return () => {
        socket.off('updateProcessChart');
      };
    })()
  }, []);

  return <>
    <Card>
      <h1>Memory</h1>
      <Line {...ramConfig} data={processChart}/>
    </Card>

    <Card>
      <h1>CPU</h1>
      <Line {...cpuConfig} data={processChart}/>
    </Card>
  </>;
};

export default ProjectUsageChart;
