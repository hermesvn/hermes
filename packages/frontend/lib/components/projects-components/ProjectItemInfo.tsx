import { Tag, Tooltip } from "antd";
import Flex from "../shared-components/Flex";
import {
	PaperClipOutlined,
	CheckCircleOutlined,
	ClockCircleOutlined, GitlabOutlined, DashboardOutlined,
} from '@ant-design/icons';

export const ProjectItemInfo = ({completedTask, usageText, statusColor, uptime, project}: any) => (
	<Flex alignItems="center">
		<div className="mr-3">
			<Tooltip title="Usage Details">
				<DashboardOutlined className="text-muted font-size-md"/>
				<span className="ml-1 text-muted">{usageText}</span>
			</Tooltip>
		</div>
		<div>
		<Tag className={statusColor === "none"? 'bg-gray-lightest' : ''} color={statusColor !== "none"? statusColor : ''}>
			<ClockCircleOutlined />
			<span className="ml-2 font-weight-semibold">{uptime} hours</span>
		</Tag>
		</div>
	</Flex>
)