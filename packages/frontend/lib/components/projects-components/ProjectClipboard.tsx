import {Input} from "antd";
import React, {useState} from "react";
import {CopyOutlined, CopyFilled, CheckOutlined} from "@ant-design/icons";

const ProjectClipboard = ({icon, link}) => {
  const [onHover, setOnHover] = useState(false);
  const [isCopied, setIsCopied] = useState(false);

  return <Input
    disabled
    value={link}
    addonBefore={icon}
    className={"mb-2"}
    addonAfter={
      <a
        // href=""
        onMouseEnter={(e) => {
          e.preventDefault();
          setOnHover(true)
        }}
        onMouseLeave={(e) => {
          e.preventDefault();
          setOnHover(false)
        }}
        onClick={(e: any) => {
          e.preventDefault();
          setIsCopied(true);
          navigator.clipboard.writeText(link).then(_ => {
            setTimeout(_ => {
              setIsCopied(false);
            }, 1000);
          });
        }}>{
        (!isCopied) ? (!onHover) ? <CopyOutlined style={{color: "#404040"}}/> :
          <CopyFilled style={{color: "#404040"}}/> : <CheckOutlined style={{color: "#00fa5c"}}/>
      }</a>
    }
  />
}

export default ProjectClipboard;