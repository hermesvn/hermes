import React, {Fragment, useEffect, useState} from "react";
import {Alert, Button, Card, Divider, Input, message, Modal} from "antd";
import {NextPage} from "next";
import {DeploymentTypeSelector} from "@lib/components/gitlab-connect/deployment-type-selector";
import {NginxConfig} from "@lib/components/app-components/NginxConfig";
import {EnvEditor} from "@lib/components/app-components/EnvEditor";
import {BranchPicker} from "@lib/components/gitlab-connect/branch-picker";
import {ConfigScripts} from "@lib/components/app-components/ConfigScripts";
import {ApiService} from "@lib/services/ApiService";
import {GitLabProjectDetail} from "@lib/components/gitlab-connect/project-detail";
import {EyeInvisibleOutlined, EyeTwoTone} from '@ant-design/icons';
import CloneOptions from "@lib/components/account-components/CloneOptions";

interface Props {
  mode: SettingMode,
  account?: Account,
  project?: Project,
  defaultGitCredentials?: GitCredentials,
  setting?: ProjectSettingTypes
}

interface SettingCardProp {
  title: string,
  description: string,
  component: JSX.Element;
  confirmModal?: JSX.Element;
}

interface SendRequestPayload {
  success: string,
  error?: string,
  callback: (...props) => Promise<any>
  redirect?: string
}

type SendRequestType = "updateProject" | "updateAccount" | "updateCredentials";

export const SettingCards = ({cards}: { cards: () => SettingCardProp[] }) => {
  const settings = cards()

  return (
    <>
      {settings && settings.map((data: SettingCardProp, index) => {
        return (
          <Fragment key={index}>
            <Card>
              <h3>{data?.title}</h3>
              <p className="mt-3 mb-4">{data?.description}</p>
              {data?.component}
            </Card>
            {data.confirmModal}
          </Fragment>
        )
      })}
    </>
  );
};

export const ConfirmationModal =
  ({
     title,
     warningMessage,
     modalVisible = false,
     onOk,
     onCancel,
     confirmContent,
     onInputChange,
     inputType,
     isActionConfirm = false
   }) => {
    return (
      <Modal
        title={
          <div style={{textAlign: "center"}}>
            <h2 className="mt-3">{title}</h2>
            <p style={{fontSize: 16}}>{warningMessage}</p>
            <Alert
              showIcon={true}
              style={{fontSize: 14}}
              message={<p className="mt-3" style={{color: "#f7483b", textAlign: "left"}}>Warning: This action is not
                reversible. Please be certain.</p>}
              type="error"
            />
          </div>

        }
        visible={modalVisible}
        onOk={() => onOk()}
        onCancel={() => onCancel()}
        okButtonProps={{danger: true, disabled: !isActionConfirm}}
        width={600}
      >
        <div style={{fontSize: 16}}>
          <p>{confirmContent}</p>
          {inputType === "text" ?
            <Input onChange={(e) => onInputChange(e)}></Input> :
            <Input.Password
              onChange={(e) => onInputChange(e)}
              iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
            ></Input.Password>}
        </div>
      </Modal>
    )
  }

const SettingSections: NextPage<Props> = ({mode, account, defaultGitCredentials, project, setting}) => {
  const [updatedAccount, setUpdatedAccount] = useState<Account>(account);
  const [updatedProject, setUpdatedProject] = useState<Project>(project);
  const [scripts, setScripts] = useState<DeploymentScripts>({});
  const [isModalActionConfirm, setIsModalActionConfirm] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [isNewPasswordValid, setIsNewPasswordValid] = useState<boolean>(setting === "password");
  const [disableSaveBtn, setDisableSaveBtn] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [sendRequestType, setSendRequestType] = useState<SendRequestType>();

  const sendRequest = (payload: SendRequestPayload) => {
    setLoading(!loading);
    payload?.callback().then(async () => {
      message.success(payload?.success || "Sent request successfully");
      if (typeof payload?.redirect === "undefined") location.reload()
      else location.href = payload.redirect;
    }).catch(async e => {
      setLoading(false);
      message.error(payload?.error || e.message);
    });
  };

  const onCancel = () => {
    setModalVisible(false);
  }

  let onInputChange, onConfirm;

  useEffect(() => {
    if (mode === "project" && project) {
      setUpdatedProject(project)
    } else if (mode === "account" && account) {
      setUpdatedAccount(account);
    }
  }, []);

  const settingCardSections = () => {
    switch (setting) {
      case "general":
        if (mode === "project") {
          setSendRequestType("updateProject");
          onConfirm = () => {
            setModalVisible(false);
            sendRequest({
              success: "Delete project successfully",
              callback: () => ApiService.deleteProject(project?.id),
              redirect: "/"
            });
          };

          onInputChange = (e) => {
            setIsModalActionConfirm(e.target.value === project.name);
          }

          return ([{
            title: "Project Name",
            description: "Used to identify your Project on the Dashboard, CLI, and in the URL of your Deployments.",
            component:
              <Input
                className="mb-3"
                addonBefore="Name"
                placeholder={"Insert name..."}
                onChange={(e) => {
                  setUpdatedProject(prevState => ({...prevState, name: e.target.value}));
                }}
                value={updatedProject?.name}
              />
          }, {
            title: "Build & Development Settings",
            description: "When using a framework for a new project, it will be automatically detected. As a result, several project settings are automatically configured to achieve the best result. You can override them below.",
            component: <>
              <DeploymentTypeSelector
                defaultValue={project.type}
                onChange={(e => {
                  setUpdatedProject(prevState => ({...prevState, type: e}));
                })}
              />
              <Divider dashed={true}/>
              <ConfigScripts
                onStartupScriptChange={script => setScripts({...scripts, startup: script})}
                onBuildScriptChange={script => setScripts({...scripts, build: script})}
                onInstallScriptChange={script => setScripts({...scripts, install: script})}
              />
            </>
          }, {
            title: "Delete Project",
            description: "The project will be permanently deleted, including its deployments and domains. This action is irreversible and can not be undone.",
            component: <Button danger onClick={() => setModalVisible(!modalVisible)}>Delete</Button>,
            confirmModal: <ConfirmationModal
              title="Delete Project"
              warningMessage={"This project will be deleted permanently, along with all of its Deployments,\nDomains and Environment Variables."}
              modalVisible={modalVisible}
              onOk={onConfirm}
              onCancel={onCancel}
              confirmContent={["Enter the project name ",
                <strong key={"project-name"}>{project?.name}</strong>, " to continue:"]}
              onInputChange={onInputChange}
              inputType="text"
              isActionConfirm={isModalActionConfirm}
            />
          }]);
        } else if (mode === "account") {
          setSendRequestType("updateAccount");
          return ([{
            title: "Your Username",
            description: "Used to identify your Project on the Dashboard, CLI, and in the URL of your Deployments.",
            component:
              <Input
                className="mb-3"
                addonBefore="Name"
                placeholder={"Insert name..."}
                onChange={(e) => {
                  setUpdatedAccount(prevState => ({...prevState, username: e.target.value}));
                }}
                value={updatedAccount?.username}
              />
          }]);
        }
        break;
      case "password":
        setSendRequestType("updateAccount");
        return ([{
          title: "Password",
          description: "Change your password or recover your current one.",
          component:
            <div className="d-flex  flex-1 ant-row">
              <p style={{color: "black", fontWeight: "bold"}}>Current Password</p>
              <Input.Password
                className="mb-3"
                placeholder={"Insert current password..."}
                onChange={(e) => {
                  Object.assign(updatedAccount, {currentPassword: e.target.value})
                  setUpdatedAccount(updatedAccount);
                }}
                iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                value={updatedAccount.currentPassword}
              />

              <p style={{color: "black", fontWeight: "bold"}}>New Password</p>
              <Input.Password
                className="mb-3"
                placeholder={"Insert new password..."}
                onChange={(e) => {
                  setUpdatedAccount(prevState => ({...prevState, password: e.target.value}));
                }}
                iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                value={updatedAccount.password}
              />
              <p style={{color: "black", fontWeight: "bold"}}>Confirm Password</p>
              <Input.Password
                className="mb-3"
                placeholder={"Insert confirm password..."}
                iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                onChange={(e) => {
                  // console.log("New password ", e.target.value, " Current password ", updatedAccount.password, e.target.value.length > 0, e.target.value === updatedAccount.password)
                  if (e.target.value.length > 0 && e.target.value === updatedAccount.password) {
                    setIsNewPasswordValid(false);
                  }
                }}
              />
            </div>
        }]);
      case "credentials":
        setDisableSaveBtn(true);
        setSendRequestType("updateCredentials");

        onConfirm = () => {
          setModalVisible(false);
          sendRequest({
            success: "Update git credentials successfully",
            callback: () => ApiService.updateGitCredentials(account?._id, {
              ...updatedAccount.gitCredentials,
              currentPassword: updatedAccount.currentPassword,
            }),
          });
        };
        onInputChange = (e) => {
          setUpdatedAccount((prevState) => ({...prevState, currentPassword: e.target.value}));
          setIsModalActionConfirm(e.target.value);
        }

        return ([{
          title: "GitLab Clone Options",
          description: "Add credentials for cloning project from GitLab.",
          component:
            <div>
              <CloneOptions mode={"system"} defaultGitCredentials={defaultGitCredentials} onChange={(data) => {
                console.log("gitCredentials", data)
                setUpdatedAccount((prevState) => ({
                  ...prevState, gitCredentials: {
                    username: data.username,
                    password: data.password,
                    privateKeyPath: data.privateKeyPath,
                    publicKeyPath: data.publicKeyPath,
                    sshUsername: data.sshUsername,
                    passPhrase: data.passPhrase
                  }
                }))
              }}/>
              <Button className={"mt-3"} danger onClick={() => setModalVisible(!modalVisible)}>Update
                Credentials</Button>,
            </div>,
          confirmModal: <ConfirmationModal
            title="Update Credential for Cloning Option"
            warningMessage={"Please make sure that all provided credentials are correct, all wrong configurations would lead to system error."}
            modalVisible={modalVisible}
            onOk={onConfirm}
            onCancel={onCancel}
            confirmContent={["Enter the your ",
              <strong key={"account-password"}>account&apos;s password</strong>, " to continue:"]}
            onInputChange={onInputChange}
            inputType="password"
            isActionConfirm={isModalActionConfirm}
          />
        }]);
      case "git":
        setSendRequestType("updateProject");
        return ([{
          title: "Clone Options",
          description: "By default, every commit pushed to the main branch will trigger a Production Deployment instead of the usual Preview Deployment. You can switch to a different branch here.",
          component:
            <CloneOptions
              defaultProject={updatedProject}
              onChange={(data) => {
                setUpdatedProject((prevState) => ({
                  ...prevState, autoDeploy: {
                    ...prevState.autoDeploy,
                    cloneOption: data.name
                  }
                }))
              }}/>
        }, {
          title: "Connected Git Repository",
          description: "Seamlessly create Deployments for any commits pushed to your Git repository.",
          component:
            <GitLabProjectDetail project={updatedProject}/>
        }, {
          title: "Production Branch",
          description: "By default, every commit pushed to the main branch will trigger a Production Deployment instead of the usual Preview Deployment. You can switch to a different branch here.",
          component:
            <BranchPicker
              project={updatedProject}
              defaultBranch={project.autoDeploy.branch}
              onChange={(branch) => {
                Object.assign(updatedProject, {branch});
                Object.assign(updatedProject.autoDeploy, {branch});
                setUpdatedProject(updatedProject);
              }}
            />
        }]);
      case "environment-variables":
        setSendRequestType("updateProject");
        return ([{
          title: "Environment Variables",
          description: "In order to provide your Deployment with Environment Variables at Build and Runtime, you may enter them right here, for the Environment of your choice. A new Deployment is required for your changes to take effect.",
          component:
            <EnvEditor
              defaultValue={project.environmentVariablesAsString}
              onChange={envPairs => {
                Object.assign(updatedProject.autoDeploy, {
                  environmentVariables: Object.keys(envPairs).map(data => {
                    return {
                      key: data,
                      value: envPairs[data]
                    }
                  })
                });
                setUpdatedProject(updatedProject);
              }}
            />
        }])
      case "nginx-config":
        setSendRequestType("updateProject");
        console.log((project.autoDeploy.isCustomDomain) ? project.autoDeploy.domain : project.autoDeploy.domain.split(`.${process.env.NEXT_PUBLIC_HOST_DOMAIN}` || ".northstudio.dev")[0])
        return ([{
          title: "Nginx & Domain Config",
          description: "These domains are assigned to your Production Deployments. You can also customize Nginx config.",
          component:
            <NginxConfig
              defaultValue={{
                domain: (project.autoDeploy.isCustomDomain) ? project.autoDeploy.domain : project.autoDeploy.domain.split(`.${process.env.NEXT_PUBLIC_HOST_DOMAIN}` || ".northstudio.dev")[0],
                nginxSupport: project.autoDeploy.nginxSupport,
                nginxConfig: project.autoDeploy.nginxConfig,
                isCustomDomain: project.autoDeploy.isCustomDomain
              }}
              onChange={nginxSupport => {
                Object.assign(updatedProject.autoDeploy, nginxSupport)
                setUpdatedProject(updatedProject);
              }}
            />
        }]);
    }
  }


  useEffect(() => {
    setUpdatedProject({
      ...updatedProject,
      scripts
    });
  }, [scripts]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <SettingCards
        cards={settingCardSections}
      />
      <Button
        loading={loading}
        block
        disabled={isNewPasswordValid}
        style={{display: (disableSaveBtn) ? "none" : ""}}
        type={"primary"}
        size={"large"}
        onClick={() => {
          let sendRequestPayload;
          if (sendRequestType === "updateProject") {
            console.log("updatedProject", updatedProject)
            sendRequestPayload = ({
              success: "Updated project successfully",
              callback: () => ApiService.updateProject(updatedProject.id, updatedProject)
            });
          } else if (sendRequestType === "updateAccount") {
            sendRequestPayload = ({
              success: "Updated account successfully",
              callback: () => ApiService.updateAccount(updatedAccount._id, {
                username: updatedAccount.username,
                password: updatedAccount.password,
                currentPassword: updatedAccount.currentPassword,
                email: updatedAccount.email,
              })
            });
          } else if (sendRequestType === "updateCredentials") {
            sendRequestPayload = ({
              success: "Updated account successfully",
              callback: () => ApiService.updateAccount(updatedAccount._id, updatedAccount)
            });
          }

          sendRequest(sendRequestPayload);
        }}
      >
        Save Changes
      </Button>
    </div>
  );
};

export default SettingSections;
