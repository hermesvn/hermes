import {Col, Row, Modal} from "antd";
import React, {useEffect, useState} from "react";
import {ProjectGridItem} from "./ProjectGridItem";
import {ProjectListItem} from "./ProjectListItem";
import {ApiService} from "@lib/services/ApiService";
import {WarningOutlined} from "@ant-design/icons";

const VIEW_LIST = "list", VIEW_GRID = "grid";
type ViewType = "list" | "grid";

interface ProjectsListProps {
  view: ViewType,
}

export const ProjectsList = ({view}: ProjectsListProps) => {
  const [list, setList] = useState<Project[]>([]);
  const [visible, setVisible] = useState(false)
  const [projectId, setProjectId] = useState("")
  useEffect(() => {
    ApiService.getProjects().then(projects => setList(projects));
  }, []);

  const showPromiseConfirm = (id) => {
    setVisible(true);
    setProjectId(id);
  };

  const onConfirm = () => {
    setVisible(false);
    ApiService.deployProject(projectId)
      .then(() => {
        location.href = `projects/${projectId}`
      }).catch(e => {
      console.log(e)
      // message.error().then(r => {
      // });
    });
  };

  return (
    <div className={`my-4 ${view === VIEW_LIST ? 'container' : 'container-fluid'}`}>

      {view === VIEW_LIST ? list.map(elm => (
        <ProjectListItem data={elm} removeId={(id) => showPromiseConfirm(id)} key={elm.id}/>
      )) : (
        <Row gutter={16}>
          {list.map(elm => (
            <Col xs={24} sm={24} lg={8} xl={8} xxl={6} key={elm.id}>
              <ProjectGridItem data={elm} removeId={(id) => showPromiseConfirm(id)}/>
            </Col>
          ))}
        </Row>
      )}
    </div>
  )
}
