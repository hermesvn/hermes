import React from "react";
import {List} from "antd";
import Link from "next/link";
import {useRouter} from "next/router";

interface Props {
  mode: SettingMode,
  pathname: string,
  targetId: string,
  settingSections: {
    title: string,
    path: string
  }[]
}

const SettingList = ({mode, pathname, targetId, settingSections}: Props) => {
  const router = useRouter();
  return (
    <List
      style={{
        position: "sticky",
        top: 150
      }}
      itemLayout="horizontal"
      dataSource={settingSections}
      split={false}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={
              <div>
                <Link href={pathname.replace(`[${mode}Id]`, targetId).replace("[setting]", item.path)}>
                  <a><h4 style={{color: (router.query?.setting === item.path) ? "#101010" : "rgba(21,21,21,0.7)"}}>{item.title}</h4></a>
                </Link>
              </div>
            }
          />
        </List.Item>
      )}
    />
  )
};

export default SettingList;

