import React from "react"
import { Col, Row } from "antd"
import { ProjectItemAction } from "./ProjectItemAction"
import { ProjectItemHeader } from "./ProjectItemHeader"
import { ProjectItemInfo } from "./ProjectItemInfo"
import { ProjectItemProgress } from "./ProjectItemProgress"
import Link from "next/link";

export const ProjectListItem = ({ data, removeId }: any) => (
	<div className="bg-white rounded p-3 mb-3 border">
		<Row align="middle">
      {/* eslint-disable-next-line no-mixed-spaces-and-tabs */}
    	<Col xs={24} sm={24} md={8}>
				<Link href={`/projects/${data._id}/`}>
					<a href={`/projects/${data._id}/`}>
						<ProjectItemHeader name={data.name} category={data.category} />
					</a>
				</Link>
			</Col>
			<Col xs={24} sm={24} md={6}>
				<ProjectItemInfo
					attachmentCount={data.attachmentCount}
					completedTask={data.completedTask}
					totalTask={data.totalTask}
					statusColor={data.statusColor}
					dayleft={data.dayleft}
				/>
			</Col>
			<Col xs={24} sm={24} md={5}>
				<ProjectItemProgress progression={data.progression} />
			</Col>
			<Col xs={24} sm={24} md={2}>
				<div className="text-right">
					<ProjectItemAction id={data.id} deploy={removeId}/>
				</div>
			</Col>
		</Row>
	</div>
)
