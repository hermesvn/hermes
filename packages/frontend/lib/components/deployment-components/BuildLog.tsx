import React from "react";
import {Collapse, Table} from "antd";

const {Panel} = Collapse;


const columns = [
  {
    dataIndex: 'date',
    width: 100,
  },
  {
    dataIndex: 'content',
  },
];

const BuildLog = ({itemList}) => {
  // const isServer = (typeof window === 'undefined')? false : true;

  const onChange = (key: string | string[]) => {
    // console.log(key);
  };

  return (
    <>

      <Collapse defaultActiveKey={['1']} onChange={onChange}>
        <Panel header="Building Logs" key="1">

          <Table showHeader={false} columns={columns} dataSource={itemList} pagination={false}/>

        </Panel>
        <Panel header="Performance Checks" key="2">
          <p>Test</p>
        </Panel>
        {/*<Panel header="Assigning Domains" key="3">*/}
        {/*  <p>Test</p>*/}
        {/*</Panel>*/}
      </Collapse>

    </>
  )
}

export default BuildLog;
