import React from 'react'
import Icon from '@ant-design/icons';

interface CustomIconProps {
  svg: any,
  className?: string,
}

const CustomIcon = React.forwardRef((props: CustomIconProps, _) => <Icon component={props.svg}
                                                                         className={props.className}/>)
CustomIcon.displayName = "CustomIcon";
export default CustomIcon
