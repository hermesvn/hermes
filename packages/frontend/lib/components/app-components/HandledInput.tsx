import {Input, InputProps, Switch} from "antd";
import React, {useEffect, useRef, useState} from "react";

interface HandledInputProps extends InputProps {
  initialValue: string,
  onValueChange: (value: string) => void,
  enabled?: boolean
}

export const HandledInput = ({addonBefore, value, onValueChange, placeholder, className, initialValue, enabled = false}: HandledInputProps) => {
  const [disabled, setDisabled] = useState(!enabled);
  const inputRef = useRef<any>(null);

  useEffect(() => {
    if (!disabled) return;
    onValueChange(initialValue);
  }, [disabled]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className={"d-flex flex-row " + className}>
      <Input
        ref={inputRef}
        disabled={disabled}
        addonBefore={addonBefore}
        value={value}
        onChange={(e) => onValueChange(e.target.value)}
        placeholder={placeholder}
      />
      <div className={"d-flex align-items-center justify-content-end ml-8"}>
        Override?
        <Switch checked={!disabled} className={"ml-2"} onChange={(checked) => setDisabled(!checked)}/>
      </div>
    </div>
  )
}
