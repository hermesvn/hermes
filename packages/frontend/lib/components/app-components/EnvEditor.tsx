import React, {useEffect, useState} from "react";
import {Alert, Button, Col, Input, Modal, Row, Space, Table} from "antd";
import {EditOutlined, DeleteOutlined} from "@ant-design/icons";

interface ClassicEditorProps {
  value: string,
  onChange: (content: string) => void
}

interface PairEditorProps {
  envPairs: EnvVariables,
  onPairAdded: (pair: any) => void,
  onPairEdited: (pair: any) => void,
  onPairDeleted: (pair: any) => void
}

const ClassicEditor = ({value, onChange}: ClassicEditorProps) => {
  return (
    <Input.TextArea
      placeholder={"Environment Variable content..."}
      onChange={(e) => {
        onChange(e.target.value);
      }}
      style={{height: 200}}
      value={value}
    />
  )
};

const PairEditor = ({envPairs, onPairAdded, onPairEdited, onPairDeleted}: PairEditorProps) => {
  const [rows, setRows] = useState<any[]>([]);
  const [varName, setVarName] = useState<string>('');
  const [varValue, setVarValue] = useState<string>('');
  const [modalMode, setModalMode] = useState<string>('edit');
  const [envEditModal, setEnvEditModal] = useState<boolean>(false);
  const [editedEnv, setEditedEnv] = useState<{ field: string, key: string, value: string }>({
    field: "",
    key: "",
    value: ""
  })
  useEffect(() => {
    // console.log("pairs-changed", envPairs);
    const newRows: any[] = [];
    for (const field in envPairs) {
      newRows.push({
        key: field,
        field: field,
        value: envPairs[field]
      });
    }
    setRows(newRows);
  }, [envPairs]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <>
      <Row gutter={16}>
        <Col span={12}>
          <span><strong>Key</strong></span>
          <Input
            className="mt-2"
            value={varName}
            placeholder={"EXAMPLE_KEY"}
            onChange={(e) => setVarName(e.target.value)}
          />
        </Col>
        <Col span={12}>
          <span><strong>Value</strong></span>
          <Input
            className="mt-2"
            value={varValue}
            placeholder={"EXAMPLE_VALUE"}
            onChange={(e) => setVarValue(e.target.value)}
          />
        </Col>
      </Row>
      <div className={"my-2 d-flex align-items-end justify-content-between"}>
        <span>
          <a target="_blank" rel="noreferrer" className="ml-1"
             href="https://en.wikipedia.org/wiki/Environment_variable#:~:text=An%20environment%20variable%20is%20a,in%20which%20a%20process%20runs.">Find more about environment variables?</a>
        </span>
        <Button type={"primary"} size={"small"} onClick={() => {
          setVarName('');
          setVarValue('');
          onPairAdded({
            [varName]: varValue
          });
        }}>
          Add
        </Button>
      </div>

      <Modal
        title={(modalMode === "edit") ? "Edit environment variable" : "Delete environment variable"}
        centered
        visible={envEditModal}
        onOk={() => {
          setEnvEditModal(!envEditModal);
          if (modalMode === "edit") {
            onPairEdited(editedEnv);
          } else {
            onPairDeleted(editedEnv);
          }
        }}
        onCancel={() => setEnvEditModal(!envEditModal)}
      >
        {(modalMode === "edit")
          ?
          <>
            <strong>Key</strong>
            <Input
              className="mt-2 mb-4"
              value={editedEnv.key}
              placeholder={"EXAMPLE_NAME"}
              onChange={(e) => setEditedEnv({...editedEnv, key: e.target.value})}
            />
            <strong>Value</strong>
            <Input
              className="mt-2"
              value={editedEnv.value}
              placeholder={"EXAMPLE_NAME"}
              onChange={(e) => setEditedEnv({...editedEnv, value: e.target.value})}
            />
          </>
          :
          <>
            <Alert
              showIcon={true}
              style={{fontSize: 14}}
              message={<p className="mt-3" style={{color: "#f7483b", textAlign: "left"}}>Warning: This action is not
                reversible. Please be certain.</p>}
              type="error"
            />
          </>}

      </Modal>

      <Table
        className={"mt-4"}
        columns={[{
          title: 'Name',
          dataIndex: 'field',
          key: 'field'
        }, {
          title: 'Value',
          dataIndex: 'value',
          key: 'value',
          render: (text, record) => (
            <div style={{wordWrap: 'break-word', wordBreak: 'break-word'}}>
              {text}
            </div>
          ),
        }, {
          title: 'Action',
          key: 'action',
          width: 72,
          render: (_, item) => (
            <Space size="small">
              <Button size={"small"} onClick={() => {
                setEditedEnv(item);
                setModalMode("edit");
                setEnvEditModal(!envEditModal);
              }}>
                <EditOutlined/>
              </Button>
              <Button size={"small"} onClick={() => {
                setEditedEnv(item);
                setModalMode("delete");
                setEnvEditModal(!envEditModal);
              }}>
                <DeleteOutlined/>
              </Button>
            </Space>
          ),
        }]}
        dataSource={rows}
      />
    </>
  )
}

interface EnvEditorProps {
  onChange: (value: EnvVariables) => void,
  defaultValue?: string,
}

export const EnvEditor = ({onChange, defaultValue = ''}: EnvEditorProps) => {
  const [useClassic, setUseClassic] = useState(false);
  const [envPairs, setEnvPairs] = useState<EnvVariables>({});
  const [envString, setEnvString] = useState<string>(defaultValue);

  const convertToPairs = (): EnvVariables => {
    const pairs: EnvVariables = {};
    envString.split('\n').sort().map((line: string) => {
      if (line.includes('=')) {
        const parts = line.split('=');
        if (parts[0].trim() !== "" && parts[1].trim() !== "")
          pairs[parts[0].trim()] = line.substring(line.indexOf('=') + 1);
      }
    });
    return pairs;
  };
  useEffect(() => {
    if (useClassic) {
      // convert pairs to classic
      let pairString = '';
      for (const field in envPairs) {
        pairString += `${field}=${envPairs[field]}\n`;
      }
      setEnvString(pairString);
    } else {
      // convert classic text to pairs
      setEnvPairs(convertToPairs());
    }
  }, [useClassic]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (useClassic) {
      onChange(convertToPairs());
    } else {
      onChange(envPairs);
    }
  }, [envString, envPairs]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <div className={"d-flex justify-content-between mb-3"}>
        <h4>Edit Environment Variables</h4>
        <a onClick={() => setUseClassic(!useClassic)}>{useClassic ? "Key-pair Editor" : "Bulk Editor"}</a>
      </div>
      <>
        {useClassic ? (
          <ClassicEditor
            value={envString}
            onChange={(value) => {
              if (useClassic) setEnvString(value);
            }}
          />
        ) : (
          <PairEditor
            envPairs={envPairs}
            onPairAdded={(obj) => {
              const pairs = {...envPairs};
              for (const field in obj) pairs[field] = obj[field];
              setEnvPairs(pairs);
            }}
            onPairEdited={(obj) => {
              const pairs = {...envPairs};
              delete pairs[obj.field];
              pairs[obj.key] = obj.value;

              const newPairs = Object.keys(pairs)
                .sort()
                .reduce((acc, key) => ({
                  ...acc, [key]: pairs[key]
                }), {});

              setEnvPairs(newPairs);
            }}
            onPairDeleted={(obj) => {
              const pairs = {...envPairs};
              delete pairs[obj.field];
              setEnvPairs(pairs);
            }}
          />
        )}
      </>
    </>
  )
};
