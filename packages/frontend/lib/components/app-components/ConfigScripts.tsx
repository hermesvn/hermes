import React, {useEffect, useState} from "react";
import {HandledInput} from "@lib/components/app-components/HandledInput";

interface ConfigScriptsProps {
  onStartupScriptChange?: (script: string) => void,
  onInstallScriptChange?: (script: string) => void,
  onBuildScriptChange?: (script: string) => void
}

const scriptInitialValues = {
  install: 'yarn install',
  start: 'yarn start',
  build: 'yarn build'
}

export const ConfigScripts = ({onStartupScriptChange, onInstallScriptChange, onBuildScriptChange}: ConfigScriptsProps) => {
  const [startupScript, setStartupScript] = useState("yarn start");
  const [installScript, setInstallScript] = useState("yarn install");
  const [buildScript, setBuildScript] = useState("yarn build");

  useEffect(() => {
    if (typeof onStartupScriptChange === "function") {
      onStartupScriptChange(startupScript);
    }
  }, [startupScript]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (typeof onInstallScriptChange === "function") {
      onInstallScriptChange(installScript);
    }
  }, [installScript]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (typeof onBuildScriptChange === "function") {
      onBuildScriptChange(buildScript);
    }
  }, [buildScript]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <h4>Customize Scripts</h4>
      <p>
        Customize which commands will be executed once the program on different stages.
      </p>
      {!!onInstallScriptChange && (
        <HandledInput
          initialValue={scriptInitialValues.install}
          addonBefore={<div style={{width: 100, textAlign: "left"}}>
            Install deps
          </div>}
          value={installScript}
          onValueChange={value => setInstallScript(value)}
          placeholder={"Install scripts..."}
        />
      )}
      {!!onBuildScriptChange && (
        <HandledInput
          className={"mt-2"}
          initialValue={scriptInitialValues.build}
          addonBefore={<div style={{width: 100, textAlign: "left"}}>
            Build
          </div>}
          value={buildScript}
          onValueChange={value => setBuildScript(value)}
          placeholder={"Build script..."}
        />
      )}
      {!!onStartupScriptChange && (
        <HandledInput
          className={"mt-2"}
          initialValue={scriptInitialValues.start}
          addonBefore={<div style={{width: 100, textAlign: "left"}}>
            Startup
          </div>}
          value={startupScript}
          onValueChange={value => setStartupScript(value)}
          placeholder={"Startup script..."}
        />
      )}
    </>
  )
}
