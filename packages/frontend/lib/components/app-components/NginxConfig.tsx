import React, {useState} from "react";
import {Checkbox, Input, Switch} from "antd";

interface Props {
  defaultValue?: { domain?: string, nginxConfig?: string, nginxSupport?: boolean, isCustomDomain?: boolean },
  onChange: any
}

export const NginxConfig = ({
                              defaultValue = {domain: "", nginxConfig: "", nginxSupport: false, isCustomDomain: false},
                              onChange
                            }: Props) => {
  const [enableNginxSupport, setEnableNginxSupport] = useState(defaultValue.nginxSupport);
  const [domain, setDomain] = useState(defaultValue.domain);
  const [nginxConfig, setNginxConfig] = useState(defaultValue.nginxConfig);
  const [isCustomDomain, setIsCustomDomain] = useState<boolean>(defaultValue.isCustomDomain);

  const onCheckboxChange = (e) => {
    setIsCustomDomain(!isCustomDomain);
    onChange({isCustomDomain: !isCustomDomain});
  };

  return (
    <>
      <div className="d-flex flex-row align-items-center">
        <div className={"d-flex justify-content-between"}>
          <h4>Nginx Config</h4>
        </div>
        <div className="mb-2 p-2">
          <Switch checked={enableNginxSupport} className={"ml-2"} onChange={(checked) => {
            setEnableNginxSupport(checked);
            onChange({nginxSupport: checked, domain, nginxConfig, isCustomDomain})
          }}/>
        </div>
      </div>
      {(!enableNginxSupport) ? <></> : <div>
        <div className={"d-flex flex-row "}>
          <Input
            className="mb-3"
            addonBefore="Domain"
            addonAfter={(!isCustomDomain) ? (process.env.NEXT_PUBLIC_HOST_DOMAIN) ? `.${process.env.NEXT_PUBLIC_HOST_DOMAIN}` : ".northstudio.dev" : ""}
            placeholder={"Insert domain..."}
            onChange={(e) => {
              setDomain(e.target.value);
              onChange({domain: e.target.value});
            }}
            value={domain}
            required={true}
          />

        </div>
        <Checkbox checked={isCustomDomain} className="ml-3 mb-4" onChange={onCheckboxChange}>Custom domain</Checkbox>

        <Input.TextArea
          disabled={!enableNginxSupport}
          placeholder={"Insert custom Nginx config..."}
          onChange={(e) => {
            setNginxConfig(e.target.value);
            onChange({nginxConfig: e.target.value});
          }}
          value={nginxConfig}
          style={{height: 200}}
        />
      </div>}
    </>
  )
};
