export const setData = (key: string, value: any) => {
  if (typeof window === "undefined") return;
  return window.localStorage.setItem(key, JSON.stringify(value));
}

export const getData = (key: string, defaultValue: any) => {
  if (typeof window === "undefined") return;
  let stored = window.localStorage.getItem(key);
  if (stored === null) return defaultValue;
  try {
    return JSON.parse(stored);
  } catch (e) {
    return defaultValue;
  }
}
