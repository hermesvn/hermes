import axios, {AxiosResponse} from "axios";
import {IronSessionData} from "iron-session";
import moment from "moment";

export class ApiService {
  static baseUrl = typeof window !== "undefined" ? "/api" : (process.env.API_BASE || `http://localhost:3333/api`);
  static accessToken = ``;

  static setBaseUrl(baseUrl: string = (process.env.NODE_ENV === "development") ? `http://localhost:${process.env.PORT}` : `${process.env.API_BASE}`) {
    this.baseUrl = baseUrl;
  }

  static async setCredentialsWithSession(req: any) {
    const credentials = req.session.credentials;
    if (credentials) {
      this.accessToken = credentials.accessToken;
      if ((credentials.accessTokenExpiresTime) <= moment().format("x")) {
        try {
          const data = await this.refreshToken(credentials.refreshToken);
          req.session.credentials = data.credentials;
          await req.session.save();
          await this.setCredentialsWithSession(req);
        } catch (e) {
          console.log("Error occurred while refreshing token", e);
          await req.session.destroy();
        }
      }
    }
  }

  static sendRequest(reqOptions): Promise<AxiosResponse> {
    const headers = {
      ...reqOptions.headers
    };
    if (this.accessToken) {
      headers.Authorization = `Bearer ${this.accessToken}`;
    }
    const attachedOptions = {
      ...reqOptions,
      headers,
    };
    return new Promise((resolve, reject) => {
      axios(attachedOptions).then(resolve).catch(e => {
        reject((e.response.data.message) ? {message: e.response.data.message} : {})
      });
    });
  }

  static async getAccount(accountId: string): Promise<Account> {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/accounts/${accountId}`
    });
    return data.data;
  }

  static async updateAccount(accountId: string, updatedAccount: Account) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/accounts/${accountId}`,
      data: updatedAccount
    });
    return data.data;
  }

  static async getGitCredentials(accountId: string): Promise<Project> {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/accounts/${accountId}/git-credentials`
    });
    return data.data;
  }

  static async updateGitCredentials(accountId: string, gitCredentials: GitCredentials) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/accounts/${accountId}/git-credentials`,
      data: gitCredentials
    });
    return data.data;
  }

  static async getProject(projectId: string): Promise<Project> {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/projects/${projectId}`
    });
    return data.data;
  }

  static async getProjects(): Promise<Project[]> {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/projects`
    });
    return data.data;
  }

  static async createProject(newProject: Project) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/projects`,
      data: newProject
    });
    return data.data;
  }

  static async updateProject(projectId: string, updatedProject: Project) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/projects/${projectId}`,
      data: updatedProject
    });
    return data.data;
  }

  static async deployProject(projectId: string, deployOptions: ProjectDeployOptions = {}) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/projects/${projectId}/deploy`,
      data: deployOptions
    });
    return data.data;
  }

  static async getProcessUsages(projectId: string) {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/projects/${projectId}/process-usages`,
      // data: deployOptions
    });
    return data.data;
  }

  static async deleteProject(projectId: string) {
    const {data} = await this.sendRequest({
      method: "DELETE",
      url: `${this.baseUrl}/projects/${projectId}`,
    });
    return data.data;
  }

  static async getDeploymentLogs(deploymentId: string) {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/deployment/${deploymentId}/logs`,
    });
    return data.data;
  }

  static async getProcessLogs(projectId: string) {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/projects/${projectId}/process-logs`,
    });
    return data.data;
  }

  static async getGitLabTokens(code: string, redirectUri: string) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/auth/gitlab/token`,
      data: {code, redirectUri}
    });
    return data.data;
  }

  static async getGitLabConnectionStatus() {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/auth/gitlab/check-connection`
    });
    return data.data;
  }

  static async getGitLabNamespaces() {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/gitlab/get-namespaces`
    });
    return data.data;
  }

  static async getGitLabProjects(namespace: string) {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/gitlab/get-projects`,
      data: {
        namespace
      }
    });
    return data.data;
  }

  static async getGitLabProjectBranches(projectId: string) {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/gitlab/get-project-branches`,
      params: {
        projectId
      }
    });
    return data.data;
  }

  static async disconnectFromGitlab() {
    const {data} = await this.sendRequest({
      method: "GET",
      url: `${this.baseUrl}/gitlab/disconnect`,
    });
    return data.data;
  }

  static async restart(projectId: string): Promise<any> {
    const response = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/control/restart`,
      data: {
        projectId: projectId,
      }
    });
    return response.data;
  }

  static async login(loginData: string, password: string): Promise<IronSessionData> {
    const {data} = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/auth/login`,
      data: {
        loginData, password
      }
    });
    return data;
  }

  static async refreshToken(refreshToken: string): Promise<any> {
    const response = await this.sendRequest({
      method: "POST",
      url: `${this.baseUrl}/auth/refresh-token`,
      data: {
        refreshToken
      }
    });
    // console.log(response);
    return response.data;
  }

  static async logout(refreshToken?: string): Promise<any> {
    if (refreshToken) {
      const {data} = await this.sendRequest({
        method: "POST",
        url: `${this.baseUrl}/auth/logout`,
        data: {
          refreshToken
        }
      });
      return data;
    }
  }
}
