import axios from "axios";

export class JwtAuthService {
  static appId = process.env.NEXT_PUBLIC_SSO_APP_ID || "";
  static appSecret = process.env.SSO_APP_SECRET || "";
  static authServer = "https://auth.northstudio.vn";
  static redirectUri = (typeof location !== "undefined") ? `${location.protocol}//${location.hostname}/auth/callback` : "";

  static generateAuthUrl() {
    return `${this.authServer}/oauth/authorize?response_type=code&client_id=${this.appId}&redirect_uri=${this.redirectUri}`
  }

  static async requestToken(authCode: string) {
    return axios.post(`${this.authServer}/oauth/token`, {
      grant_type: 'authorization_code',
      redirect_uri: this.redirectUri,
      code: authCode,
      client_id: this.appId,
      client_secret: this.appSecret,
    });
  }

  static async getSelfInfo(accessToken: string) {
    return axios.get(`${this.authServer}/api/v1/accounts/me`, {
      headers: {
        "Authorization": `Bearer ${accessToken}`,
        'Cookie': 'connect.sid=s%3Ae-6v5kGMY7hH4hLMcauKKXHpHajU1IEQ.xp4EHovtfPQ6R3jornz%2BOKFEzj%2BIuw2Ymb5o3%2BCKS%2Fk'
      }
    });
  }
}