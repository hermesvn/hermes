interface Account {
  _id?: any,
  id?: any,
  email: string,
  name?: string,
  username?: string
  currentPassword?: string,
  password?: string,
  confirmPassword?: string,
  gitCredentials?: GitCredentials
}

interface GitCredentials {
  _id?: any,
  account?: Account,
  currentPassword?: string,
  username?: string,
  password?: string,
  privateKeyPath?: string,
  publicKeyPath?: string
  sshUsername?: string
  passPhrase?: string
}

interface Credentials {
  accessToken: string,
  refreshToken: string,
  accessTokenExpiresTime: string,
  refreshTokenExpiresTime: string,
}

interface GitLabConnection {
  owner: string | number,
  accessToken: string,
  refreshToken?: string,
  expirationTime: number,
  valid: boolean
}

type ProjectSettingTypes = "password" | "credentials" | "general" | "git" | "environment-variables" | "nginx-config";

type SettingMode = "account" | "project";


type DeploymentType = "nextjs" | "nodejs" | "svelte" | "svelte-kit" | "reactjs" | "gatsby" | "preact" | "angular";

interface DeploymentScripts {
  install?: string
  startup?: string,
  build?: string
}

type EnvVariables = {
  [key: string]: string
}

type ProjectRepoSource = 'gitlab' | 'github';

interface Project<DeployType = DeploymentType | null> {
  _id?: any,
  id?: any,
  name?: string,
  branch?: string,
  description?: string,
  repoSource?: ProjectRepoSource,
  repo?: GitLabProject | any,
  code?: string,
  avatarUrl?: string,
  owner?: string,
  cloneOption?: string,
  creationTime?: number,
  deploymentType?: DeployType,
  scripts?: DeploymentScripts
  environmentVariables?: EnvVariables,
  environmentVariablesAsString?: string,
  valid?: boolean,
  type?: DeployType,
  gitlabId?: string,
  slug?: string,
  deployments?: Deployment[],
  autoDeploy?: {
    cloneOption?: string,
    enabled: boolean,
    branch: string,
    domain: string,
    nginxSupport: boolean,
    nginxConfig: string,
    isCustomDomain?: boolean,
    environmentVariables: [{
      key: string,
      value: string
    }]
  },
}

interface ProjectDeployOptions {
  skipGitChecking?: boolean
  cachedDeployment?: boolean
}

type DeploymentStatus = "init" | "deploying" | "finished" | "failed"

interface Deployment {
  _id?: any,
  creationTime: number,
  finishedTime: number,
  project: Project,
  status: DeploymentStatus
}

interface GitLabNamespace {
  id: number,
  name: string,
  path: string,
  kind: string,
  full_path: string
  parent_id: number | string | null,
  avatar_url: string,
  web_url: string
}

interface GitLabProject {
  id: number,
  description: string,
  name: string,
  name_with_namespace: string,
  path: string,
  path_with_namespace: string,
  created_at: string,
  default_branch: string,
  tag_list: string[],
  topics: string[],
  ssh_url_to_repo: string,
  http_url_to_repo: string,
  web_url: string,
  readme_url: string,
  avatar_url: string | null,
  forks_count: number,
  star_count: number,
  last_activity_at: string,
  namespace: GitLabNamespace
}

interface GitLabCommit {
  id: string,
  short_id: string,
  created_at: string,

}

interface CloneOption {
  name: string,
  url?: string,
  default: boolean,
  credentials?: GitCredentials
}

interface GitLabBranch {
  name: string,
  can_push: boolean
  commit: GitLabCommit,
  default: boolean
  developers_can_merge: boolean
  developers_can_push: boolean
  merged: boolean
  protected: boolean
  web_url: string
}

type IFrameworkDetails = {
  [key in DeploymentType]: string;
};
