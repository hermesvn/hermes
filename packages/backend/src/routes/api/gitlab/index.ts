import 'express-async-errors';
import {Router} from "express";
import {GitlabController} from "@src/controllers/gitlab.controller";

export const gitlabRouter = Router();

gitlabRouter.get('/get-namespaces', GitlabController.getNamespaces);
gitlabRouter.get('/get-project-branches', GitlabController.getProjectBranches);
gitlabRouter.post('/get-projects', GitlabController.getProjects);
gitlabRouter.post('/disconnect', GitlabController.disconnect);
