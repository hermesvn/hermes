import 'express-async-errors';
import {Router} from "express";

import {AuthController} from "@src/controllers/auth.controller";
import {gitlabAuthRouter} from "@src/routes/api/auth/gitlab";

export const authRouter = Router();

authRouter.use('/gitlab', gitlabAuthRouter);
authRouter.post('/login', AuthController.login);
authRouter.post('/sign-up', AuthController.signUp);
authRouter.post('/logout', AuthController.logout);
authRouter.post('/refresh-token', AuthController.refreshToken);
authRouter.post('/revoke-token', AuthController.revokeToken);
