import 'express-async-errors';
import { Router } from "express";

import { authMiddleware } from "@src/middlewares/auth.middleware";
import { GitlabController } from "@src/controllers/gitlab.controller";

export const gitlabAuthRouter = Router();

gitlabAuthRouter.get("/callback", (req, res) => {
  res.json({ code: req.query.code });
});

gitlabAuthRouter.use(authMiddleware("jwt"));
gitlabAuthRouter.get("/check-connection", GitlabController.checkConnection);
gitlabAuthRouter.post("/token", GitlabController.connectToGitlab);
