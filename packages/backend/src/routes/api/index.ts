import express from "express";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import {authRouter} from "@src/routes/api/auth";
import {deploymentRouter} from "@src/routes/api/deployment";
import {projectRouter} from "@src/routes/api/projects";
import {gitlabRouter} from "@src/routes/api/gitlab";
import {controlRouter} from "@src/routes/api/control";
import {accountRouter} from "@src/routes/api/accounts";

export const apiRouter = express.Router();

apiRouter.use("/auth", authRouter);
apiRouter.use("/deployment", deploymentRouter);

apiRouter.use(authMiddleware("jwt"));

// apiRouter.use("/auth", authRouter);
apiRouter.use("/gitlab", gitlabRouter);
apiRouter.use("/projects", projectRouter);
apiRouter.use("/control", controlRouter);
apiRouter.use("/accounts", accountRouter);
