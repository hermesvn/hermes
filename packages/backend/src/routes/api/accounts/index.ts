import 'express-async-errors';
import {Router} from "express";
import {AccountController} from "@src/controllers/account.controller";
import {validateMiddleware} from "@src/middlewares/validate.middleware";
import {updateAccount, updateGitCredentials} from "@src/validations/account.validation";

export const accountRouter = Router();

accountRouter.get('/:accountId', AccountController.handleAccount);
accountRouter.post('/:accountId', validateMiddleware(updateAccount), AccountController.handleAccount);
accountRouter.get('/:accountId/git-credentials', AccountController.handleGitCredentials);
accountRouter.post('/:accountId/git-credentials', validateMiddleware(updateGitCredentials), AccountController.handleGitCredentials);
