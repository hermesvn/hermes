import 'express-async-errors';
import {Router} from "express";
import {DeploymentController} from "@src/controllers/deployment.controller";

export const deploymentRouter = Router();

deploymentRouter.post('/logs');
deploymentRouter.get('/:deploymentId');
deploymentRouter.get('/:deploymentId/logs', DeploymentController.getDeploymentLogs);
deploymentRouter.get('/:processName/process-logs', DeploymentController.getProcessLogs);
deploymentRouter.post('/');
deploymentRouter.post('/:deploymentId');
deploymentRouter.delete('/:deploymentId');
