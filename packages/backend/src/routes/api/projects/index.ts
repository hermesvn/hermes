import 'express-async-errors';
import {Router} from "express";
import {ProjectController} from "@src/controllers/project.controller";

export const projectRouter = Router();

projectRouter.get('/', ProjectController.handleProjects);
projectRouter.get('/:projectId', ProjectController.handleProject);
projectRouter.get('/:projectId/process-usages', ProjectController.getProcessUsage);
projectRouter.get('/:projectId/process-logs', ProjectController.getProcessLogs);
projectRouter.post('/', ProjectController.handleProjects);
projectRouter.post('/:projectId', ProjectController.handleProject);
projectRouter.post('/:projectId/deploy', ProjectController.deployProject);
projectRouter.delete('/:projectId', ProjectController.handleProject);
