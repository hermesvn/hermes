import 'express-async-errors';
import {Router} from "express";

import {authMiddleware} from "@src/middlewares/auth.middleware";
import {DeploymentController} from "@src/controllers/deployment.controller";
import {MonitorService} from "@src/services/monitor.service";

export const controlRouter = Router();

controlRouter.get('/process-logs/:processName', authMiddleware("jwt"), async (req, res) => {
  const process  = await MonitorService.getProcessDescription(req.params.processName);
  res.json({data: process});
});
controlRouter.post('/restart', authMiddleware("jwt"), DeploymentController.restart);
