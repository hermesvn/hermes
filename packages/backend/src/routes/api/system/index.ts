import {Router} from "express";
import {OsFunc} from "@src/utils/exec-command";

export const systemRouter = Router()

systemRouter.get("/directory/deployments", async (req, res) => {
  try {
    const a = await OsFunc.execCommand(`sudo ls ${process.env.DEPLOYMENT_PATH}`);
    res.json({data: a})
  } catch (e) {
    // console.log(e)
  }
})
