import 'express-async-errors';
import {Router} from "express";

import {DeploymentController} from "@src/controllers/deployment.controller";

export const webhookRouter = Router();

webhookRouter.post('/gitlab', DeploymentController.webhook);
