import express from "express";
import {apiRouter} from '@src/routes/api';
import {webhookRouter} from "@src/routes/webhooks";
import {systemRouter} from "@src/routes/api/system";

export const mainRouter = express.Router();

mainRouter.use('/api', apiRouter);
mainRouter.use('/system', systemRouter);
mainRouter.use('/webhooks', webhookRouter);

mainRouter.get('/', (req, res) => {
  res.json({message: "Welcome to Hermes API"});
});
