import Joi from "joi";
import {password} from "@src/validations/custom.validations";

export const updateAccount = {
  body: Joi.object().keys({
    currentPassword: Joi.string(),
    password: Joi.string().custom(password),
    username: Joi.string(),
    email: Joi.string(),
  })
};

export const updateGitCredentials = {
  body: Joi.object().keys({
    currentPassword: Joi.string(),
    password: Joi.string(),
    username: Joi.string(),
    privateKeyPath: Joi.string(),
    publicKeyPath: Joi.string(),
    sshUsername: Joi.string(),
    passPhrase: Joi.string(),
  })
};
