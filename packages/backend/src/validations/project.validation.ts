export const projectValidation = (project: Project) => {
  return !(!project.name || !project.branch || !project.deploymentType);
}
