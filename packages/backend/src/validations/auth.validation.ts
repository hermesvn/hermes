import Joi from "joi";
import { password } from "@src/validations/custom.validations";

export const register = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
    username: Joi.string().required(),
    email: Joi.string(),
    fullName: Joi.string()
  })
};

export const login = {
  body: Joi.object().keys({
    username: Joi.string(),
    password: Joi.string(),
    fbAccessToken: Joi.string(),
    ggAccessToken: Joi.string(),
    remember: Joi.boolean()
  })
};

export const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required()
  })
};

export const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required()
  })
};

export const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required()
  })
};

export const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required()
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password)
  })
};

export const verifyEmail = {
  query: Joi.object().keys({
    token: Joi.string().required()
  })
};
