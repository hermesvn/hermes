import {Schema, model} from 'mongoose';

const schema = new Schema<GitLabConnection>({
  owner: String,
  accessToken: String,
  refreshToken: String,
  expirationTime: Number,
  valid: Boolean
}, {
  collection: 'gitlab-connections',
  timestamps: true
});

export const GitLabConnectionModel = model<GitLabConnection>("GitLabConnection", schema);
