import mongoose from "mongoose";

const schema = new mongoose.Schema({
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  time: Date,
  cpu: Number,
  mem: Number,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

export const ProcessUsageModel = mongoose.model('ProcessUsage', schema);
