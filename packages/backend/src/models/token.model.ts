import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";

interface TokenDocument extends Document, Token {
}

const schema = new mongoose.Schema<Token>({
  token: {
    type: String,
    required: true
  },
  type: {
    type: String,
    default: "REFRESH"
  },
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.plugin(paginate);

export const TokenModel = mongoose.model<TokenDocument>("Token", schema);
