import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/process-string";

interface RoleDocument extends Document, Role {
}

const schema = new mongoose.Schema<Role>({
  id: String,
  name: {
    type: String,
    trim: true,
    required: true
  },
  slug: {
    type: String,
    required: true,
    trim: true
  }
}, {
  timestamps: true
});

schema.pre("save", async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", RoleModel);

  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", RoleModel, doc.name, "slug");

  next();
});

schema.plugin(paginate);

export const RoleModel = mongoose.model<RoleDocument>("Role", schema);
