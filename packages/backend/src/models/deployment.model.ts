import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/process-string";

interface DeploymentDocument extends Document, Deployment {
}

const schema = new mongoose.Schema<Deployment>({
  id: String,
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  domain: String,
  port: Number,
  framework: String,
  path: {
    type: String
  },
  status: {
    type: String
  },
  deploymentLog: String,
  runtimeLog: String,
  errLog: String,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.pre('save', async function (next) {
  // eslint-disable-next-line @typescript-eslint/no-this-alias
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", DeploymentModel);
  next();
});

schema.plugin(paginate);

export const DeploymentModel = mongoose.model<DeploymentDocument>('Deployment', schema);
