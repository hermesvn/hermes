import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/process-string";
import bcrypt from "bcryptjs";

interface AccountDocument extends Document, Account {
}

const schema = new mongoose.Schema<Account>({
  id: String,
  avatar: {
    type: String,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    required: true
  },
  password: {
    type: String,
    trim: true,
    select: 0,
    required: true
  },
  role: {
    type: String,
    default: "user"
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.pre("save", async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", AccountModel);
  if (doc.isModified("password")) doc.password = bcrypt.hashSync(doc.password, 8);
  next();
});

schema.virtual("gitCredentials", {
  foreignField: "account",
  localField: "_id",
  ref: "GitCredentials"
})

schema.plugin(paginate);

export const AccountModel = mongoose.model<AccountDocument>("Account", schema);
