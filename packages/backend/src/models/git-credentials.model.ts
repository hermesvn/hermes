import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";

interface GitCredentialDocument extends Document, GitCredentials {
}

const schema = new mongoose.Schema<GitCredentials>({
  id: String,
  account: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Account"
  },
  username: {
    type: String,
    trim: true
  },
  password: {
    type: String,
    trim: true,
  },
  sshUsername: {
    type: String,
    default: "git"
  },
  privateKeyPath: String,
  publicKeyPath: String,
  passPhrase: String,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.plugin(paginate);

export const GitCredentialsModel = mongoose.model<GitCredentialDocument>("GitCredentials", schema);
