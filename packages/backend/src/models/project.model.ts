import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/process-string";

interface ProjectDocument extends Document, Project {
}

const schema = new mongoose.Schema<Project>({
  id: String,
  code: String,
  avatarUrl: String,
  owner: String,
  creationTime: Date,
  type: {
    type: String,
    required: true
  },
  valid: Boolean,
  branch: String,
  description: String,
  repoSource: {
    type: String,
    enum: ["gitlab", "github"]
  },
  repo: {},
  scripts: {
    startup: String,
    install: String,
    build: String
  },
  gitlabId: {
    type: String
  },
  name: {
    type: String,
    required: true
  },
  slug: String,
  autoDeploy: Object({
    cloneOption: String,
    enabled: Boolean,
    branch: String,
    domain: String,
    nginxSupport: Boolean,
    nginxConfig: String,
    isCustomDomain: {type: Boolean, default: false},
    environmentVariables: [{
      key: String,
      value: String
    }]
  }),
  cpuUsage: Number,
  memUsage: Number,
  uptime: Number,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual("deployments", {
  foreignField: "project",
  localField: "_id",
  ref: "Deployment"
})

schema.pre("save", async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", ProjectModel);
  if (doc.isModified("name")) {
    doc.code = await uniqueStringGenerator("slug", ProjectModel, doc.name, "code");
    doc.slug = await uniqueStringGenerator("slug", ProjectModel, doc.name, "slug");
  }
  next();
});

schema.plugin(paginate);

export const ProjectModel = mongoose.model<ProjectDocument>("Project", schema);
