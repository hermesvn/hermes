declare module 'express-session' {
  interface SessionData {
    isLoggedIn?: boolean;
    user?: Account;

    [key: string]: any;
  }
}

interface DocFilter {
  start?: string,
  end?: string,
  range?: string,

  [key: string]: any
}

type TokenType = "REFRESH"

interface Token {
  account: Account,
  type: TokenType,
  token: string
}

interface Account {
  _id?: any,
  id?: any,
  email: string,
  username: string,
  password: string,
  avatar: string,
  role: string,
  gitCredentials?: GitCredentials
}

interface GitCredentials {
  _id?: any,
  id?: any,
  account: Account,
  username?: string,
  password?: string,
  sshUsername?: string,
  privateKeyPath?: string,
  publicKeyPath?: string,
  passPhrase?: string,
}

interface Role {
  _id?: any,
  id?: any,
  name: string,
  slug: string
}

interface Credentials {
  accessToken: string,
  refreshToken: string,
  expirationTime: number,
}

interface GitLabConnection {
  owner: string | number,
  accessToken: string,
  refreshToken?: string,
  expirationTime: number,
  valid: boolean
}

type DeploymentType = "nextjs" | "nodejs" | "svelte" | "svelte-kit" | "reactjs" | "gatsby" | "preact" | "angular";

interface DeploymentScripts {
  install?: string
  startup?: string,
  build?: string
}

interface EnvVariables {
  [key: string]: string
}

type ProjectRepoSource = 'gitlab' | 'github';

interface Project<DeployType = DeploymentType | null> {
  _id?: any,
  id?: any,
  name?: string,
  branch?: string,
  description?: string,
  repoSource?: ProjectRepoSource,
  repo?: GitLabProject | any,
  code?: string,
  avatarUrl?: string,
  owner?: Account,
  creationTime?: number,
  deploymentType?: DeployType,
  type?: DeployType,
  scripts?: DeploymentScripts
  environmentVariables?: EnvVariables,
  valid?: boolean,
  gitlabId?: string,
  slug?: string,
  autoDeploy?: {
    cloneOption: string
    enabled: boolean,
    branch: string,
    domain: string,
    nginxSupport: boolean,
    nginxConfig: string,
    isCustomDomain: boolean,
    environmentVariables: [{
      key: string,
      value: string
    }]
  },
  cpuUsage: number,
  memUsage: number,
  uptime: number
}

type DeploymentStatus = "init" | "deploying" | "finished" | "failed"

interface Deployment {
  _id?: any,
  id?: any,
  creationTime: number,
  finishedTime: number,
  project: Project,
  status: DeploymentStatus
  domain: string,
  port: number,
  framework: string,
  path: string,
  deploymentLog: string,
  runtimeLog: string,
  errLog: string,
  headCode: string
}

interface GitLabNamespace {
  id: number,
  name: string,
  path: string,
  kind: string,
  full_path: string
  parent_id: number | string | null,
  avatar_url: string,
  web_url: string
}

interface GitLabProject {
  id: number,
  description: string,
  name: string,
  name_with_namespace: string,
  path: string,
  path_with_namespace: string,
  created_at: string,
  default_branch: string,
  tag_list: string[],
  topics: string[],
  ssh_url_to_repo: string,
  http_url_to_repo: string,
  web_url: string,
  readme_url: string,
  avatar_url: string | null,
  forks_count: number,
  star_count: number,
  last_activity_at: string,
  namespace: GitLabNamespace
}

interface GitLabCommit {
  id: string,
  short_id: string,
  created_at: string,

}

interface GitLabBranch {
  name: string,
  can_push: boolean
  commit: GitLabCommit,
  default: boolean
  developers_can_merge: boolean
  developers_can_push: boolean
  merged: boolean
  protected: boolean
  web_url: string
}

type IFrameworkDetails = {
  [key in DeploymentType]: string;
};
