import {config} from '@src/config';
import {AuthService} from '@src/services/auth.service';
import {AccountModel} from '@src/models/account.model';
import {Strategy as JwtStrategy} from 'passport-jwt';

const jwtOptions = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: AuthService.extractTokenFromRequest,
};

const passportJwtVerify = async (payload, done) => {
  try {
    const user = await AccountModel.findOne({id: payload.sub}).populate("role");

    if (!user) {
      return done(null, false);
    }
    done(null, user);
  } catch (error) {
    done(error, false);
  }
};

export const jwtStrategy = new JwtStrategy(jwtOptions, passportJwtVerify);
