import dotenv from "dotenv";
import path from "path";

dotenv.config({path: path.join(__dirname, "/.env")});

export const config = {
  env: process.env.NODE_ENV || "development",
  port: process.env.PORT || 3333,
  host: {
    domain: process.env.NEXT_PUBLIC_HOST_DOMAIN,
    ipAddress: process.env.HOST_IP_ADDRESS,
  },
  system: {
    roles: ["ADMIN", "USER"]
  },
  mongoose: {
    url: process.env.MONGO_URL,
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    accessExpirationMinutes: process.env.JWT_ACCESS_EXPIRATION_MINUTES || 60,
    refreshExpirationDays: process.env.JWT_REFRESH_EXPIRATION_DAYS || 1,
    resetPasswordExpirationMinutes: process.env.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: process.env.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES
  },
  gCloud: {
    projectId: process.env.GCLOUD_PROJECT_ID,
    clientEmail: process.env.GCLOUD_CLIENT_EMAIL,
    privateKey: process.env.GCLOUD_PRIVATE_KEY,
    baseUrl: "https://storage.googleapis.com/"
  },
  discord: {
    botToken: process.env.DISCORD_BOT_TOKEN || `OTQ1Mjk1NDk0OTQ1NTk5NTMw.YhOFCA.KbVT0kQbco-HEWnaKtq5b-Uf6BU`,
    pushLogChannel: process.env.DISCORD_PUSH_CHANNEL || `945292558479790160`,
    deploymentLogChannel: process.env.DISCORD_DEPLOYMENT_CHANNEL || `945372477478039602`
  },
};
