export const NGINX_NODEJS_CONFIG = (domain, port) => `server {
  listen 80;
  server_name ${domain};
  location / {
    proxy_pass http://localhost:${port};
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}`;

export const NGINX_REACTJS_CONFIG = (domain, path) => `server {
  server_name ${domain};
  root ${path};
  index index.html index.htm;
  location / {
    try_files $uri /index.html =404;
  }
}`;
