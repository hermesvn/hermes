import morgan from 'morgan';
import {config} from '@src/config/index';
import {logger} from '@src/config/logger.config';
import {ServerResponse} from "http"

type ExtServerResponse = ServerResponse & {
  errorMessage: string
}

morgan.token('message', (req, res: ExtServerResponse) => res.errorMessage as string | '');

const getIpFormat = () => (config.env === 'production' ? ':remote-addr - ' : '');
const successResponseFormat = `${getIpFormat()}:method :url :status - :response-time ms`;
const errorResponseFormat = `${getIpFormat()}:method :url :status - :response-time ms - message: :message`;

const successHandler = morgan(successResponseFormat, {
  skip: (_req, res) => res.statusCode >= 400,
  stream: {write: (message) => logger.info(message.trim())},
});

const errorHandler = morgan(errorResponseFormat, {
  skip: (_req, res) => res.statusCode < 400,
  stream: {write: (message) => logger.error(message.trim())},
});

export default {
  successHandler,
  errorHandler,
};
