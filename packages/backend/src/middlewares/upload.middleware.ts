import {IncomingForm} from 'formidable'
import httpStatus from "http-status";
import {ApiError} from "@src/utils/api-error";
import {uploadFile} from "@src/utils/google-storage";

const uploadData = (extFilter = [], limitedSize = 10 * 1024 * 1024) => (req, res, next) => {
  const uniqueSuffix = Date.now() + '_' + Math.round(Math.random() * 1E9);
  const form = new IncomingForm();
  form.parse(req, async (err, fields, files) => {
    try {
      if (err) {
        throw new ApiError(httpStatus.BAD_REQUEST, "Failed to upload file");
      }
      req.body = fields;
      if (Object.keys(files).length <= 0) return next();

      const {newFilename, filepath, mimetype: rawMimetype, size} = files.file;
      const mimetype: string = rawMimetype.split("/")[1];
      const destFileName = `${newFilename}_${uniqueSuffix}.${mimetype}`

      const gFile = await uploadFile(filepath, {
        destination: destFileName,
      });


      if (!extFilter.includes(mimetype)) {
        throw new ApiError(httpStatus.BAD_REQUEST, "Invalid file type");
      }

      if (size > limitedSize) {
        throw new ApiError(httpStatus.BAD_REQUEST, "This file was to large to upload");
      }
      req.file = files.file;
      req.file.linkUrl = gFile[1]["mediaLink"];
      next();
    } catch (err) {
      next(err);
    }
  });
}

export default uploadData;
