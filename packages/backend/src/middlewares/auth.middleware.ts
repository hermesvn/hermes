import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import passport from "passport";
import {jwtStrategy} from "@src/config/passport.config";

passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
passport.use('jwt', jwtStrategy);

const verifyJWTCallback = (req, next) => async (err, user, info) => {
  if (err || !user || info) {
    // console.log(err, user, info);
    return next(new ApiError(httpStatus.UNAUTHORIZED, 'Not authenticated'));
  }
  req.account = user;
  next();
}

export const authMiddleware = (method: string) => async (req, res, next) => {
  switch (method) {
    case "jwt":
      passport.authenticate(method, verifyJWTCallback(req, next))(req, res, next);
      break;
    default: {
      return next(new Error("Invalid authentication strategy"));
    }
  }
}

