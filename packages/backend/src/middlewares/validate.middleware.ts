import Joi from 'joi';
import httpStatus from 'http-status';
import {pick} from '@src/utils/pick';
import {ApiError} from '@src/utils/api-error';

export const validateMiddleware = (schema) => (req, res, next) => {
  try {
    const validSchema = pick(schema, ['params', 'query', 'body']);
    const object = pick(req, Object.keys(validSchema));
    const {value, error} = Joi.compile(validSchema)
      .prefs({errors: {label: 'key'}, abortEarly: false})
      .validate(object);

    if (error) {
      const errorMessage = error.details.map((details) => details.message).join(', ');
      console.log(errorMessage);
      throw new ApiError(httpStatus.BAD_REQUEST, "Validation Failed" || errorMessage);
    }
    Object.assign(req, value);
    next()
  } catch (err) {
    next(err)
  }
};
