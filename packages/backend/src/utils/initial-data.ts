import {config} from "@src/config";
import {slugify} from "@src/utils/process-string";
import {RoleModel} from "@src/models/role.model";
import {AccountModel} from "@src/models/account.model";
import {logger} from "@src/config/logger.config";

export const initiateData = async () => {
  const existsRole = config.system.roles;
  const roles = (await RoleModel.find({name: {$in: existsRole}})).map(data => data.name);
  const newRoles = existsRole.filter(data => !roles.includes(data)).map(data => {
    return {
      insertOne: {
        document: {
          name: data,
          slug: slugify(data)
        }
      }
    }
  });
  if (newRoles.length > 0) {
    // @ts-ignore
    await RoleModel.bulkWrite(newRoles);
    logger.info("Created default roles");
  }
  if (await AccountModel.countDocuments({}) <= 0) {
    logger.info("Created default account");
    // await AccountModel.create({username: "admin", password: "admin@123", role: "admin", email: "admin@northstudio.vn"});
  }
}
