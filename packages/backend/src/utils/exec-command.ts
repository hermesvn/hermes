import {exec} from "child_process";

export class OsFunc{
  static execCommand(cmd) {
    return new Promise((resolve, reject)=> {
      exec(cmd, (error, stdout, stderr) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(stdout);
      });
    })
  }
}
