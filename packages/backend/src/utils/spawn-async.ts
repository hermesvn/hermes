import {spawn} from "child_process";
import {ThreadChannel} from "discord.js";
import {LogService} from "@src/services/log.service";
import {logger} from "@src/config/logger.config";
import moment from "moment";

const defaultTimeout = 900; // 15 mins
interface SpawnOptions {
  command: string,
  options: {
    shell: boolean,
    cwd: string
  },
  prefix: string,
  thread: ThreadChannel,
  logService?: LogService,
  timeout?: number
}

export const SpawnAsync = async (spawnOpts: SpawnOptions) => {
  let message = null;

  let logContent = 'Spawned `' + spawnOpts.command + '` at ' + moment().utcOffset('+0700').format() + '\n';
  let timeElapsed = 0;
  try {
    await spawnOpts.logService.generateDeploymentLogs(logContent);
    message = await spawnOpts.thread.send(logContent);
  } catch (e) {
    // logger.error(e);
    console.log("Process has been successfully spawned but the message deliverer is not working");
  }
  try {
    const sh = spawn(spawnOpts.command || "", spawnOpts.options);
    const timeout = spawnOpts.timeout || defaultTimeout;
    const startTime = new Date().getTime();


    const editLog = (logContent) => {
      logContent = logContent.split('\n').filter(x => x.trim() !== '').slice(-19).map(line => {
        if (line.length > 100) line = line.substring(0, 100);
        return line;
      }).join('\n');
      if (message) {
        message.edit('```shell\n' + logContent + '```');
      }
    }

    const addLog = async data => {
      if (data.toString().trim() !== "") {
        await spawnOpts.logService.generateDeploymentLogs(data.toString().trim());
        logContent += data.toString() + '\n';
        editLog(logContent);
      }
    };

    sh.stdout.setEncoding('utf8');
    sh.stdout.on('data', addLog);
    sh.stdout.on('error', addLog);

    sh.stderr.setEncoding('utf8');
    sh.stderr.on('data', addLog);
    sh.stderr.on('error', addLog);

    return {
      process: sh,
      runtime: new Promise(_ => {
        const checkingInterval = setInterval(async () => {
          timeElapsed = ~~((new Date().getTime() - startTime) / 1000);
          if (timeElapsed > timeout) {
            clearInterval(checkingInterval);
            // sh.stdin.pause();
            sh.kill();
            if (message) {
              await spawnOpts.logService.generateDeploymentLogs(`[!!!] Error: Process was killed due to timeout (${timeElapsed}s over ${timeout}s)`)
              spawnOpts.thread.send(`[!!!] Error: Process was killed due to timeout (${timeElapsed}s over ${timeout}s)`);
            }
            _(1);
          }
        }, 1000);
        sh.on('exit', async (code) => {
          logContent += `\n[${spawnOpts.prefix}] Exited with code ${code}`;
          await spawnOpts.logService.generateDeploymentLogs(`[${spawnOpts.prefix}] Exited with code ${code}`);

          editLog(logContent);
          clearInterval(checkingInterval);
          _(code);
        });
        sh.on('close', async (code) => {
          logContent += `\n[${spawnOpts.prefix}] Closed with code ${code}\n`;
          await spawnOpts.logService.generateDeploymentLogs(`[${spawnOpts.prefix}] Closed with code ${code}`);
          editLog(logContent);
          clearInterval(checkingInterval);
          _(code);
        });
      })
    };
  } catch (e) {
    logger.error(e);
    throw new Error("Cannot execute spawn commands");
  }
};
