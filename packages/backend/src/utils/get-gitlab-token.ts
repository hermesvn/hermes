import {GitLabConnectionModel} from "@src/models/gitlab-connection.model";
import {GitLabService} from "@src/services/gitlab.service";
import {HydratedDocument} from "mongoose";
import {logger} from "@src/config/logger.config";

export const getGitlabToken = async (userId: string) => {
  const lastConnections: HydratedDocument<GitLabConnection>[] = await GitLabConnectionModel.find({
    valid: true,
    owner: userId
  }, null, {
    sort: {
      expirationTime: -1
    }
  }).limit(1);
  // TODO: fix duplicate requests on refreshing token.ts.

  for (const connection of lastConnections) {
    if (connection.expirationTime > new Date().getTime() + 5 * 60 * 1000) {
      return connection.accessToken;
    } else {
      if (typeof connection.refreshToken === "string") {
        try {
          logger.info("Requesting new token.ts...");
          const {data: newTokenData} = await GitLabService.refreshToken(connection.refreshToken);
          connection.accessToken = newTokenData.access_token;
          connection.refreshToken = newTokenData.refresh_token;
          connection.expirationTime = (newTokenData.created_at + newTokenData.expires_in) * 1000;
          await connection.save();
          return newTokenData.access_token;
        } catch (e) {
          logger.error(e);
          throw new Error("Failed to refresh token.ts");
        }
      } else {
        throw new Error("Refresh Token is missing.");
      }
    }
  }
  if (lastConnections.length === 0) return "";/*throw new Error("No connection available.");*/

};
