import {v4 as uuid} from "uuid";
import crypto from "crypto";
import {Model} from "mongoose";

export const randomString = (length: number) => {
  const result = [];
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return result.join('');
}

export const randomNum = (length: number) => {
  const result = [];
  const characters = '0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return parseInt(result.join(''));
}

export const slugify = (str: string) => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/đ/g, 'd').replace(/Đ/g, 'D').replace(/ /g, '_').toLowerCase().replace(/[^\w-]+/g, '');
}

export const uniqueStringGenerator = async <T>(type: string, model: Model<T>, value: string = "", field?: any): Promise<string> => {
  switch (type) {
    case "slug":
      let count = 0, newSlug = slugify(value);
      while (await model.exists((field) ? {[field]: newSlug.toString()} : {slug: newSlug.toString()})) {
        newSlug = `${slugify(value)}_${++count}`;
      }
      return newSlug;
    case "iId":
      const doc = await model.findOne({}).sort({id: -1});
      let newIId = (doc && doc.id) ? parseInt(doc.id) : (value) ? parseInt(value) : 10000000000;
      while (await model.exists((field) ? {[field]: newIId.toString()} : {id: newIId.toString()})) {
        newIId = newIId + randomNum(1);
      }
      return newIId.toString();
    case "uuid":
      return uuid();
    case "crypto":
      const seed = crypto.randomBytes(256);
      return crypto
        .createHash('sha1')
        .update(seed)
        .digest('hex');
  }
  return "";
};
