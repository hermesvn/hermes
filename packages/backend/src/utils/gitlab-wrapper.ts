import axios from "axios";
import {logger} from "@src/config/logger.config";
import {TelegramService} from "@src/services/telegram-service";
import {getGitlabToken} from "@src/utils/get-gitlab-token";
import {ProjectModel} from "@src/models/project.model";

const gitlabHome = "https://gitlab.com"

export const getRepoInfo = async (projectId: string): Promise<{ ssh_url_to_repo, http_url_to_repo }> => {
  logger.info(`Getting repo info with id ${projectId}...`);
  const project = await ProjectModel.findOne({gitlabId: projectId}).lean();
  const accessToken = await getGitlabToken(project.owner.toString());

  return new Promise((resolve, reject) => {
    axios(`${gitlabHome}/api/v4/projects/${projectId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    }).then(response => {
      resolve(response.data);
    }).catch(async e => {
      await TelegramService.sendMessage(1362849660, e.response.data.message);
      reject(e.response.data.message);
    });
  });
}
