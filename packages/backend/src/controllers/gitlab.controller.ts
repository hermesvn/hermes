import {GitLabService} from "@src/services/gitlab.service";
import {GitLabConnectionModel} from "@src/models/gitlab-connection.model";
import {getGitlabToken} from "@src/utils/get-gitlab-token";

export class GitlabController {
  static async getNamespaces(req, res) {
    let token: string;
    const retrievedToken = await getGitlabToken(req.account._id);
    if (typeof retrievedToken === "string") {
      token = retrievedToken;
      const {data: namespaces} = await GitLabService.getNamespaces(token);
      res.json({
        data: namespaces
      });
    } else throw new Error("Token is not valid.");
  }

  static async getProjectBranches(req, res) {
    const projectId = req.query.projectId;
    if (!projectId) return res.status(400).json({
      message: "Project ID is required."
    });
    let token: string;
    try {
      const retrievedToken = await getGitlabToken(req.account?._id);
      if (typeof retrievedToken === "string") {
        token = retrievedToken;
      } else throw new Error("Token is not valid.");
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: e.message
      });
    }
    const {data: branches} = await GitLabService.getProjectBranches(projectId as string, token);
    res.json({
      data: branches
    });
  }

  static async getProjects(req, res) {
    const namespace = req.body.namespace;
    if (!namespace) return res.status(400).json({
      message: "A namespace is required. eg. users/someone"
    });
    let token: string;
    try {
      const retrievedToken = await getGitlabToken(req.account?._id);
      if (typeof retrievedToken === "string") {
        token = retrievedToken;
      } else throw new Error("Token is not valid.");
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: e.message
      });
    }
    const {data: projects} = await GitLabService.getProjects(namespace, token);
    res.json({
      data: projects
    });
  }

  static async checkConnection(req, res) {
    const token = await getGitlabToken(req.account._id);
    return res.json({
      data: !!token
    });
  }

  static async connectToGitlab(req, res) {
    const {data} = await GitLabService.requestToken(req.body);

    if (await GitLabConnectionModel.countDocuments({
      owner: req.account._id
    }) <= 0) {
      await GitLabConnectionModel.create({
        owner: req.account._id,
        accessToken: data.access_token,
        refreshToken: data.refresh_token,
        expirationTime: (data.created_at + data.expires_in) * 1000,
        valid: true
      });
    }
    res.json({message: "Connect to GitLab successfully"});
  }

  static async disconnect(req, res) {
    const connection = await GitLabConnectionModel.findOne({owner: req.account._id});
    if (connection) connection.deleteOne();
    res.json({data: connection});
  }
}
