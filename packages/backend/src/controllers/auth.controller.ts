import {AccountModel} from "@src/models/account.model";
import bcrypt from "bcryptjs";
import {AuthService} from "@src/services/auth.service";
import {TokenModel} from "@src/models/token.model";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";

export class AuthController {
  static async login(req, res) {
    const account = await AccountModel.findOne({$or: [{username: req.body.loginData}, {email: req.body.loginData}]}).select("+password");
    if (!account || !bcrypt.compareSync(req.body.password, account.password)) throw Error("Login failed");
    delete account.password;
    const data = await AuthService.generateAuthTokens(account);
    res.json(data);
  }

  static async signUp(req, res) {
    const account = await AccountModel.create(req.body);
    const data = await AuthService.generateAuthTokens(account);
    res.json(data);
  }

  static async refreshToken(req, res) {
    const refreshToken = await TokenModel.findOne({token: req.body.refreshToken}).populate({
      path: "account",
    });

    if (!refreshToken || !refreshToken.account) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid token");
    const data = await AuthService.generateAuthTokens(refreshToken.account);
    await AuthService.revokeToken(refreshToken.token);
    res.json(data);
  }

  static async revokeToken(req, res) {
    await AuthService.revokeToken(req.body.token);
    res.json({message: "Revoked token successfully"});
  }

  static async logout(req, res) {
    const refreshToken = await TokenModel.findOne({token: req.body.refreshToken});
    if (!refreshToken) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid token");
    await AuthService.revokeToken(refreshToken.token);
    res.status(httpStatus.NO_CONTENT).json();
  }
}
