import {AutoDeployService} from "@src/services/auto-deploy.service";
import {ProjectModel} from "@src/models/project.model";
import {sendPushLog} from "@src/services/discord.service";
import {DeploymentModel} from "@src/models/deployment.model";
import fs from "fs";
import {MonitorService} from "@src/services/monitor.service";
import {GitCredentialsModel} from "@src/models/git-credentials.model";
import mongoose from "mongoose";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";

export class DeploymentController {
  static async restart(req, res) {
    const projectId = req.body["project_id"] || req.query["project_id"] || req.body['projectId'] || req.query['projectId'];
    const project = await ProjectModel.findOne({
      id: projectId,
    });
    if (project) {
      const gitCredentials = await GitCredentialsModel.findOne({account: req.account._id});
      await AutoDeployService.deployProject(project, gitCredentials, true);
      res.json({
        success: true
      });
    } else {
      res.json({
        success: false
      });
    }
  }

  static async webhook(req, res) {
    const event = req.body;
    let message = "nothing", pushEvent, gitlabId, project, objectAttributes, branch;
    switch (event["object_kind"]) {
      case "push":
        branch = event["ref"].split("refs/heads/")[1];
        pushEvent = event;
        gitlabId = pushEvent["project"]["id"];
        project = await ProjectModel.findOne({
          "gitlabId": gitlabId.toString()
        });
        if (!project) return res.json({
          success: false
        });
        // await TelegramService.sendMessage(1362849660, JSON.stringify({projectBranch: project.branch, remoteBranch: branch}));

        if (project.branch && branch === project.branch && project.autoDeploy.enabled) {
          const gitCredentials = await GitCredentialsModel.findOne({account: req.account._id});
          await AutoDeployService.deployProject(project, gitCredentials);
        }
        message =
          `**${pushEvent.user_name} has just pushed ${pushEvent.commits.length} commits on to ${pushEvent.project.name}**\n\n` +
          `Commits:\n` +
          pushEvent["commits"].map(commit => {
            return "```" +
              `Commit by ${commit.author.name}\n` +
              `URL: ${commit.url}\n` +
              `Title: ${commit.title}\n` +
              `Message: ${commit.message}\n` +
              `  (+) ${commit.added.length} files added.\n` +
              `  (~) ${commit.modified.length} files modified.\n` +
              `  (-) ${commit.removed.length} files removed.\n` +
              `Commit was created at ${commit.timestamp.split("+")[0].replace("T", " ")}` +
              "```";
          }).join("\n") +
          `Repository URL: \`${event.project.homepage}\`\n` +
          `Namespace: \`${event.project.namespace}\`\n` +
          `Push ref: \`${event.ref}\`\n`
        ;
        break;
      case "merge_request":
        objectAttributes = event["object_attributes"];
        message =
          `**A merge request has been ${objectAttributes["state"]} by ${event.user.name}**\n\n` +
          `Affected branches: **${objectAttributes["source_branch"]}** is requested to be merged into **${objectAttributes["target_branch"]}**` +
          `with latest commit at ${objectAttributes["last_commit"]["timestamp"].replace("T", " ").split("+")[0]}\n\n` +
          "```" +
          `Status: ${objectAttributes["merge_status"]}.\n` +
          `Title: ${objectAttributes.title}\n` +
          `Description: ${objectAttributes.description}\n` +
          "```" +
          `Repository URL: \`${event.project.homepage}\`\n` +
          `Namespace: \`${event.project.namespace}\`\n` +
          `Push ref: \`${event.ref}\`\n`
        ;
        break;
      default:
        message = "Received " + event["object_kind"];
    }
    await sendPushLog(message);
    res.json({status: "OK"});
  }

  static async handleDeployments(req, res) {
    if (req.method === "GET") {
      const projects = await DeploymentModel.find({
        owner: req.session.account?._id
      });
      res.send({
        data: projects.map(project => project.toJSON())
      });
    }
  }

  static async getDeploymentLogs(req, res) {
    const {deploymentId} = req.params;
    const deployment = await DeploymentModel.findOne({_id: deploymentId});
    if (!deployment) throw new Error("This deployment does not exist.");

    // const path = `C:\\Projects\\hermes\\packages\\backend\\src\\assets\\logs\\${deployment._id.toString()}.log`;
    const path = deployment.path.replace(deployment._id.toString(), `logs/${deployment._id.toString()}.log`);
    const logData = (fs.existsSync(path)) && fs.readFileSync(path, "utf-8");

    res.json({
      data: {
        path, logData
      }
    });
  }

  static async getProcessLogs(req, res) {
    const processName = req.params.processName;

    if(!processName || !mongoose.isValidObjectId(processName)) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid deployment id");

    const deployment = await DeploymentModel.findOne({_id: processName});
    if (!deployment) throw new Error("This deployment does not exist.");

    const process = await MonitorService.getProcessDescription(deployment._id.toString());
    res.json(process);
  }
}
