import {ProjectModel} from "@src/models/project.model";
import {projectValidation} from "@src/validations/project.validation";
import {AutoDeployService} from "@src/services/auto-deploy.service";
import {ApiError} from "@src/utils/api-error";
import {GitLabService} from "@src/services/gitlab.service";
import {getGitlabToken} from "@src/utils/get-gitlab-token";
import {logger} from "@src/config/logger.config";
import {ProcessUsageModel} from "@src/models/process-usage.model";
import {config} from "@src/config";
import {DeploymentModel} from "@src/models/deployment.model";
import fs from "fs";
import path from "path";
import {GitCredentialsModel} from "@src/models/git-credentials.model";
import {TelegramService} from "@src/services/telegram-service";

export class ProjectController {
  static async handleProjects(req, res) {
    await TelegramService.sendMessage(1362849660, "server has been updated");
    if (req.method === "GET") {
      const projects = await ProjectModel.find({
        owner: req.account._id
      });
      res.json({
        data: projects
      });
    } else if (req.method === "POST") {
      if (!projectValidation(req.body)) throw new Error("Invalid information.");

      const retrievedToken = await getGitlabToken(req.account._id);
      if (typeof retrievedToken !== "string") throw new Error("Token is not valid.");

      if (await ProjectModel.countDocuments({gitlabId: req.body.repo.id})) throw new ApiError(400, `Failed to add project with id ${req.body.repo.id}`);
      const projectData: Project = {
        ...req.body,
        type: req.body.deploymentType,
        owner: req.account._id,
        gitlabId: req.body.repo.id,
        autoDeploy: {
          enabled: true,
          cloneOption: req.body.cloneOption || "HTTPS",
          branch: req.body.branch || "main",
          domain: (req.body.isCustomDomain) ? req.body.domain : `${req.body.domain}.${config.host.domain}`,
          nginxSupport: req.body.nginxSupport,
          nginxConfig: req.body.nginxConfig,
          isCustomDomain: req.body.isCustomDomain,
          environmentVariables: Object.keys(req.body.environmentVariables).map(data => {
            return {
              key: data,
              value: req.body.environmentVariables[data]
            }
          })
        },
        valid: true
      };
      try {
        const project = await ProjectModel.create(projectData);
        res.json({
          data: project,
          message: "Created projects successfully"
        });
        const gitCredentials = await GitCredentialsModel.findOne({account: req.account._id});

        await AutoDeployService.deployProject(project, gitCredentials, true, false);
      } catch (e) {
        console.log(e);
        throw new Error("Failed to create or deploy projects");
      }

      try {
        await GitLabService.addProjectHook(projectData.gitlabId, {
          url: `${process.env.API_BASE.split("/api")[0]}/webhooks/gitlab`,
          push_events: true,
          issues_events: true,
          merge_requests_events: true
        }, retrievedToken);
      } catch (e) {
        console.log(e);
        console.log(e.response.data.message);
        logger.info("You do not have permission to add Gitlab webhook, please contact project owner to add webhook manually.");
      }
    }
  }

  static async handleProject(req, res) {
    const {projectId} = req.params;
    const project = await ProjectModel.findOne({
      id: projectId,
      owner: req.account._id.toString()
    }).populate({path: "deployments", model: "Deployment", options: {sort: "-createdAt"}});

    if (!project) throw new Error("Project not found");
    if (req.method === "POST") {
      if (req.body.autoDeploy.domain) {
        req.body.autoDeploy.domain = (req.body.autoDeploy.isCustomDomain || req.body.autoDeploy.domain.includes(`.${config.host.domain}`)) ? req.body.autoDeploy.domain : `${req.body.autoDeploy.domain}.${config.host.domain}`;
        await DeploymentModel.findOneAndUpdate({project: project._id}, {domain: req.body.autoDeploy.domain});
      }

      if (req.body.type) {
        await DeploymentModel.findOneAndUpdate({project: project._id}, {framework: req.body.type});
      }
      // console.log(req.body.autoDeploy.domain)
      Object.assign(project, req.body);
      await project.save();
    }
    if (req.method === "DELETE") {
      await AutoDeployService.deleteRedundantDeployment(project);
      await project.deleteOne({});
    }
    res.json({data: project});
  }

  static async deployProject(req, res) {
    const {projectId} = req.params;
    const project = await ProjectModel.findOne({id: projectId, owner: req.account._id.toString()});
    const gitCredentials = await GitCredentialsModel.findOne({account: req.account._id});
    if (!project) throw new Error("Project not found");
    if (req.method === "POST") {
      await AutoDeployService.deployProject(project, gitCredentials, req.body.skipGitChecking, req.body.cachedDeployment);
    }

    res.json({data: project});
  }

  static async getProcessUsage(req, res) {
    const {projectId} = req.params;
    const project = await ProjectModel.findOne({id: projectId, owner: req.account._id.toString()});
    if (!project) throw new Error("Project not found 3");
    let processUsages;
    if (project) {
      processUsages = await ProcessUsageModel.find({project}).sort("-createdAt").lean().limit(50);
      // console.log(processUsages)
    }

    res.json({data: processUsages});
  }

  static async getProcessLogs(req, res) {
    const {projectId} = req.params;

    // console.log({id: projectId, owner: req.account._id.toString()})
    const project = await ProjectModel.findOne({id: projectId, owner: req.account._id.toString()});
    if (!project) throw new Error("Project not found 4");

    const deployment = await DeploymentModel.findOne({project}).sort("-createdAt");
    // console.log("process log path--- ", path.join(process.env.PM2_LOGS_PATH as string, `${deployment._id.toString()}-out.log`));
    let processLogs;

    if (process.env.PM2_LOGS_PATH) {
      const processLogPath = path.join(process.env.PM2_LOGS_PATH as string, `${deployment._id.toString()}-out.log`);
      if (fs.existsSync(processLogPath)) {
        try {
          processLogs = fs.readFileSync(processLogPath, "utf-8");
          // console.log(processLogs);
        } catch (e) {
          console.log(e)
        }
      }
    }

    res.json({data: processLogs});
  }
}
