import {SlashCommandBuilder} from '@discordjs/builders';
import {REST} from '@discordjs/rest';
import {Routes} from 'discord-api-types/v9';
import Discord, {ThreadChannel} from "discord.js";
import {config} from "@src/config";

const clientId = ``;
const guildId = ``;
const botToken = config.discord.botToken;
const pushLogChannel = config.discord.pushLogChannel;
const deploymentLogChannel = config.discord.deploymentLogChannel;

// TODO: Convert into class
let discordClient;

export const discordStartup = async () => {
  try {
    discordClient = new Discord.Client({intents: ["GUILDS", "GUILD_MESSAGES"]});
    await discordClient.login(botToken);
    return discordClient;
  } catch (e) {
    console.log("Discord service could not start due to some unexpected issue.");
    console.log(e);
    return "";
  }
}

export const getChannel = async (id: string = pushLogChannel) => {
  const channel = discordClient.channels.cache.get(id);
  if (!channel) {
    await discordClient.channels.fetch(id);
    return await getChannel();
  }
  return channel;
}

export const createThread = async (name = 'new-thread', description = 'Just a new thread'): Promise<ThreadChannel> => {
  const channel = await getChannel(deploymentLogChannel);
  return await channel.threads.create({
    name: name,
    autoArchiveDuration: 60,
    reason: description
  });
}

export const sendPushLog = async (logContent) => {
  const logChannel = await getChannel(pushLogChannel);
  return await logChannel.send(logContent);
}

export const sendDeploymentLog = async (logContent) => {
  try {
    if (logContent.trim() !== "") {
      const logChannel = await getChannel(deploymentLogChannel);
      return await logChannel.send(logContent);
    }
  } catch (e) {
    console.log("Cannot send deployment log due to unexpected error from Discord.");
    console.log(e);
  }
}

export const editDeploymentLog = async (message, logContent) => {
  return await message.edit(logContent);
}

export const setupCommands = async () => {
  const commands = [
    new SlashCommandBuilder().setName('ping').setDescription('Replies with pong!'),
    new SlashCommandBuilder().setName('server').setDescription('Replies with server info!'),
    new SlashCommandBuilder().setName('user').setDescription('Replies with user info!'),
  ].map(command => command.toJSON());
  const rest = new REST().setToken(botToken);

  rest.put(Routes.applicationGuildCommands(clientId, guildId), {body: commands})
    .then(() => console.log('Successfully registered application commands.'))
    .catch(console.error);
}
