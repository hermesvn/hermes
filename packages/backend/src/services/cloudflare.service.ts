import axios from "axios";

interface DNSRecordBody {
  type: string,
  name: string,
  ttl: number,
  proxied: boolean
}

export class CloudflareService {
  protected static apiRoot = "https://api.cloudflare.com/client/v4/";
  protected static hostDomain = process.env.NEXT_PUBLIC_HOST_DOMAIN;
  protected static accessToken = process.env.CLOUDFLARE_TOKEN;
  protected static zoneId = process.env.CLOUDFLARE_ZONE_ID;
  protected static ip = process.env.HOST_IP_ADDRESS;

  static async getDNSRecords() {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/zones/${this.zoneId}/dns_records`,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`
      }
    });
  }

  static async getDNSRecord(recordId: string) {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/zones/${this.zoneId}/dns_records/${recordId}`,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`
      }
    });
  }

  static async addDNSRecords(dnsRecordBody: DNSRecordBody, options?: { auto: boolean }) {

    // if (options.auto) {
    //   const autoGenName = ""
    //   dnsRecordBody.name = `${autoGenName}.${this.hostDomain}`
    // }
    return axios({
      method: "POST",
      url: `${this.apiRoot}/zones/${this.zoneId}/dns_records`,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`
      },
      data: {
        ...dnsRecordBody,
        content: this.ip,
      }
    });
  }

  static async updateDNSRecords(recordId: string, dnsRecordBody: Partial<DNSRecordBody>) {
    return axios({
      method: "PATCH",
      url: `${this.apiRoot}/zones/${this.zoneId}/dns_records/${recordId}`,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`
      },
      data: dnsRecordBody
    });
  }
}
