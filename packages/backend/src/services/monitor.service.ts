import pm2, {ProcessDescription,} from "pm2";
import usage from "pidusage";
import {DeploymentModel} from "@src/models/deployment.model";
import {ProjectModel} from "@src/models/project.model";
import {ProcessUsageModel} from "@src/models/process-usage.model";
import {isValidObjectId} from "mongoose"

type ProcessStatus = 'online' | 'stopping' | 'stopped' | 'launching' | 'errored' | 'one-launch-status';

const listProcesses = (): Promise<any> => new Promise((resolve, reject) => {
  pm2.list((err, processDescriptionList) => {
    if (err) reject(err);
    resolve(processDescriptionList);
  });
});

export class MonitorService {
  static async register() {
    setInterval(() => {
      pm2.connect((err) => {
        if (err) console.log("Error occurred while connecting with pm2 service.");
        // console.log("PM2 service connected.");
        this.getUsageStats().then(processes => {
          // console.log("PM2 processes", processes)
          processes.forEach(async (proc) => {
            // console.log("pm2 process", proc);
            if (proc.name && isValidObjectId(proc.name)) {
              const deployment = await DeploymentModel.findOne({_id: proc.name}).lean();
              if (deployment) {
                try {
                  const project = await ProjectModel.findOne({_id: deployment.project});
                  project.cpuUsage = proc.cpu;
                  project.memUsage = proc.memory;
                  project.uptime = proc.uptime;
                  await project.save();
                  await ProcessUsageModel.create({
                    project: project._id,
                    time: new Date(),
                    cpu: proc.cpu,
                    mem: proc.memory,
                  });
                } catch (e) {
                  console.log("error while updating project info");
                }
              }
            }
          });
        });
      });
    }, 10000);
  }

  static async getUsageStats() {
    try {
      const procList: ProcessDescription[] = await listProcesses();
      const result = [];
      for (const pm2Process of procList) {
        await new Promise(_ => {
          usage(pm2Process.pid, (err, data) => {
            result.push({
              ...pm2Process,
              ...data
            });
            _(null);
          });
        });
      }

      return result.map(proc => ({
        pmId: proc.pm_id,
        pm2Env: {
          pm_out_log_path: proc.pm2_env.pm_out_log_path,
          pm_err_log_path: proc.pm2_env.pm_err_log_path,
          status: proc.pm2_env.status,
        },
        pid: proc.pid,
        name: proc.name,
        pm2Id: proc.pm_id,
        cpu: proc.monit.cpu,
        memory: proc.monit.memory,
        uptime: proc.pm2_env.pm_uptime,
      }));
    } catch (e) {
      console.log("Failed to get usage stats due to some error", e);
    }
  }

  static getProcessDescription(processName: string): Promise<ProcessDescription> {
    return new Promise(resolve => {
      pm2.describe(processName, function (err, processDescription) {
        if (err) {
          console.log(err);
        } else {
          const des = processDescription[0];
          const process = des ? {
            name: processName,
            pid: des.pid,
            pm_id: des.pm_id,
            monit: des.monit,
            pm2_env: {
              pm_out_log_path: des.pm2_env.pm_out_log_path,
              pm_err_log_path: des.pm2_env.pm_err_log_path,
              status: des.pm2_env.status,
            }
          } : {
            name: processName,
            pid: -1,
            pm_id: -1,
            monit: {
              memory: 0,
              cpu: 0
            },
            pm2_env: {
              status: "stopped" as ProcessStatus
            }
          };
          resolve(process);
        }
      });
    });
  }
}
