import {Server} from "http";
import {Server as SocketServer, Socket} from "socket.io";
import {logger} from "@src/config/logger.config";
import {ProcessUsageModel} from "@src/models/process-usage.model";
import moment from "moment";

type ProcessUsageState = "start" | "stop"

export class SocketService {
  static io: SocketServer;
  static socket;

  static bindSocketServer(server: Server) {
    this.io = new SocketServer(server, {
      path: "/socket.io",
      cors: {
        origin: "*"
      }
    });

    this.io.on("connection", (socket: Socket) => {
      logger.info(`A new connection has been established with id ${socket.id}`);

      socket.on("registerDeploymentLogThread", (project: string) => {
        socket.join(project);
      });

      socket.on("startProcessUsage", (project: string, state: ProcessUsageState = "start") => {
        // let processUsages;
        // const interval = setInterval(async () => {
        //   processUsages = (await ProcessUsageModel.find({project}).sort("-createdAt").limit(50).lean()).reverse().map(data => {
        //     return {
        //       date: moment(data["createdAt"]).format("YYYY-MM-DD HH:mm:ss"),
        //       cpu: data.cpu,
        //       ram: data.mem
        //     }
        //   });
        //   // console.log("processUsage", processUsages);
        //   if (processUsages.length > 0) {
        //     this.io.to(project).emit("updateProcessChart", processUsages);
        //   }
        // }, 15000);
        //
        // if (interval && state === "stop") clearInterval(interval);
      });

      socket.on("sendDeploymentLog", (project: string, log: string) => {
        socket.to(project).emit("updateDeploymentLogs", log, false);
      });
    });
  }
}
