import axios, {AxiosRequestConfig, AxiosResponse, Method} from "axios";

interface TokenResponse {
  access_token: string,
  token_type: string,
  expires_in: number,
  refresh_token: string,
  created_at: number
}

interface GitLabQueryOptions {
  pagination?: boolean;
  maxPages?: number;
  page?: number;
  limit?: number;
}

interface HookBody {
  url: string;
  push_events?: boolean;
  issues_events?: boolean;

  [key: string]: any;
}

interface ResponseData<T> extends AxiosResponse {
  data: T;
}

export class GitLabService {
  static appId = process.env.NEXT_PUBLIC_GITLAB_APP_ID;
  static appSecret = process.env.GITLAB_APP_SECRET;
  static apiRoot = "https://gitlab.com/api/v4";
  static redirectUri = (typeof location !== "undefined") ? (location.hostname === "localhost") ? `${location.protocol}//${location.hostname}:4200/auth/callback/gitlab` : `${location.protocol}//${location.hostname}/auth/callback/gitlab` : "";

  // static scopes = process.env.GITLAB_SCOPE

  static async verifyToken(accessToken: string) {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/projects`,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
  }

  static generateAuthUrl() {
    return `https://gitlab.com/oauth/authorize?client_id=${this.appId}&redirect_uri=${this.redirectUri}&response_type=code&state=STATE&scope=api`;
  }

  static openLogin(): Promise<void> {
    return new Promise((resolve, reject) => {
      const dialog = window.open(this.generateAuthUrl(), "_blank");
      const checkingInterval = setInterval(() => {
        try {
          // console.log(dialog?.location.pathname)
          if (dialog?.location.pathname === "/auth/success") {
            clearInterval(checkingInterval);
            dialog.close();
            resolve();
          }
        } catch (e) {
          console.log("---");
        }
      }, 500);
      dialog?.addEventListener("close", () => reject());
    });
  }

  static async requestToken({code, redirectUri}): Promise<ResponseData<TokenResponse>> {
    return axios({
      method: "POST",
      url: `https://gitlab.com/oauth/token`,
      params: {
        client_id: this.appId,
        code: code,
        grant_type: "authorization_code",
        redirect_uri: redirectUri,
        client_secret: this.appSecret
      }
    });
  }

  static async refreshToken(refreshToken: string): Promise<ResponseData<TokenResponse>> {
    // console.log('Requested to refresh the token.ts: ', refreshToken);
    return axios({
      method: "POST",
      url: `https://gitlab.com/oauth/token`,
      params: {
        client_id: this.appId,
        refresh_token: refreshToken,
        grant_type: "refresh_token",
        redirect_uri: this.redirectUri,
        client_secret: this.appSecret
      }
    });
  }

  static async getNamespaces(accessToken: string) {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/namespaces`,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
  }

  static async getProjects(namespace: string, accessToken: string) {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/${namespace}/projects`,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
  }

  static async getProjectBranches(projectId: string, accessToken: string) {
    return axios({
      method: "GET",
      url: `${this.apiRoot}/projects/${projectId}/repository/branches`,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
  }

  static async getProjectHooks(projectId: string, options: GitLabQueryOptions, accessToken: string) {
    const hooks = await this.request(`/projects/${projectId}/hooks`, "get", accessToken);
    return this.paginate(hooks, options);
  }

  static async addProjectHook(projectId: string, hookBody: HookBody, accessToken: string) {
    const hooks = await this.request(`/projects/${projectId}/hooks`, "get", accessToken);
    let hook = hooks.find(data => data.url === hookBody.url);
    hook = (hook) ?
      await this.request(`/projects/${projectId}/hooks/${hook.id}`, "put", accessToken, {data: hookBody}) :
      await this.request(`/projects/${projectId}/hooks`, "post", accessToken, {
        data: {
          ...hookBody,
          enable_ssl_verification: false
        }
      });
    return hook;
  }

  static async deleteProjectHook(accessToken: string, projectId: string, hookId: string) {
    let hook = await this.request(`/projects/${projectId}/hooks/${hookId}`, "get", accessToken);
    if (hook) hook = await this.request(`/projects/${projectId}/hooks/${hook.id}`, "delete", accessToken);
    return hook;
  }

  static async request(endpoints: string, method: Method, accessToken: string, axiosConfig?: AxiosRequestConfig) {
    const {data} = await axios(`${this.apiRoot}/${endpoints}`, {
      method,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
      ...axiosConfig
    });
    return data;
  }

  private static paginate(arr: any[], options: GitLabQueryOptions) {
    if (!options.page) options.page = 1;
    if (!options.limit) options.limit = 5;
    options.page = parseInt(options.page.toString());
    options.limit = parseInt(options.limit.toString());
    const skip = (options.page === 1) ? 0 : options.limit * (options.page - 1);
    const data = arr.slice(skip, (skip + options.limit));
    return {
      data: data,
      pagination: {
        total: arr.length,
        page: parseInt(options.page.toString()),
        hasPrev: options.page > 1,
        hasNext: arr.length > options.page * options.limit
      }
    };
  }


}
