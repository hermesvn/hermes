import {DeploymentModel} from "@src/models/deployment.model";
import portastic from "portastic";
import fs from "fs";
import path from "path";
import pm2 from "pm2";
import {SpawnAsync} from "@src/utils/spawn-async";
import {NGINX_NODEJS_CONFIG, NGINX_REACTJS_CONFIG} from "@src/config/nginx";
import {OsFunc} from "@src/utils/exec-command";
import rimraf from "rimraf";
import {logger} from "@src/config/logger.config";
import {LogService} from "@src/services/log.service";
import {config} from "@src/config";
import {CloudflareService} from "@src/services/cloudflare.service";
import {VersionControlService} from "@src/services/version-control.service";

export class ProjectDeployment {
  project = null;
  gitCredentials = null;
  deployment = null;
  port = null;
  listeners = {};
  isFirstDeployment = false;
  lastDeployment = null;
  gitRepo;
  newDeployment = false;
  skipGitChecking = false;
  thread = null;
  cachedDeployment = false;
  lastDeploymentGitRepoExists = false;
  deploymentPath = '';
  logService: LogService = null;

  constructor(project: Project, gitCredentials: GitCredentials, skipGitChecking = false, cachedDeployment = true) {
    this.skipGitChecking = skipGitChecking;
    this.project = project;
    this.gitCredentials = gitCredentials;
    this.cachedDeployment = cachedDeployment;
  }

  async init() {
    await this.prepare();
    this.logService = new LogService(this.project, this.deployment);
    this.thread = await this.logService.createLogThread();

    // console.log("Skip Git checking enabled: ", this.skipGitChecking);
    if (this.skipGitChecking) {
      await this.logService.generateDeploymentLogs("```[!!!] Warning: You're using skip git checking options. This deployment will ignore all previous deployment to pull the latest code. Please be noticed.```");
    }
    await this.logService.generateDeploymentLogs("GitLab ID: `" + this.project.gitlabId + "`");

    /*
    * First step, we will check for the last deployment of projects.
    * This will make sure that the projects will only be deployed when it's really
    * needed (git repo update). Otherwise, it will not trigger a new deployment.
    * */
    await this.prepareVersioning();
    if (this.newDeployment) {
      const installCode = await this.installDependencies();
      if (installCode !== 0) return this.logService.generateDeploymentLogs("Failed to install dependencies.");
      const buildCode = await this.buildProject();
      if (buildCode !== 0) return this.logService.generateDeploymentLogs("Failed to buildProject.");
      await this.finish();
    }
  }

  async prepare() {
    // Find a free port for running this project
    this.port = await this.getFreePort();
    // check for first deployment status
    const deployments = await DeploymentModel.find({
      project: this.project._id
    }).sort({
      _id: -1
    }).limit(1);
    if (!deployments || deployments.length === 0) {
      this.isFirstDeployment = true;
    } else {
      this.lastDeployment = deployments[0];
    }

    if (!this.isFirstDeployment) {
      // Find previous deployment
      this.lastDeploymentGitRepoExists = this.checkLastDeploymentExists(this.lastDeployment.path);
    }

    // Create a new deployment each time received a deployment request
    this.deployment = (!this.cachedDeployment || !this.lastDeploymentGitRepoExists) ? new DeploymentModel({
      creationTime: new Date(),
      project: this.project._id,
      status: "started",
      framework: this.project.type,
      domain: this.project.autoDeploy.domain
    }) : this.lastDeployment;

    this.deployment.port = this.port;

    if (!this.isFirstDeployment && this.lastDeploymentGitRepoExists) {
      this.deploymentPath = this.lastDeployment.path;
    } else {
      this.deploymentPath = this.getDeploymentPath();
    }
    this.deployment.path = this.deploymentPath;
  }

  checkLastDeploymentExists(path: string) {
    return fs.existsSync(path);
  }

  async prepareVersioning() {
    logger.info("Current Deployment: ", this.deployment);

    const lastDeployment = this.lastDeployment;
    const project = this.project;
    this.lastDeploymentGitRepoExists = this.lastDeployment && fs.existsSync(lastDeployment.path);

    logger.info(`[${project.code}] with GitlabID: ${project.gitlabId}`);

    const {gitRepo, newDeployment} = await VersionControlService.prepareVersioning(this.gitCredentials, {
        cloneOption: this.project.autoDeploy.cloneOption,
        gitlabId: project.gitlabId,
        branch: project.autoDeploy.branch,
        lastDeploymentPath: lastDeployment && lastDeployment.path,
        deploymentPath: this.getDeploymentPath(),
        skipGitChecking: this.skipGitChecking,
        cachedDeployment: this.cachedDeployment,
        lastDeploymentGitRepoExists: lastDeployment && lastDeployment.path && this.lastDeploymentGitRepoExists
      }, this.logService
    );

    // return this.finish();

    this.gitRepo = gitRepo;
    this.newDeployment = newDeployment;

    console.log("git repo", this.gitRepo);
  }

  async finish() {

    console.log("Git repo", this.gitRepo)
    if (this.newDeployment) {
      const project = this.project;
      await this.deployment.save();
      const envPair = {};
      project.autoDeploy["environmentVariables"].map(variable => {
        envPair[variable.key] = variable.value;
      });
      envPair["PORT"] = this.deployment.port;
      envPair["NODE_ENV"] = "production";
      if (["nodejs", "nextjs", "svelte-kit", "nodejs-webserver"].includes(project["type"])) {
        await this.logService.generateDeploymentLogs(`Project will be deployed`);

        const finalize = async () => {
          let config;
          if (project["type"] === "svelte-kit") {
            config = {
              name: this.deployment._id.toString(),
              script: "node",
              args: "build/index.ts",
              cwd: this.getDeploymentPath(),
              env: envPair
            };
          } else {
            config = {
              name: this.deployment._id.toString(),
              script: "yarn",
              args: "start",
              cwd: this.getDeploymentPath(),
              interpreter: "none",
              env: envPair
            };
          }
          pm2.start(config, async (err) => {
            if (err) {
              await this.logService.generateDeploymentLogs("Failed to start deployment " + this.deployment._id.toString());
              await this.logService.generateDeploymentLogs(err.toString());
            } else {
              await this.logService.generateDeploymentLogs("Started project process " + this.deployment._id.toString());
            }
          });
        };

        if (!this.cachedDeployment && this.lastDeployment) {
          pm2.delete(this.lastDeployment._id.toString(), async (err) => {
            if (err) {
              await this.logService.generateDeploymentLogs("Failed to delete project process " + this.lastDeployment._id.toString());
              await this.logService.generateDeploymentLogs(err.toString());
            } else await this.logService.generateDeploymentLogs("Deleted project process " + this.lastDeployment._id.toString());

            await this.removeOldDeployment();
            await finalize();
          });
        } else {
          await finalize();
        }
      }
      // else if (["reactjs"].includes(project["type"])) {
      //   await this.configNetwork();
      // }
      await this.configNetwork();
      this.deployment.status = "finish";
      await this.deployment.save();
      await this.logService.generateDeploymentLogs(`Deployment finished.`);
      this.call("finished");
      this.thread.setName(this.generateThreadName('Finished'));
    } else {
      this.thread.setName(this.generateThreadName('Finished'));
    }
    this.logService.saveDeploymentLog();
  }

  getDeploymentPath() {
    const baseDir = process.env.DEPLOYMENT_PATH || process.env.HOME || process.env.USERPROFILE;
    return path.join(baseDir, this.deployment._id.toString());
  }

  async installDependencies() {
    const project = this.project;
    const depsInstall = await SpawnAsync({
      command: "yarn install",
      options: {
        shell: true,
        cwd: this.getDeploymentPath()
      },
      prefix: project["code"],
      thread: this.thread,
      logService: this.logService
    });
    return await depsInstall.runtime;
  }

  async buildProject() {
    const project = this.project;
    if (project["type"] === "reactjs" || project["type"] === "nextjs" || project["type"] === "svelte-kit") {
      const building = await SpawnAsync({
        command: "yarn build",
        options: {
          shell: true,
          cwd: this.getDeploymentPath()
        },
        prefix: project["code"],
        thread: this.thread,
        logService: this.logService
      });
      return await building.runtime;
    }
    return 0;
  }

  generateThreadName(status: string) {
    return `[${status}] ${this.project.name}`
  }

  async getFreePort() {
    const freePorts = await portastic.find({min: 1300, max: 1800});
    return freePorts[~~(Math.random() * freePorts.length)];
  }

  async configNetwork() {
    const {autoDeploy} = this.project;
    const {nginxSupport} = autoDeploy;
    if (nginxSupport) {
      const siteConfigPath = path.join(process.env.NGINX_PATH, autoDeploy.domain);
      if (fs.existsSync(siteConfigPath)) {
        fs.unlinkSync(siteConfigPath);
      }

      if (this.project.autoDeploy.domain.includes(config.host.domain)) {
        try {
          await CloudflareService.addDNSRecords({
            type: "A",
            name: this.project.autoDeploy.domain.split(config.host.domain)[0],
            ttl: 1,
            proxied: true
          });
        } catch (e) {
          // if ()
          logger.info("No changes in domain");
        }
      }

      let networkConfig;
      if (this.project["type"] === "reactjs") {
        const buildDir = path.join(this.getDeploymentPath(), "build");
        networkConfig = NGINX_REACTJS_CONFIG(autoDeploy.domain, buildDir);
      } else {
        networkConfig = NGINX_NODEJS_CONFIG(autoDeploy.domain, this.port);
      }
      fs.writeFileSync(siteConfigPath, networkConfig);
      await OsFunc.execCommand(`sudo nginx -s reload`);
    }
    this.call("network-configured");
  }

  removeOldDeployment() {
    return new Promise(resolve => {
      rimraf(this.lastDeployment.path, async (err) => {
        if (err) {
          console.log(err)
          await this.logService.generateDeploymentLogs("Failed to delete deployment " + this.lastDeployment._id.toString());
          await this.logService.generateDeploymentLogs(err.toString());
        }
        await this.logService.generateDeploymentLogs(`Removed previous deployment files.`);
        resolve("");
      });
    });
  }

  call(name: string, data?: any) {
    if (this.listeners[name]) this.listeners[name](data);
  }

  on(name, callback) {
    this.listeners[name] = callback;
  }
}
