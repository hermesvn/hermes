import nodegit from "nodegit";
import path from "path";
import fs from "fs";
import {NGINX_NODEJS_CONFIG} from "@src/config/nginx";
import {OsFunc} from "@src/utils/exec-command";
import {ProjectDeployment} from "@src/services/project-deployment";
import {logger} from "@src/config/logger.config";
import {DeploymentModel} from "@src/models/deployment.model";
import pm2 from "pm2";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";

export class AutoDeployService {
  static projects = [];
  static processes = {};

  static gitCredentials = function (url, userName) {
    return nodegit.Cred.userpassPlaintextNew(process.env.GITLAB_USER as string, process.env.GITLAB_PASSWORD as string);
  };

  // static async start() {
  //   await this.checkDeploymentPaths();
  //
  //   const projects = await ProjectModel.find({
  //     "autoDeploy.enabled": true
  //   });
  //
  //   projects.forEach(project => {
  //     logger.info("Deploying projects...");
  //     this.deployProject(project, null,false);
  //   });
  // }

  static async checkDeploymentPaths() {
    if (!fs.existsSync(path.join(process.env.NGINX_PATH as string, "autodeploy"))) {
      const config = NGINX_NODEJS_CONFIG("hermes.northstudio.dev", 4123);
      fs.writeFileSync(path.join(process.env.NGINX_PATH as string, "autodeploy"), config);
      await OsFunc.execCommand(`sudo nginx -s reload`);
    }

    try {
      await OsFunc.execCommand(`sudo ls ${process.env.DEPLOYMENT_PATH}`);
    } catch (e) {
      logger.info("Deployment path does not exists, creating new one...");
      await OsFunc.execCommand(`sudo mkdir ${process.env.DEPLOYMENT_PATH}`);
    }
  }

  static async deployProject(project: Project, gitCredentials: GitCredentials = null, skipGitChecking = false, cachedDeployment = true) {
    if (!gitCredentials) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Please add credentials config before deploying a project");

    const projectDeployment = new ProjectDeployment(project, gitCredentials, skipGitChecking, cachedDeployment);
    await projectDeployment.init();
    projectDeployment.on("created", () => {
      logger.info("new deployment has just been created");
    });
    projectDeployment.on("repo-prepared", () => {
      logger.info("repository has been prepared");
    });
    projectDeployment.on("network-configured", () => {
      logger.info("network has been configured");
    });
    projectDeployment.on("finished", () => {
      logger.info("deployment finished");
    });
  }

  static async deleteRedundantDeployment(project: Project) {
    const deployments = await DeploymentModel.find({project: project._id}).sort("-createdAt");
    const lastDeployment = deployments[0];

    if (lastDeployment) {
      if (project.autoDeploy.domain) {
        const siteConfigPath = path.join(process.env.NGINX_PATH, project.autoDeploy.domain);
        // console.log(siteConfigPath)
        if (fs.existsSync(siteConfigPath)) fs.unlinkSync(siteConfigPath);
      }

      pm2.delete(lastDeployment._id.toString(), async (err, proc) => {
        if (err) logger.error(err);
      });

      for (const deployment of deployments) {
        try {
          await OsFunc.execCommand(`sudo ls \\${process.env.DEPLOYMENT_PATH}`);
          await OsFunc.execCommand(`sudo rm -rf \\${process.env.DEPLOYMENT_PATH}/${deployment._id.toString()}`);
        } catch (e) {
          logger.info(`Deployment with id ${deployment._id} does not exists, skipped deleting!`);
        }
      }
    }
    await DeploymentModel.deleteMany({project: project._id});
  }
}
