// import pm2 from "pm2";
import {createThread} from "@src/services/discord.service";
import {logger} from "@src/config/logger.config";
import moment from "moment";
import fs from "fs";
import path from "path";
import {io, Socket} from "socket.io-client";
import {MonitorService} from "@src/services/monitor.service";

export class LogService {
  protected thread = null;
  protected project: Project;
  protected deployment: Deployment;
  protected deploymentLogPath: string;
  protected socket: Socket;
  deploymentLogText = "";

  constructor(project: Project, deployment?: Deployment) {
    this.project = project;
    this.deployment = deployment
    if (this.deployment) this.deploymentLogPath = `${process.env.DEPLOYMENT_PATH}`;

  }

  // deployment log
  async createLogThread() {
    try {
      this.socket = io(`ws://localhost:${process.env.PORT}`, {
        // auth: {
        //   token: "1000000000",
        // },
      });
      this.thread = await createThread(this.generateThreadName('Started'), "Project deployment log.");
      return this.thread;
    } catch (e) {
      console.log(e);
      console.log("[Error] Cannot create new deployment thread due to Discord issue.");
    }
  }

  protected generateThreadName(status: string) {
    return `[${status}] ${this.project.name}`
  }

  getLogsPath(deploymentId, type = "runtime") {
    // const defaultDeploymentPath = this.getDeploymentPath();

    // return path.join(defaultDeploymentPath, "logs", type + "-" + deploymentId.toString() + ".log");
  }

  async generateDeploymentLogs(text: string): Promise<string> {
    await this.sendDiscordThreadMessage(text);

    this.deploymentLogText += `${moment().utcOffset('+0700').format("HH:mm:ss")}:${moment().millisecond()}-logContent-${text}-break-`;

    // send deployment id as room key
    this.socket.emit("sendDeploymentLog", this.project._id.toString(), `${moment().utcOffset('+0700').format("HH:mm:ss")}:${moment().millisecond()}-logContent-${text}\n`);
    return this.deploymentLogText;
  }

  saveDeploymentLog() {
    if (!fs.existsSync(path.join(this.deploymentLogPath, "logs"))) {
      fs.mkdirSync(path.join(this.deploymentLogPath, "logs"));
    }

    if (this.deploymentLogPath) {
      fs.writeFileSync(path.join(this.deploymentLogPath, "logs", `${this.deployment._id.toString()}.log`), this.deploymentLogText);
    }
  }

  protected async sendDiscordThreadMessage(text: string) {
    try {
      if (this.thread)
        await this.thread.send(text);
    } catch (e) {
      logger.error("[Error] An unexpected error occurred while sending message");
    }
  }

  static async getProcessLogs (processName: string) {
    return await MonitorService.getProcessDescription(processName)
  }
}
