import 'module-alias/register';
import express from "express";
import passport from "passport";
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import {config} from '@src/config';
import {errorHandler} from "@src/middlewares/error.middleware";
import {mainRouter} from '@src/routes';
import {logger} from "@src/config/logger.config";
import multer from 'multer';
import mongoose from "mongoose";
import {initiateData} from "@src/utils/initial-data";
import {discordStartup, sendDeploymentLog} from "@src/services/discord.service";
import pm2 from "pm2";
import {SocketService} from "@src/services/socket.service";
// import {AutoDeployService} from "@src/services/auto-deploy.service";
// import {TelegramService} from "@src/services/telegram-service";

const port = config.port;

export class WebService {
  static port: string | number = config.port;

  private static app = express();
  private static upload = multer();

  public static async start() {
    logger.info("Starting new Mongo DB connection...");
    await mongoose.connect(config.mongoose.url);
    logger.info("Connected to MongoDB.");
    await initiateData();
    // await TelegramService.register();
    await discordStartup();

    // if (process.env.NODE_ENV === "production") {
    pm2.connect(async function (err) {
      if (err) {
        await sendDeploymentLog(`Deployment service error:`);
        await sendDeploymentLog(err);
        process.exit(2);
      }
      await sendDeploymentLog(`Deployment service connected.`);
      await sendDeploymentLog(``);
      await sendDeploymentLog(`Auto deployment & Push Service has just been started.`);
      // await AutoDeployService.start();
    });
    // }
    this.runMiddlewares();
    const httpServer = this.app.listen(this.port, () => {
      logger.info(`Server is listening at http://localhost:${this.port}`);
      logger.info(`Socket server is running at ws://localhost:${this.port}`);
    });

    SocketService.bindSocketServer(httpServer);
  }

  private static runMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: true}));
    this.app.use(this.upload.array("file"));
    this.app.use(cookieParser(config.jwt.secret));
    this.app.use(cors());
    this.app.options('*', cors());
    this.app.use(passport.initialize());
    this.app.use(mainRouter);
    this.app.use(function (req, res) {
      return res.status(404).json({error: 'Page not found'});
    });
    this.app.use(errorHandler);
  }
}
