import {Telegraf} from "telegraf";

export class TelegramService {
  static bot = new Telegraf("5561298695:AAGrQziH_MLisEelj9NYa1ZUNQcOqPx0BB4");

  static async register() {
    const bot = this.bot;
    await bot.launch();

    process.once('SIGINT', () => bot.stop('SIGINT'))
    process.once('SIGTERM', () => bot.stop('SIGTERM'))
  }

  static async sendMessage(chatId: string | number, content: any) {
    return this.bot.telegram.sendMessage(chatId, content);
  }
}
