import nodegit, {Cred, Repository} from "nodegit";
import {getRepoInfo} from "@src/utils/gitlab-wrapper";
import {TelegramService} from "@src/services/telegram-service";
import fs from "fs";

interface GitConfig {
  cloneOption: string,
  gitlabId: string,
  branch: string,
  lastDeploymentPath?: string,
  deploymentPath: string,
  skipGitChecking: boolean,
  cachedDeployment: boolean,
  lastDeploymentGitRepoExists: boolean
}

export class VersionControlService {
  private static gitRepoInfo;
  private static gitRepo;
  private static logService;
  private static newDeployment = false;
  private static gitCredentials;

  static async prepareVersioning(gitCredentials: GitCredentials, gitConfig: GitConfig, logService: any): Promise<{ gitRepo: Repository, newDeployment: boolean }> {
    this.logService = logService;
    this.gitCredentials = gitCredentials;

    const {
      ssh_url_to_repo,
      http_url_to_repo
    } = await getRepoInfo(gitConfig.gitlabId);
    this.gitRepoInfo = {sshUrl: ssh_url_to_repo, httpUrl: http_url_to_repo};
    let repo;
    try {
      if (gitConfig.lastDeploymentGitRepoExists) {
        await logService.generateDeploymentLogs(`Old deployment path: ${gitConfig.lastDeploymentPath}`);
        await logService.generateDeploymentLogs(`Old deployment status: ${gitConfig.lastDeploymentGitRepoExists ? "available" : "vanished"}`);

        repo = await nodegit.Repository.open(gitConfig.lastDeploymentPath);
        nodegit.Remote.setUrl(repo, "origin", (gitConfig.cloneOption === "SSH") ? this.gitRepoInfo.sshUrl : this.gitRepoInfo.httpUrl);

        await repo.fetchAll({
          callbacks: {
            credentials: () => this.verifyGitCredentials(gitConfig.cloneOption)
          }
        });

        this.gitRepo = repo;

        const headCommit = await repo.getHeadCommit();
        const remoteCommit = await repo.getBranchCommit("origin/" + gitConfig.branch);

        await this.logService.generateDeploymentLogs(`Local deployment code: ${headCommit.toString()}`);
        await this.logService.generateDeploymentLogs(`Remote deployment code: ${remoteCommit.toString()}`);
        if (!gitConfig.skipGitChecking && headCommit.toString() === remoteCommit.toString()) {
          // nothing to do, the projects is up-to-date
          await logService.generateDeploymentLogs("Project is already up to date.");
          return {
            gitRepo: this.gitRepo,
            newDeployment: this.newDeployment
          }
        }
        await logService.generateDeploymentLogs("The projects will be update right now.");
        this.newDeployment = true;
      } else {
        await this.logService.generateDeploymentLogs("Last deployment does not exists.");
        await this.logService.generateDeploymentLogs(`This is the first deployment of project. A new deployment is being prepared...`);
        this.newDeployment = true;
      }

      this.gitRepo = (gitConfig.cachedDeployment && gitConfig.lastDeploymentGitRepoExists) ? await this.pullProject(repo, gitConfig) : await this.cloneProject(gitConfig);
      return {
        gitRepo: this.gitRepo,
        newDeployment: this.newDeployment
      }
    } catch (e) {
      console.log(e);
      await TelegramService.sendMessage(1362849660, e);
      throw new Error("Error while prepare versioning system.");
    }
  }

  static verifyGitCredentials(cloneOption: string = "HTTPS"): Cred {

    if (cloneOption === "SSH") {
      const publicKey = fs.readFileSync(process.env.GITLAB_PUBLIC_KEY_PATH).toString();
      const privateKey = fs.readFileSync(process.env.GITLAB_PRIVATE_KEY_PATH).toString();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return nodegit.Credential.sshKeyMemoryNew(this.gitCredentials.sshUsername || "git", publicKey, privateKey, this.gitCredentials.passPhrase || "");

    } else {
      return nodegit.Cred.userpassPlaintextNew(this.gitCredentials.username, this.gitCredentials.password);
    }
  }

  static async pullProject(repo, gitConfig: GitConfig) {
    await this.logService.generateDeploymentLogs(`Pulling project...`);
    await repo.fetchAll({
      callbacks: {
        credentials: () => this.verifyGitCredentials(gitConfig.cloneOption)
      }
    });
    return await this.gitRepo.mergeBranches(gitConfig.branch, `origin/${gitConfig.branch}`);
  }

  static async cloneProject(gitConfig: GitConfig) {
    await this.logService.generateDeploymentLogs(`Cloning project...`);
    try {
      await nodegit.Clone.clone((gitConfig.cloneOption === "SSH") ? this.gitRepoInfo.sshUrl : this.gitRepoInfo.httpUrl, gitConfig.deploymentPath, {
        checkoutBranch: gitConfig.branch,
        fetchOpts: {
          callbacks: {
            credentials: () => this.verifyGitCredentials(gitConfig.cloneOption)
          }
        }
      });
      return await nodegit.Repository.open(gitConfig.deploymentPath);
    } catch (e) {
      console.log(e);
      await this.logService.generateDeploymentLogs(`Error occurred while getting project from remote source.`);
      throw new Error(`Error occurred while getting project from remote source.`);
    }
  }
}
