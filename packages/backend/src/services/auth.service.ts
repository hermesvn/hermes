import jwt, {SignOptions} from "jsonwebtoken";
import {config} from "@src/config";
import {TokenModel} from "@src/models/token.model";
import moment from "moment";

interface TokenPayload {
  [key: string]: string
}

export class AuthService {
  static generateToken(key: TokenPayload, options: SignOptions = {}) {
    return jwt.sign({
      ...key
    }, config.jwt.secret, options).toString();
  }

  static saveToken(token: string, account: Account, type?: TokenType) {
    return TokenModel.create({token, account, type});
  }

  static revokeToken(token: string) {
    return TokenModel.deleteOne({token});
  }

  static extractTokenFromRequest = (req) => {
    let jwt: string;
    if (req.cookies && req.cookies.access_token) {
      // jwt = getCookie("access_token", { req });
    } else if (req.headers && req.headers.authorization) {
      jwt = req.headers.authorization.slice("Bearer ".length);
    } else if (req.body && req.body.access_token) {
      jwt = req.body.access_token;
    }
    return jwt;
  };

  static async generateAuthTokens(account) {
    const accessToken = AuthService.generateToken({sub: account.id}, {expiresIn: "1h"});
    const refreshToken = AuthService.generateToken({sub: account.id}, {expiresIn: "1d"});
    await this.saveToken(refreshToken, account._id);
    return {
      account,
      credentials: {
        accessToken,
        refreshToken,
        accessTokenExpiresTime: moment().add(1, 'hours').format("x"),
        refreshTokenExpiresTime: moment().add(30, 'days').format("x"),
      }
    }
  }
}
