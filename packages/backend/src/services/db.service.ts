// import httpStatus from 'http-status';
// import {ApiError} from '@src/utils/api-error';
// import mongoose, {Document, PaginateOptions} from "mongoose";
// import {logger} from "@src/config/logger.config";
// import {config} from "@src/config";
// import {slugify} from "@src/utils/process-string";
//
// const DATE_FIELDS = ["createdAt", "dueDate", "deadline"];
//
// export interface DbQueryOptions extends PaginateOptions {
//   nullable?: boolean,
//   uniqueFields?: string[],
//   many?: boolean,
//   ability?: any,
//   customError?: {
//     message: string
//     code: number
//   },
//   filterAsDoc?: any
// }
//
// export interface PaginateDocResult<T> {
//   data: (Document<T> & T)[],
//   pagination: {
//     total: number,
//     page: number,
//     hasPrev: boolean,
//     hasNext: boolean,
//   }
// }
//
// export type DataModel<T> = Document & T;
// export type Documents = string;
// // 'AccountModel'
// // | 'TokenModel'
// // | 'RoleModel'
// // | 'AppModel'
// // | 'AppAccountModel'
// // | 'AppAuthRecordModel'
// // | 'ProxyModel';
//
// export class DbService {
//
//   static async start() {
//     mongoose.connect(process.env.MONGO_URL, async () => {
//       try {
//         await this.generateInitialData();
//       } catch (e) {
//         logger.error(e.stack);
//         await this.disconnect()
//       }
//     });
//   }
//
//   static async generateInitialData() {
//     // const existsRole = config.system.roles;
//     // const roles = (await this.getDocsByFilter<Role>("RoleModel", {name: {$in: existsRole}}, {
//     //   many: true,
//     //   lean: true,
//     //   nullable: true
//     // })).map(data => data.name);
//     // const newRoles = existsRole.filter(data => !roles.includes(data)).map(data => {
//     //   return {
//     //     insertOne: {
//     //       document: {
//     //         name: data,
//     //         slug: slugify(data)
//     //       }
//     //     }
//     //   }
//     // });
//     // if (newRoles.length > 0) await this.bulkWriteDoc("RoleModel", newRoles);
//     // if ((await this.countDocumentByFilter("AccountModel", {})) <= 0) {
//     //   await this.addDoc("AccountModel", {
//     //     fullName: "Admin",
//     //     role: "admin",
//     //     email: "admin@northstudio.vn",
//     //     password: "admin@123"
//     //   });
//     // }
//   }
//
//   static async disconnect() {
//     return await mongoose.disconnect();
//   }
//
//   // static async paginateDocsByFilter<T>(model: Documents, filter: DocFilter = {}, req, options: DbQueryOptions = {}): Promise<PaginateDocResult<T>> {
//   //   const excludedFields = [...EXCLUDE_FIELDS, filter.range]
//   //
//   //   const paginateSettings = requestPagination(req);
//   //   if (filter.range) {
//   //     if (DATE_FIELDS.includes(filter.range)) {
//   //       Object.assign(filter, {
//   //         start: new Date(parseInt(req.query["start"].toString())),
//   //         end: new Date(parseInt(req.query["end"].toString())),
//   //       });
//   //     }
//   //     filter[filter.range] = (filter.start && filter.end) ? {
//   //       $gte: filter.start,
//   //       $lte: filter.end
//   //     } : (filter.start) ? {$gte: filter.start} : (filter.end) && {$lte: filter.end}
//   //     delete filter.start;
//   //     delete filter.end;
//   //     delete filter.range;
//   //   }
//   //
//   //   for (const field in filter) {
//   //     if (!isValidObjectId(filter[field]) && !excludedFields.includes(field) && typeof filter[field] === 'string') filter[field] = new RegExp(filter[field], 'i');
//   //   }
//   //
//   //   const docs = await models[model].paginate(filter, {...paginateSettings, ...options});
//   //   return processPaginationData(docs);
//   // }
//
//   // static async getDocsByFilter<T>(model: Documents, filter: any = {}, options: DbQueryOptions = {}): Promise<DataModel<T>[]> {
//   //   if (filter.range) {
//   //     if (DATE_FIELDS.includes(filter.range)) {
//   //       Object.assign(filter, {
//   //         start: new Date(parseInt(filter.start)),
//   //         end: new Date(parseInt(filter.end)),
//   //       });
//   //     }
//   //     filter[filter.range] = (filter.start && filter.end) ? {
//   //       $gte: filter.start,
//   //       $lte: filter.end
//   //     } : (filter.start) ? {$gte: filter.start} : (filter.end) && {$lte: filter.end}
//   //     delete filter.start;
//   //     delete filter.end;
//   //     delete filter.range;
//   //   }
//   //   const doc = await models[model].find(filter, null, options).populate(options.populate).select(options.select);
//   //   if (options.nullable === false && doc.length === 0) throw (options.customError) ? new ApiError(options.customError.code, options.customError.message) : new Error(`${(model as string).split("Model")[0]} not found`);
//   //   return doc;
//   // }
//   //
//   // static async getDocByFilter<T>(model: Documents, filter: any = {}, options: DbQueryOptions = {}): Promise<DataModel<T>> {
//   //   const doc = await models[model].findOne(filter, null, options).populate(options.populate).select(options.select);
//   //   if (!options.nullable && !doc) throw (options.customError) ? new ApiError(options.customError.code, options.customError.message) : new Error(`${(model as string).split("Model")[0]} not found`);
//   //   return doc;
//   // }
//   //
//   // static async addDoc<T>(model: Documents, docBody, options: DbQueryOptions = {
//   //   uniqueFields: [],
//   //   populate: ""
//   // }): Promise<DataModel<T>> {
//   //   if (typeof docBody !== "object" || Object.keys(docBody).length <= 0) throw new ApiError(httpStatus.BAD_REQUEST, "Please provide valid doc body");
//   //   if (options.many) {
//   //     const doc = await models[model].insertMany(docBody, options);
//   //     return doc;
//   //   } else {
//   //     if (options?.uniqueFields?.length > 0) {
//   //       for (const field of options.uniqueFields) {
//   //         if (await models[model].exists({[field]: docBody[field]})) {
//   //           throw new ApiError(httpStatus.BAD_REQUEST, `This ${field} has already been taken`);
//   //         }
//   //       }
//   //     }
//   //     const doc = await models[model].create(docBody);
//   //     (options.populate) && await doc.populate(options.populate);
//   //     return doc;
//   //   }
//   // }
//   //
//   // static async updateDocByFilter<T>(model: Documents, filter, updateBody: any, options: DbQueryOptions = {}): Promise<DataModel<T>> {
//   //   if (options.many) {
//   //     await models[model].updateMany(filter, updateBody);
//   //   } else {
//   //     let doc = (options.filterAsDoc) ? options.filterAsDoc : await this.getDocByFilter<T>(model, filter, options);
//   //     if (options?.uniqueFields?.length > 0) {
//   //       for (const field of options.uniqueFields) {
//   //         if (await models[model].exists({[field]: updateBody[field], _id: {$ne: doc._id}})) {
//   //           throw new ApiError(httpStatus.BAD_REQUEST, `This ${field} has already been taken`);
//   //         }
//   //       }
//   //     }
//   //     Object.assign(doc, updateBody);
//   //     doc = await doc.save();
//   //     if (options.populate) await doc.populate(options.populate);
//   //     return doc;
//   //   }
//   // }
//   //
//   // static async deleteDocByFilter<T>(model: Documents, filter, options: DbQueryOptions = {}): Promise<DataModel<T>> {
//   //   if (options.many) {
//   //     await models[model].deleteMany(filter);
//   //
//   //   } else {
//   //     const doc = await this.getDocByFilter<T>(model, filter, options) as DataModel<T>;
//   //     await doc.deleteOne({});
//   //     return doc;
//   //   }
//   // }
//   //
//   // static async bulkWriteDoc(model: Documents, operations: any = []) {
//   //   await models[model].bulkWrite(operations);
//   //   return;
//   // }
//   //
//   // static async countDocumentByFilter(model: Documents, filter: any = {}, options: DbQueryOptions = {}) {
//   //   return await models[model].countDocuments(filter);
//   // }
// }
