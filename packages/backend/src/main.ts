import "module-alias/register";
import {logger} from "@src/config/logger.config";
import {WebService} from "@src/services/web.service";
// import {TelegramService} from "@src/services/telegram-service";
// import {MonitorService} from "@src/services/monitor.service";

const main = async () => {
  try {
    // await TelegramService.register();
    await WebService.start();
    // MonitorService.register().then(() => {
    //   logger.info("Registered Monitoring Service.");
    // });
  } catch (e) {
    logger.error(e.stack);
  }
};
// eslint-disable-next-line @typescript-eslint/no-empty-function
main().then();
